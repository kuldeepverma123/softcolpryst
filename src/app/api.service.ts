import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  // apiUrl: string = 'http://localhost:3000/api';
  apiUrl: string = 'https://softcolprystapi-app.herokuapp.com/api';
  headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) { }


  createTask(data): Observable<any> {
    let API_URL = `${this.apiUrl}/new-request`;
    return this.http.post(API_URL, data)
      .pipe(
        catchError(this.error)
      )
  }
   // Read
  getAllRequest() {
    return this.http.get(`${this.apiUrl}/get-request`);
  }

  getAllRequestByClient(id) {
    return this.http.get(`${this.apiUrl}/get-requests/${id}`);
  }

  // Update
  updateTask(id, data): Observable<any> {
    let API_URL = `${this.apiUrl}/update-task/${id}`;
    return this.http.put(API_URL, data, { headers: this.headers }).pipe(
      catchError(this.error)
    )
  }

  // Delete
  deleteTask(id): Observable<any> {
    var API_URL = `${this.apiUrl}/delete-task/${id}`;
    return this.http.delete(API_URL).pipe(
      catchError(this.error)
    )
  }

  createUser(data): Observable<any> {
    let API_URL = `${this.apiUrl}/new-user`;
    return this.http.post(API_URL, data)
      .pipe(
        catchError(this.error)
      )
  }

   
  getUsers(){
    return this.http.get(`${this.apiUrl}/get-users`);
  }

     
  getUsersById(id){
    return this.http.get(`${this.apiUrl}/get-user/${id}`);
  }

  deleteUserById(id){
    return this.http.get(`${this.apiUrl}/delete/user/${id}`);
  }

  updateUserById(data, id){
    let API_URL = `${this.apiUrl}/user/update/${id}`;
    return this.http.post(API_URL, data)
      .pipe(
        catchError(this.error)
      )
  }

  loginUsers(data){
   // return this.http.post(`${this.apiUrl}/login`);
    let API_URL = `${this.apiUrl}/login`;
    return this.http.post(API_URL, data)
      .pipe(
        catchError(this.error)
      )
  }


  getSerivceProudct(){
    return this.http.get(`${this.apiUrl}/all-services`);
  }

  createProduct(data){
    let API_URL = `${this.apiUrl}/new-product`;
    return this.http.post(API_URL, data)
      .pipe(
        catchError(this.error)
      )
  }

  getAllProduct(){
    return this.http.get(`${this.apiUrl}/get-product`);
  }

  createService(data){
    let API_URL = `${this.apiUrl}/new-service`;
    return this.http.post(API_URL, data)
      .pipe(
        catchError(this.error)
      )
  }
  
  getServiceByProduct(data){
    return this.http.get(`${this.apiUrl}/service/${data}`);
  }
  

  getUserRoles(){
    return this.http.get(`${this.apiUrl}/user-roles`);
  }

  getCustomers(){
      return this.http.get(`${this.apiUrl}/customers`);
  }

  addCustomer(data){
    let API_URL = `${this.apiUrl}/new-customer`;
    return this.http.post(API_URL, data)
      .pipe(
        catchError(this.error)
      )
  }

  addEmployee(data){
    let API_URL = `${this.apiUrl}/add-employee`;
    return this.http.post(API_URL, data)
      .pipe(
        catchError(this.error)
      )
  }

  getEmployeeByClientId(data){
    return this.http.get(`${this.apiUrl}/get-employees/${data}`);
  }
  
  bulkuploadrequest(data){
    let API_URL = `${this.apiUrl}/bulk-new-request`;
    return this.http.post(API_URL, data)
      .pipe(
        catchError(this.error)
      )
  }

  bulkuploadrequestForClient(data) {
    let API_URL = `${this.apiUrl}/bulk-new-requests/client/${data.id}`;
    return this.http.post(API_URL, {d:data.d})
      .pipe(
        catchError(this.error)
      )
  }

  getRequestForAnalyst(data) { 
    return this.http.get(`${this.apiUrl}/get-requests/analyst/${data}`);
  }

  updateRequestById(data) { 
    return this.http.get(`${this.apiUrl}/get-request/${data}`);
  }   

  getRequestStatusById(data) { 
    return this.http.get(`${this.apiUrl}/get-request_status/${data}`);
  }    
  
  updateRequestStatus(data){
    let API_URL = `${this.apiUrl}/update_request_status/${data.request_id}`;
    return this.http.post(API_URL, data)
      .pipe(
        catchError(this.error)
      )
  }


  updateRequestByRequestId(data,id){
    let API_URL = `${this.apiUrl}/update_request/${id}`;
    return this.http.post(API_URL, data)
      .pipe(
        catchError(this.error)
      )
  }

  deleteByRequestId(id){
    return this.http.get(`${this.apiUrl}/delete_request/${id}`);
  }

  requestFilter(data){
    let API_URL = `${this.apiUrl}/filter/request`;
    return this.http.post(API_URL, data)
      .pipe(
        catchError(this.error)
      )
  }
  requestFilterAnalyst(data,id){
    let API_URL = `${this.apiUrl}/filter/request/analyst/${id}`;
    return this.http.post(API_URL, data)
      .pipe(
        catchError(this.error)
      )
  }
  
  listOfAnalyst(){
    return this.http.get(`${this.apiUrl}/analysts`);
  }

  
  requestFilterclient(data,id){
    let API_URL = `${this.apiUrl}/filter/request/client/${id}`;
    return this.http.post(API_URL, data)
      .pipe(
        catchError(this.error)
      )
  }

  userFilter(data){
    let API_URL = `${this.apiUrl}/users/filter`;
    return this.http.post(API_URL, data)
      .pipe(
        catchError(this.error)
      )
  }

  customerFilter(data){
    let API_URL = `${this.apiUrl}/customer/filter`;
    return this.http.post(API_URL, data)
      .pipe(
        catchError(this.error)
      )
  }

  productFilter(data){
    let API_URL = `${this.apiUrl}/products/filter`;
    return this.http.post(API_URL, data)
      .pipe(
        catchError(this.error)
      )
  }

  employeeFilter(data,id){
    let API_URL = `${this.apiUrl}/employee/filter/${id}`;
    return this.http.post(API_URL, data)
      .pipe(
        catchError(this.error)
      )
  }

  getCustomerById(id){
    return this.http.get(`${this.apiUrl}/customer/${id}`);
  }
  
  // Handle Errors 
  error(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }

}
