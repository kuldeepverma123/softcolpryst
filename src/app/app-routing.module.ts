import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutfrontendComponent } from './layout/layoutfrontend/layoutfrontend/layoutfrontend.component';
import { LayoutadminpanelComponent } from './layout/layoutadminpanel/layoutadminpanel.component';
import { AuthGuardService } from './services/auth-guard.service';
import { LayoutclientpanelComponent } from './layout/layoutclientpanel/layoutclientpanel.component';
import { LayoutanalystspanelComponent } from './layout/layoutanalystspanel/layoutanalystspanel.component';
import { AuthGuardClientService } from './services/auth-guard-client.service';
import { AuthGuardAnalystsService } from './services/auth-guard-analysts.service';

const routes: Routes = [
  {
    path: '',
    component: LayoutfrontendComponent,    
    children: [
      { 
        path: '', 
        pathMatch: 'full', 
        redirectTo: '/auth'
      },
      { 
        path: 'auth', 
        loadChildren: () => import('./pages/login/login.module').then(l => l.LoginModule)
      },   
    ]
  },
  {
    path: 'admin',
    canActivate: [AuthGuardService],
    component: LayoutadminpanelComponent,    
    children: [    
      // { 
      //   path: '', 
      //   pathMatch: 'full', 
      //   redirectTo: '/admin/welcome'
      // },        
      { 
        path: 'requests',
        canActivate: [AuthGuardService], 
        loadChildren: () => import('./pages/admin/application/application.module').then(application => application.ApplicationModule) 
      }, 
      { 
        path: 'create-requests',
        canActivate: [AuthGuardService], 
        loadChildren: () => import('./pages/admin/create-application/create-application.module').then(createapplication => createapplication.CreateApplicationModule) 
      },
      { 
        path: 'assigned-requests',
        canActivate: [AuthGuardService], 
        loadChildren: () => import('./pages/admin/applied-application/applied-application.module').then(appliedapplication => appliedapplication.AppliedApplicationModule) 
      },
      { 
        path: 'billing',
        canActivate: [AuthGuardService], 
        loadChildren: () => import('./pages/admin/billing/billing.module').then(billing => billing.BillingModule) 
      },
      { 
        path: 'quality',
        canActivate: [AuthGuardService], 
        loadChildren: () => import('./pages/admin/quality/quality.module').then(quality => quality.QualityModule) 
      },
      { 
        path: 'billing',
        canActivate: [AuthGuardService], 
        loadChildren: () => import('./pages/admin/billing/billing.module').then(billing => billing.BillingModule) 
      },
      { 
        path: 'customers',
        canActivate: [AuthGuardService], 
        loadChildren: () => import('./pages/admin/customers/customers.module').then(customers => customers.CustomersModule) 
      },
      { 
        path: 'personal',
        canActivate: [AuthGuardService], 
        loadChildren: () => import('./pages/admin/personal/personal.module').then(personal => personal.PersonalModule) 
      },
      { 
        path: 'products',
        canActivate: [AuthGuardService], 
        loadChildren: () => import('./pages/admin/product/product.module').then(product => product.ProductModule) 
      }
    ]
  },
  {
    path: 'client',
    canActivate: [AuthGuardClientService],
    component: LayoutclientpanelComponent,    
    children: [           
      { 
        path: 'requests',
        canActivate: [AuthGuardClientService], 
        loadChildren: () => import('./pages/client/request/request.module').then(request => request.RequestModule) 
      },
      { 
        path: 'employees',
        canActivate: [AuthGuardClientService], 
        loadChildren: () => import('./pages/client/employees/employees.module').then(employees => employees.EmployeesModule) 
      }
    ]
  },
  {
    path: 'analysts',
    canActivate: [AuthGuardAnalystsService],  
    component: LayoutanalystspanelComponent,    
    children: [           
      { 
        path: 'requests',
        canActivate: [AuthGuardAnalystsService], 
        loadChildren: () => import('./pages/analysts/application/application.module').then(application => application.ApplicationModule) 
      }
    ]
  } 
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
