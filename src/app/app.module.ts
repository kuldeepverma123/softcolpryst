import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IconsProviderModule } from './icons-provider.module';
import { NgZorroAntdModule, NZ_I18N, en_US } from 'ng-zorro-antd';
import { FileDropDirective, FileSelectDirective} from 'ng2-file-upload';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { LayoutadminpanelComponent } from './layout/layoutadminpanel/layoutadminpanel.component';
import { LayoutfrontendComponent } from './layout/layoutfrontend/layoutfrontend/layoutfrontend.component';
import { LayoutclientpanelComponent } from './layout/layoutclientpanel/layoutclientpanel.component';

// import ngx-translate and the http loader
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { LayoutanalystspanelComponent } from './layout/layoutanalystspanel/layoutanalystspanel.component';
import { TokenInterceptor } from './token-interceptor';

registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
    LayoutadminpanelComponent,
    LayoutclientpanelComponent,
    LayoutfrontendComponent,
    LayoutanalystspanelComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    IconsProviderModule,
    NgZorroAntdModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })    
  ],
  providers: [{ provide: NZ_I18N, useValue: en_US},{ provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true }],
  bootstrap: [AppComponent],
})
export class AppModule { 

}

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
