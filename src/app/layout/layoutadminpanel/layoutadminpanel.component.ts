import { Component, OnInit, HostListener } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';

import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-layoutadminpanel',
  templateUrl: './layoutadminpanel.component.html',
  styleUrls: ['./layoutadminpanel.component.scss']
})
export class LayoutadminpanelComponent implements OnInit {
  isCollapsed = false

  innerWidth: any
  drawerVisible = false
  drawerPlacement = 'left'

  customerRouteArray: any = ['/user/customers/list', '/user/customers/new']

  customerRouteActive: any

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerWidth = window.innerWidth
  }  

  openMap: { [name: string]: boolean } = {
    sub1: false,
    sub2: false,
    sub3: false,
    sub4: false,
    sub5: false
  }  

  selectBranchForm: FormGroup

  constructor(
    private authService: AuthenticationService,
    private translate: TranslateService,
    private router: Router,
    private fb: FormBuilder
  ) {  

  }  
  
  drawerOpen() {
    this.drawerVisible = true
  }

  drawerClose() {
    this.drawerVisible = false
  }  

  ngOnInit() {
    this.innerWidth = window.innerWidth
    // console.log(this.router.url)
    this.selectBranchFormInit()
  }  

  findCurrentRoute(){
    for(let i = 0; i < this.customerRouteArray.length; i++) {
      console.log(this.customerRouteArray[i])
      if(this.router.url == this.customerRouteArray[i]) {
        console.log('true')
        this.customerRouteActive = true
        return
      } else {
        console.log('false')
        this.customerRouteActive = false
      }
    }
  }

  openHandler(value: string) {
    for (const key in this.openMap) {
      if (key !== value) {
        this.openMap[key] = false;
      }
    }
  }  

  userLogout() {
    localStorage.removeItem('userRes')
    this.authService.logout()
  }

  useLanguage(language: string) {
    this.translate.use(language)
    localStorage.setItem('acLanguage', language)
    window.location.reload()
  }    

  selectBranchFormInit() {
    this.selectBranchForm = this.fb.group({
      select_branch: ['1']
    })  
  }    

}
