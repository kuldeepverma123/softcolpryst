import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutanalystspanelComponent } from './layoutanalystspanel.component';

describe('LayoutanalystspanelComponent', () => {
  let component: LayoutanalystspanelComponent;
  let fixture: ComponentFixture<LayoutanalystspanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayoutanalystspanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutanalystspanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
