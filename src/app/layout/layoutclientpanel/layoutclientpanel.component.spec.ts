import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutclientpanelComponent } from './layoutclientpanel.component';

describe('LayoutclientpanelComponent', () => {
  let component: LayoutclientpanelComponent;
  let fixture: ComponentFixture<LayoutclientpanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayoutclientpanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutclientpanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
