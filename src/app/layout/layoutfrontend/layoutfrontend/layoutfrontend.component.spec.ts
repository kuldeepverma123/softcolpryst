import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutfrontendComponent } from './layoutfrontend.component';

describe('LayoutfrontendComponent', () => {
  let component: LayoutfrontendComponent;
  let fixture: ComponentFixture<LayoutfrontendComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayoutfrontendComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutfrontendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
