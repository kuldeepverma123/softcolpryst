import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../../../../api.service';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
@Component({
  selector: 'app-application-delete',
  templateUrl: './application-delete.component.html',
  styleUrls: ['./application-delete.component.scss']
})
export class ApplicationDeleteComponent implements OnInit {

  customerconsultdelete: FormGroup
  applicationEdit :any = []
  id : any
  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private service: ApiService,
    private route: ActivatedRoute, 
    private routers: Router,    
    private notification: NzNotificationService    
  ) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.languageTranslate()
    this.getRequestData()

    
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  formVariablesInit() {
    this.customerconsultdelete = this.fb.group({
      // department: ['1'],
      // no_card: [null, [Validators.required]],
      // name: [null, [Validators.required]],
      // last_name: [null, [Validators.required]],
      // phone: [null, [Validators.required]],
      // mobile: [null, [Validators.required]],
      // address: [null, [Validators.required]],
      // position: [null, [Validators.required]],
      // area: [null, [Validators.required]],
      // profession: [null, [Validators.required]],
      // mail: [null, [Validators.required]],
      // city: [null, [Validators.required]],
      // administrator: [null],
      // observer: [null]
    })      
  }

  getRequestData(){
    this.service.updateRequestById(this.id).subscribe((response) => {

      console.log("rsponse data", response);
     
      this.applicationEdit.neighborhood = response['data'][0].neighborhood;
      this.applicationEdit.details = response['data'][0].details;
      this.applicationEdit.email = response['data'][0].email;
      this.applicationEdit.phone = response['data'][0].phone;
      this.applicationEdit.mobile = response['data'][0].mobile;
      this.applicationEdit.departmant =response['data'][0].departmant;
      this.applicationEdit.city = response['data'][0].city;
      this.applicationEdit.identity_document =response['data'][0].identity_document;
      this.applicationEdit.observations = response['data'][0].observations;
      this.applicationEdit.description = response['data'][0].description;
      this.applicationEdit.position = response['data'][0].position;
      this.applicationEdit.direction = response['data'][0].direction;
      this.applicationEdit.typeofDocument = response['data'][0].typeofDocument;
      this.applicationEdit.ducumentNumber = response['data'][0].ducumentNumber;
      this.applicationEdit.firstname = response['data'][0].firstname;
      this.applicationEdit.business = response['data'][0].cbusiness;
      this.applicationEdit.lastname = response['data'][0].lastname;
      this.applicationEdit.expenditureCenter = response['data'][0].expenditureCenter;
      this.applicationEdit.servicetype = response['data'][0].servicetype;      
      this.applicationEdit.services = response['data'][0].services;      
     
      
      console.log("response",  this.applicationEdit)
    });
  }

  deleteCustomer(){
    this.service.deleteByRequestId(this.id).subscribe((response) => {
        this.routers.navigate(['/admin/requests'])
        this.successNotification('success')
        
       }, 
      (error) => {
        this.errorNotification('error');
        });
    
  }

  successNotification(type: string): void {
    let acLanguage = localStorage.getItem('acLanguage')
   let tit, message;
  console.log(acLanguage)
  if(acLanguage == "es"){

  tit = 'Solicitud eliminada correctamente'

  }else {
    tit = 'Request successfully removed'
  }

this.notification.create(
  type,
  tit,
  message
   );
  }

  errorNotification(type: string): void {
    let acLanguage = localStorage.getItem('acLanguage')
    let tit, message;
      console.log(acLanguage)
      if(acLanguage == "es"){

      tit = 'Solicitar eliminar error'
      message = 'algo está mal'

      }else {
        tit = 'Request remove error'
        message = 'something is wrong'
      }

    this.notification.create(
      type,
      tit,
      message
    );
  }


}







