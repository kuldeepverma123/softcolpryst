import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../../api.service';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FileUploader, FileItem, ParsedResponseHeaders  } from 'ng2-file-upload';
import { FileUploadModule } from "ng2-file-upload"; 
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { HttpClient } from '@angular/common/http'
@Component({
  selector: 'app-application-edit',
   templateUrl: './application-edit.component.html',
  styleUrls: ['./application-edit.component.scss']
})
export class ApplicationEditComponent implements OnInit {
  public  uploader:FileUploader = new FileUploader({url:'http://18.216.180.72:3000/api/upload/photo', itemAlias: 'attach_photo' });
  public docUploader:FileUploader = new FileUploader({url:'http://18.216.180.72:3000/api/upload/doc', itemAlias: 'attach_document'});
    state = {
      value: 1, 
    };  applicationEdit: FormGroup
      id : any
      attachphoto : any
      attachdoc : any
      fileName : ""
      isSpinning : boolean


  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private service: ApiService,
    private route: ActivatedRoute,    
    private http: HttpClient,
    private notification: NzNotificationService
  ) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.languageTranslate()
    this.formVariablesInit()
    this.getRequestData()

    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;
    };

    this.uploader.onCompleteItem = (item: any, status: any) => {
      console.log('Uploaded File Details:', item);
    };

    this.docUploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;
    };

    this.docUploader.onCompleteItem = (item: any, status: any) => {
      console.log('Uploaded File Details:', item);
    };

    this.uploader.onSuccessItem = (item, response, status, headers) => this.onSuccessItem(item, response, status, headers);
    this.docUploader.onSuccessItem = (item, response, status, headers) => this.onSuccessItem(item, response, status, headers);
 
  }

  onSuccessItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
    let data = JSON.parse(response); //success server response
    console.log(data);
     if(data){
        let finame =  data.data.fieldname;
         if(finame == "attach_photo"){
              this.attachphoto  = data.data.filename;
         } if( finame == "attach_document"){
             this.attachdoc  = data.data.filename;
         }
      }
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  formVariablesInit() {
    this.applicationEdit = this.fb.group({
      business: [null], 
      lastname: [null],
      neighborhood: [null],
      details: [null],
      email: [null],
      phone: [null],
      mobile: [null],
      departmant: ['1'],
      city: [null],
      identity_document: [null],
      observations: [null],
      position: [null],
      description: [null],
      direction : [null],
      typeofDocument :[null],
      ducumentNumber :[null],
      firstname :[null],
      expenditureCenter : [null]
    })      
  }

  saveCustomer() {
    for (const i in this.applicationEdit.controls) {
      this.applicationEdit.controls[i].markAsDirty()
      this.applicationEdit.controls[i].updateValueAndValidity()
    }
    if(this.applicationEdit.valid) {
      console.log('this.applicationEdit.value', this.applicationEdit.value)
    }


      //   if (this.createapplicationAddform.invalid) {
      //     return;
      // }
      this.uploader.uploadAll();
      this.docUploader.uploadAll();

        this.isSpinning = true;
       console.log("imagedata",this.uploader);
       console.log("imagedata",this.docUploader);

       setTimeout (() => {

        
        this.applicationEdit.value.attach_photo = this.attachphoto;

        this.applicationEdit.value.attachdoc = this.attachdoc;
        
        console.log("--->>",this.attachphoto);
        console.log("--->>",this.attachdoc);
        console.log("--->>",this.applicationEdit.value);
        this.service.updateRequestByRequestId(this.applicationEdit.value,this.id).subscribe(
          (response) =>{
            console.log(response)
            this.successNotification('success')
           // this.applicationEdit.reset();
            this.isSpinning = false;
          }, 
          (error) => {
            this.errorNotification('error');
            this.isSpinning = false;
          }
        )
      }, 2000);

  }

  successNotification(type: string): void {
    let acLanguage = localStorage.getItem('acLanguage')
   let tit, message;
  console.log(acLanguage)
  if(acLanguage == "es"){

  tit = 'Solicitud de creación exitosa'

  }else {
    tit = 'Request create successful'
  }

this.notification.create(
  type,
  tit,
  message
   );
  }

  errorNotification(type: string): void {
    let acLanguage = localStorage.getItem('acLanguage')
    let tit, message;
      console.log(acLanguage)
      if(acLanguage == "es"){

      tit = 'Solicitud crear falla'
      message = 'algo está mal'

      }else {
        tit = 'Request create fail'
        message = 'something is wrong'
      }

    this.notification.create(
      type,
      tit,
      message
    );
  }

  clearFilter() {
    this.applicationEdit.reset()
    this.applicationEdit.patchValue({
      customer_type: ['1'],
      customer_tax_exempt: ['no']
    })
  }

  getRequestData(){
    this.service.updateRequestById(this.id).subscribe((response) => {
      console.log("response", response['data'])
      this.applicationEdit.controls['neighborhood'].setValue(response['data'][0].neighborhood);
      this.applicationEdit.controls['details'].setValue(response['data'][0].details);
      this.applicationEdit.controls['email'].setValue(response['data'][0].email);
      this.applicationEdit.controls['phone'].setValue(response['data'][0].phone);
      this.applicationEdit.controls['mobile'].setValue(response['data'][0].mobile);
      this.applicationEdit.controls['departmant'].setValue(response['data'][0].departmant);
      this.applicationEdit.controls['city'].setValue(response['data'][0].city);
      this.applicationEdit.controls['identity_document'].setValue(response['data'][0].identity_document);
      this.applicationEdit.controls['observations'].setValue(response['data'][0].observations);
      this.applicationEdit.controls['description'].setValue(response['data'][0].description);
      this.applicationEdit.controls['position'].setValue(response['data'][0].position);
      this.applicationEdit.controls['direction'].setValue(response['data'][0].direction);
      this.applicationEdit.controls['typeofDocument'].setValue(response['data'][0].typeofDocument);
      this.applicationEdit.controls['ducumentNumber'].setValue(response['data'][0].ducumentNumber);
      this.applicationEdit.controls['firstname'].setValue(response['data'][0].firstname);
      this.applicationEdit.controls['business'].setValue(response['data'][0].business);
      this.applicationEdit.controls['lastname'].setValue(response['data'][0].lastname);
      this.applicationEdit.controls['expenditureCenter'].setValue(response['data'][0].expenditureCenter);   
      console.log('this.applicationEdit.value', this.applicationEdit.value)
    })
  }
}

