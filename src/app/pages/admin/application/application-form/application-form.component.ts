import { Component, OnInit, Input } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
   selector: 'app-application-form',
   templateUrl: './application-form.component.html',
   styleUrls: ['./application-form.component.scss']
})
export class ApplicationFormComponent implements OnInit {
  @Input() applicationaccountData: any
  applicationForm: FormGroup

  currentDate:any = new Date()  

  // cheque table data 
  chequeTableDatas = []  

  // search account dialog
  
  accountDatas = []

  // applicationaccount search dialog
  searchApplicationaccountVisible = false
  applicationaccountDatas = []  

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.applicationFormInit()

    this.chequeTableData()  

  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  applicationFormInit() {
    this.applicationForm = this.fb.group({
     application_type: ['1'],
     application_tax_exempt: ['no'],
      state: [null, [Validators.required]],
      assigned_to: [null, [Validators.required]],
      business: [null, [Validators.required]],
      name: [null, [Validators.required]],
      type_of_service: [null, [Validators.required]],
      id_card: [null, [Validators.required]],
      chequeTableDatas: []
    })      
  }
  
  saveCheque() {
    for (const i in this.applicationForm.controls) {
      this.applicationForm.controls[i].markAsDirty()
      this.applicationForm.controls[i].updateValueAndValidity()
    }
    if(this.applicationForm.valid) {
      console.log('this.applicationForm.value', this.applicationForm.value)
    }
  }

  chequeTableData() {
    this.chequeTableDatas = []    
  }

  chequeTableDataRemove(item_index) {
    this.chequeTableDatas.splice(item_index, 1)
    this.applicationForm = this.fb.group({
      commentary: [null],
      debit: [null],
      credit: [null]
    })     
  }  


  accountAddInput($event) {
    this.chequeTableDatas.push(
      $event
    )

    this.applicationForm.patchValue({
      chequeTableDatas: $event
    })     

  

}
clearFilter() {
  this.applicationForm.reset()
  this.applicationForm.patchValue({
    application_type: ['1'],
    application_tax_exempt: ['no']
  })
}
}
