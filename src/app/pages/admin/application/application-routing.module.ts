import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApplicationComponent } from './application.component';
import { ApplicationEditComponent } from './application-edit/application-edit.component';
import { ApplicationStatusComponent } from './application-status/application-status.component';
import { ApplicationDeleteComponent } from './application-delete/application-delete.component';



const routes: Routes = [
  {
    path: '',
    component: ApplicationComponent
  },
  {
    path: 'edit/:id',
    component: ApplicationEditComponent
  },
  {
    path: 'status/:id',
    component: ApplicationStatusComponent
  },
  {
    path: 'delete/:id',
    component: ApplicationDeleteComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApplicationRoutingModule { }
