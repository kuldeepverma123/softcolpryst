import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../../api.service';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzNotificationService } from 'ng-zorro-antd/notification';

@Component({
  selector: 'app-application-status',
  templateUrl: './application-status.component.html',
  styleUrls: ['./application-status.component.scss']
})
export class ApplicationStatusComponent implements OnInit {
  joined: boolean
  assignoadvisor: boolean = false
  academicv :boolean = false
  labourv : boolean = false
  preliminary : boolean = false
  finialized : boolean = false
  physical : boolean = false
  approval : boolean = false
  applicationStatus: FormGroup
  id : any
  concept : any
  background :any
  joindate : string = ''
  assignadvisordate : string = ''
  finalizedate : string = ''


  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private service: ApiService,
    private notification: NzNotificationService    
  ) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.languageTranslate()
    this.formVariablesInit()
    this.getStatus()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  formVariablesInit() {
    this.applicationStatus = this.fb.group({
      joined: ['false'],
      assignoadvisor: ['false'],
      academicv : ['false'],
      labourv : ['false'],
      preliminary : ['false'],
      finialized : ['false'],
      physical : ['false'],
      approval : ['false'],
      background: [null, [Validators.required]],
      concept: [null, [Validators.required]],
      request_id : [this.id]
    })      
  } 

  saveApplication() {
    for (const i in this.applicationStatus.controls) {
      this.applicationStatus.controls[i].markAsDirty()
      this.applicationStatus.controls[i].updateValueAndValidity()
    }
    if(this.applicationStatus.valid) {
      
      this.service.updateRequestStatus(this.applicationStatus.value).subscribe((response) => {
            console.log("response", response)
        })
        this.notification.success('Request Status Update Successfully', '')
    }
  } 

  clearFilter() {
    this.applicationStatus.reset()
    this.applicationStatus.patchValue({
      customer_type: ['1'],
      customer_tax_exempt: ['no']
    })
  }
  log(value: string[]): void {
    console.log(value);
  }
   getStatus(){
    this.service.getRequestStatusById(this.id).subscribe((response) => {
    // console.log("response", response['data'])

    let data  = response['data'][0];
 
     if(data.joined == "true"){
      this.joined = true
     }
     if(data.assignoadvisor == "true"){
      this.assignoadvisor = true
     }

     if(data.academicv == "true"){
      this.academicv = true
     }

     if(data.preliminary == "true"){
      this.preliminary = true
     }

     if(data.finialized == "true"){
      this.finialized = true
     }

     if(data.physical == "true"){
      this.physical = true
     }

     if(data.approval == "true"){
      this.approval = true
     }

     if(data.labourv == "true"){
      this.labourv = true
     }

    this.background = data.background
    this.concept = data.concept
    this.joindate = data.joindate
    this.assignadvisordate = data.assignadvisordate
    this.finalizedate = data.finalizedate
    })
  }

}


