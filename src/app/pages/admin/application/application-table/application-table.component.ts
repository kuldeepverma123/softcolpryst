import { Component, OnInit, Input } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-application-table',
  templateUrl: './application-table.component.html',
  styleUrls: ['./application-table.component.scss']
})
export class ApplicationTableComponent implements OnInit {

  @Input() applicationaccountDatas: any

  //  application account form
  ApplicationAccountForm: FormGroup  
  ApplicationAccountVisible = false
  today: number = Date.now();
  request: any = [];
  // search
  search_title = ''

  applicationaccountDisplayData: any

  sortName: string | null = null
  sortValue: string | null = null

  deleteConfirmVisible = false

  constructor(
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.ApplicationAccountFormInit()

    this.applicationAccountData()
  }
  
  //  application account form
  ApplicationAccountFormInit() {
    this.ApplicationAccountForm = this.fb.group({
      business: [null, [Validators.required]],
      user: [null, [Validators.required]],
      name: [null, [Validators.required]],
      type_service: [null, [Validators.required]],
      application_correlative_checks: [null, [Validators.required]]
    })      
  }  

  ApplicationAccountOpen() {
    this.ApplicationAccountVisible = true
  }

  ApplicationAccountClose() {
    this.ApplicationAccountForm.reset()
    this.ApplicationAccountVisible = false
  }      

  ApplicationAccountSave() {    
    for (const i in this.ApplicationAccountForm.controls) {
      this.ApplicationAccountForm.controls[i].markAsDirty()
      this.ApplicationAccountForm.controls[i].updateValueAndValidity()
    }          

    if(this.ApplicationAccountForm.valid) {
      this.ApplicationAccountClose()
    }
  }

  applicationAccountData() {
    this.applicationaccountDisplayData = [...this.applicationaccountDatas]
    console.log('table component ', this.applicationaccountDisplayData)
  }

  keyUpSearch() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { state: string; assigned_to: string; days_passed: string; id_card: string; business: string; user: string; name: string; type_service: string }) => {
      return (
        item.business.toLowerCase().indexOf(searchLower) !== -1 ||
        item.id_card.toLowerCase().indexOf(searchLower) !== -1 ||
        item.assigned_to.toLowerCase().indexOf(searchLower) !== -1 ||
        item.days_passed.toLowerCase().indexOf(searchLower) !== -1 ||
        item.state.toLowerCase().indexOf(searchLower) !== -1 ||
        item.user.toLowerCase().indexOf(searchLower) !== -1 ||
        item.name.toLowerCase().indexOf(searchLower) !== -1 ||
        item.type_service.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.applicationaccountDatas.filter((item: { state: string; assigned_to: string; days_passed: string;  id_card: string; business: string; user: string; name: string; type_service: string }) => filterFunc(item))

    this.applicationaccountDisplayData = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    ) 
    
    if(searchLower.length == 0) {
      this.applicationaccountDisplayData = [...this.applicationaccountDatas]
    }    
  }  

  // delete confirm
  deleteConfirmOpen() {
    this.deleteConfirmVisible = true
  }

  deleteConfirmClose() {
    this.deleteConfirmVisible = false
  }  
  

}


