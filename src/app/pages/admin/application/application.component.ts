import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ApiService } from '../../../api.service';
@Component({
  selector: 'app-application',
  templateUrl: './application.component.html', 
  styleUrls: ['./application.component.scss']
})
export class  ApplicationComponent implements OnInit {

  applicationaccountDatas: any = []
  activeCollapse = false  
  applicationForm: FormGroup
  products : any  = []
  services : any = []
  areaSelectValue: any
  typeOfServiceSelectValue: any
  assigned_to :any = []
  
  constructor(
    private translate: TranslateService,
    private service: ApiService,
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.applicationaccountData() 
    this.applicationFormInit()
    this.getProduct()
    this.listOfAllAnalyst()   
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }

  applicationFormInit() {
    this.applicationForm = this.fb.group({
     application_type: ['1'],
     application_tax_exempt: ['no'],
      assigned_to: [null],
      area: [null],
      name: [null],
      type_of_service: [null],
      id_card: [null]
    })      
  }

  saveCheque() {
    for (const i in this.applicationForm.controls) {
      this.applicationForm.controls[i].markAsDirty()
      this.applicationForm.controls[i].updateValueAndValidity()
    }
    if(this.applicationForm.valid) {
      console.log('this.applicationForm.value', this.applicationForm.value)
      this.service.requestFilter(this.applicationForm.value).subscribe((response) => {
        console.log("response", response['data'])
        this.applicationaccountDatas = response['data']
           var i = 0;
        response['data'].forEach(element => {
           console.log(element.assignDate);
          let date1 = new Date();
          let date2 = new Date(element.assignDate);
           var Difference_In_Time = date1.getTime() - date2.getTime();
  
           var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24); 
           this.applicationaccountDatas[i].Difference_In_Days = Math.round(Difference_In_Days);
          });
  
        console.log("after", this.applicationaccountDatas)
      })
    }
  }
  
  showFilter() {
    this.activeCollapse = !this.activeCollapse
  } 

  getProduct() {
    this.service.getAllProduct().subscribe((response) => {
      console.log("response", response['data'])
      this.products = response['data']
      console.log("after", this.products)
    })
   }

   getService(data) {
    this.service.getServiceByProduct(data).subscribe((response) => {
     // console.log("response", response['data'])
      this.services = response['data']
      console.log("after  services", this.services)
    })
   }

   typeOfSelectService($event) {
    console.log("$event",$event)
    this.typeOfServiceSelectValue = $event
    //this.createapplicationAddform.get('type_of_service').reset()
    //console.log("-->areaSelectValue",this.areaSelectValue);
  }  

   areaSelect($event) {
    this.areaSelectValue = $event
    this.getService($event)
    this.applicationForm.get('type_of_service').reset()
    //console.log("-->areaSelectValue",this.areaSelectValue);
  }
  

  listOfAllAnalyst() {
    this.service.listOfAnalyst().subscribe((response) => {
      console.log("response", response['data'])
      this.assigned_to = response['data']
      console.log("after", this.assigned_to)
    })
  }
  
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
  }
  clearFilter() {
  this.applicationForm.reset()
  this.applicationForm.patchValue({
    application_type: ['1'],
    application_tax_exempt: ['no']
  })
}
  submitForm() {
    this.applicationaccountDatas = []
  }
  
  applicationaccountData() {

    // this.applicationaccountDatas = [
    //   {
    //     business: 'Merco group',
    //     user: 'John Doe',
    //     application: 'Banco Financiera Comercial',
    //     type_service: 'Business associates',
    //     name: 'John',
    //     id_card: '458689',
    //     state: 'Finalized',
    //     days_passed: '4 Days',
    //     assigned_to: 'Vanessa ariza'
    //   },
    //   {
    //     business: 'Merco group',
    //     user: 'John Doe',
    //     application: 'Banco Financiera Comercial',
    //     type_service: 'Business associates',
    //     name: 'John',
    //     id_card: '458689',
    //     state: 'Finalized',
    //     days_passed: '4 Days',
    //     assigned_to: 'Vanessa ariza'
    //   }                
    // ]

    // console.log("=== dss ",this.applicationaccountDatas);

     this.service.getAllRequest().subscribe((response) => {
      console.log("response", response['data'])
      this.applicationaccountDatas = response['data']
         var i = 0;
      response['data'].forEach(element => {
         console.log(element.assignDate);
        let date1 = new Date();
        let date2 = new Date(element.assignDate);
         var Difference_In_Time = date1.getTime() - date2.getTime();

         var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24); 
         this.applicationaccountDatas[i].Difference_In_Days = Math.round(Difference_In_Days);
        });

      console.log("after", this.applicationaccountDatas)
      // this.applicationaccountDatas = response['data'];
      // console.log("=== dss ",this.applicationaccountDatas);
    })

    
  }
}

   
  


