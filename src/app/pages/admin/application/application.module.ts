import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgZorroAntdModule } from 'ng-zorro-antd';

// import ngx-translate and the http loader
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import { HttpClient, HttpClientModule } from '@angular/common/http';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ApplicationRoutingModule } from './application-routing.module';
import { ApplicationComponent } from './application.component';
import { ApplicationTableComponent } from './application-table/application-table.component';
import { ApplicationFormComponent } from './application-form/application-form.component';
import { ApplicationEditComponent } from './application-edit/application-edit.component';
import { ApplicationDeleteComponent } from './application-delete/application-delete.component';
import { ApplicationStatusComponent } from './application-status/application-status.component';


@NgModule({
  declarations: [ApplicationComponent, ApplicationTableComponent, ApplicationFormComponent, ApplicationEditComponent, ApplicationDeleteComponent, ApplicationStatusComponent],
  imports: [
    NgZorroAntdModule,
    HttpClientModule,
    CommonModule,
    ApplicationRoutingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class ApplicationModule { }
// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}