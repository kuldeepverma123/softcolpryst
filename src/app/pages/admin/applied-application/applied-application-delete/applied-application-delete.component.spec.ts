import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppliedApplicationDeleteComponent } from './applied-application-delete.component';

describe('AppliedApplicationDeleteComponent', () => {
  let component: AppliedApplicationDeleteComponent;
  let fixture: ComponentFixture<AppliedApplicationDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppliedApplicationDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppliedApplicationDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
