import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-applied-application-delete',
  templateUrl: './applied-application-delete.component.html',
  styleUrls: ['./applied-application-delete.component.scss']
})
export class AppliedApplicationDeleteComponent implements OnInit {

  appliedapplicationdelete: FormGroup

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder    
  ) { }

  ngOnInit() {
    this.languageTranslate()

    this.formVariablesInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  formVariablesInit() {
    this.appliedapplicationdelete = this.fb.group({
      // department: ['1'],
      // no_card: [null, [Validators.required]],
      // name: [null, [Validators.required]],
      // last_name: [null, [Validators.required]],
      // phone: [null, [Validators.required]],
      // mobile: [null, [Validators.required]],
      // address: [null, [Validators.required]],
      // position: [null, [Validators.required]],
      // area: [null, [Validators.required]],
      // profession: [null, [Validators.required]],
      // mail: [null, [Validators.required]],
      // city: [null, [Validators.required]],
      // administrator: [null],
      // observer: [null]
    })      
  }

  saveCustomer() {
    for (const i in this.appliedapplicationdelete.controls) {
      this.appliedapplicationdelete.controls[i].markAsDirty()
      this.appliedapplicationdelete.controls[i].updateValueAndValidity()
    }
    if(this.appliedapplicationdelete.valid) {
      //console.log('this.appliedapplicationdelete.value', this.appliedapplicationdelete.value)
    }
  }

  clearFilter() {
    this.appliedapplicationdelete.reset()
    this.appliedapplicationdelete.patchValue({
      //department: ['1'],
      //customer_tax_exempt: ['no']
    })
  }



}







