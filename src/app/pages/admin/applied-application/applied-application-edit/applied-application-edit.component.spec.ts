import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppliedApplicationEditComponent } from './applied-application-edit.component';

describe('AppliedApplicationEditComponent', () => {
  let component: AppliedApplicationEditComponent;
  let fixture: ComponentFixture<AppliedApplicationEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppliedApplicationEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppliedApplicationEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
