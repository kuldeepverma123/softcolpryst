import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-applied-application-edit',
 templateUrl: './applied-application-edit.component.html',
 styleUrls: ['./applied-application-edit.component.scss']
})
export class AppliedApplicationEditComponent implements OnInit {

  appliedapplicationEdit: FormGroup

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder    
  ) { }

  ngOnInit() {
    this.languageTranslate()

    this.formVariablesInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  formVariablesInit() {
    this.appliedapplicationEdit = this.fb.group({
      customer_type: ['1'],
      customer_tax_exempt: ['no'],
      reference_report: [null, [Validators.required]],
      expiration_date: [null, [Validators.required]],
      name: [null, [Validators.required]],
      id_card: [null, [Validators.required]],
      age: [null, [Validators.required]],
      expendition_date: [null, [Validators.required]],
      d_o_b: [null, [Validators.required]],
      blood_type: [null, [Validators.required]],
      attach_photo: [null],
      driving_license: [null, [Validators.required]],
      military_card: [null, [Validators.required]],
      assigned_to: [null, [Validators.required]],
      marital_status: [null, [Validators.required]],
      particular_signal: [null, [Validators.required]],
      social_page: [null, [Validators.required]],
      phone: [null, [Validators.required]],
      mobile: [null, [Validators.required]],
      email: [null, [Validators.required]],
      passport: [null, [Validators.required]],
      visa: [null, [Validators.required]],
      state: [null, [Validators.required]]
    })      
  }

  saveCustomer() {
    for (const i in this.appliedapplicationEdit.controls) {
      this.appliedapplicationEdit.controls[i].markAsDirty()
      this.appliedapplicationEdit.controls[i].updateValueAndValidity()
    }
    if(this.appliedapplicationEdit.valid) {
      console.log('this.appliedapplicationEdit.value', this.appliedapplicationEdit.value)
    }
  }

  clearFilter() {
    this.appliedapplicationEdit.reset()
    this.appliedapplicationEdit.patchValue({
      customer_type: ['1'],
      customer_tax_exempt: ['no']
    })
  }



}


