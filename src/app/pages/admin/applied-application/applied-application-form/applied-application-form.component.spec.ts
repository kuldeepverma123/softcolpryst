import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppliedApplicationFormComponent } from './applied-application-form.component';

describe('AppliedApplicationFormComponent', () => {
  let component: AppliedApplicationFormComponent;
  let fixture: ComponentFixture<AppliedApplicationFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppliedApplicationFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppliedApplicationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
