 import { Component, OnInit, Input } from '@angular/core';

  import { TranslateService } from '@ngx-translate/core';
  
  import { FormBuilder, FormGroup, Validators } from '@angular/forms';
  
  @Component({
    selector: 'app-applied-application-form',
    templateUrl: './applied-application-form.component.html',
    styleUrls: ['./applied-application-form.component.scss']
  })
  export class AppliedApplicationFormComponent  implements OnInit {
    @Input() appliedapplicationaccountData: any
    appliedapplicationForm: FormGroup
  
    currentDate:any = new Date()  
  
    // appliedapplication table data 
    appliedapplicationTableDatas = []  
  
    // search account dialog
    
    accountDatas = []
  
    // appliedapplicationaccount search dialog
    searchAppliedApplicationaccountVisible = false
    appliedapplicationaccountDatas = []  
  
    constructor(
      private translate: TranslateService,
      private fb: FormBuilder
    ) { 
  
    }
  
    ngOnInit() {
      this.languageTranslate()
  
      this.appliedapplicationFormInit()
  
      this.appliedapplicationTableData()  
  
    }
  
    languageTranslate() {
      let acLanguage = localStorage.getItem('acLanguage')
  
      if(acLanguage != null && acLanguage != '') {
        this.translate.use(acLanguage)
      } else {
        this.translate.setDefaultLang('es')
      }    
    }
    
    appliedapplicationFormInit() {
      this.appliedapplicationForm = this.fb.group({
       application_type: ['1'],
       application_tax_exempt: ['no'],
       candidate_id: [null, [Validators.required]],
       type_of_service: [null, [Validators.required]],
       candidate: [null, [Validators.required]],
       appliedapplicationTableDatas: []
      })      
    }
    
    saveApplied() {
      for (const i in this.appliedapplicationForm.controls) {
        this.appliedapplicationForm.controls[i].markAsDirty()
        this.appliedapplicationForm.controls[i].updateValueAndValidity()
      }
      if(this.appliedapplicationForm.valid) {
        console.log('this.appliedapplicationForm.value', this.appliedapplicationForm.value)
      }
    }
  
    appliedapplicationTableData() {
      this.appliedapplicationTableDatas = []    
    }
  
    appliedapplicationTableDataRemove(item_index) {
      this.appliedapplicationTableDatas.splice(item_index, 1)
      this.appliedapplicationForm = this.fb.group({
        commentary: [null],
        debit: [null],
        credit: [null]
      })     
    }  
  
  
    accountAddInput($event) {
      this.appliedapplicationTableDatas.push(
        $event
      )
  
      this.appliedapplicationForm.patchValue({
        appliedapplicationTableDatas: $event
      })     
  
    
  
  }
  clearFilter() {
    this.appliedapplicationForm.reset()
    this.appliedapplicationForm.patchValue({
      appliedapplication_type: ['1'],
      appliedapplication_tax_exempt: ['no']
    })
  }
  }
  
