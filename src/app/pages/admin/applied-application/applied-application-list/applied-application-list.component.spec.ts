import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppliedApplicationListComponent } from './applied-application-list.component';

describe('AppliedApplicationListComponent', () => {
  let component: AppliedApplicationListComponent;
  let fixture: ComponentFixture<AppliedApplicationListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppliedApplicationListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppliedApplicationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
