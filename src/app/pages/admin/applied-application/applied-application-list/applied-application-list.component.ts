  import { Component, OnInit } from '@angular/core';

  import { TranslateService } from '@ngx-translate/core';
  
  @Component({
    selector: 'app-applied-application-list',
  templateUrl: './applied-application-list.component.html',
  styleUrls: ['./applied-application-list.component.scss']
  })
  export class  AppliedApplicationListComponent implements OnInit {
  
    appliedapplicationaccountDatas: any
  
    activeCollapse = false  
  
    constructor(
      private translate: TranslateService
    ) { 
  
    }
  
    ngOnInit() {
      this.languageTranslate()
  
      this.appliedapplicationaccountData()    
    }
  
    languageTranslate() {
      let acLanguage = localStorage.getItem('acLanguage')
  
      if(acLanguage != null && acLanguage != '') {
        this.translate.use(acLanguage)
      } else {
        this.translate.setDefaultLang('es')
      }    
    }
    
    showFilter() {
      this.activeCollapse = !this.activeCollapse
    } 
    
    receiveSearchData($event) {
      // console.log('receiveSearchData', $event)
    }
    
    submitForm() {
      this.appliedapplicationaccountDatas = []
    }
    
    appliedapplicationaccountData() {
      this.appliedapplicationaccountDatas = [
        {
          business: 'Merco group',
          user: 'John Doe',
          application: 'Banco Financiera Comercial',
          type_service: 'Business associates',
          name: 'John',
          id_card: '458689',
          state: 'Finalized',
          days_passed: 'peritoneal membrane',
          analyst: 'Vanessa ariza'
        }                  
      ]
    }
  }
  
  

