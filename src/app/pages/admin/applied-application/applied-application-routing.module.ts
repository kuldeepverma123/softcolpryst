import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppliedApplicationListComponent } from './applied-application-list/applied-application-list.component';
import { AppliedApplicationEditComponent } from './applied-application-edit/applied-application-edit.component';
import { AppliedApplicationStatusComponent } from './applied-application-status/applied-application-status.component';
import { AppliedApplicationDeleteComponent } from './applied-application-delete/applied-application-delete.component';


const routes: Routes = [
  {
    path: '',
    component: AppliedApplicationListComponent
  },
  {
    path: 'edit',
    component: AppliedApplicationEditComponent
  },
  {
    path: 'status',
    component: AppliedApplicationStatusComponent
  },
  {
    path: 'delete',
    component: AppliedApplicationDeleteComponent
  }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppliedApplicationRoutingModule { }
