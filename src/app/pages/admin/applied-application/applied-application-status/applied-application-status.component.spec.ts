import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppliedApplicationStatusComponent } from './applied-application-status.component';

describe('AppliedApplicationStatusComponent', () => {
  let component: AppliedApplicationStatusComponent;
  let fixture: ComponentFixture<AppliedApplicationStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppliedApplicationStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppliedApplicationStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
