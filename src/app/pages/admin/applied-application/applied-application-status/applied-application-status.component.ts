import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-applied-application-status',
  templateUrl: './applied-application-status.component.html',
  styleUrls: ['./applied-application-status.component.scss']
})
export class AppliedApplicationStatusComponent implements OnInit {
  checked = true;
  appliedapplicationStatus: FormGroup

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder    
  ) { }

  ngOnInit() {
    this.languageTranslate()

    this.formVariablesInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  formVariablesInit() {
    this.appliedapplicationStatus = this.fb.group({
      customer_type: ['1'],
      customer_tax_exempt: ['no'],
      background: [null, [Validators.required]],
      concept: [null, [Validators.required]]
    })      
  }

  saveApplied() {
    for (const i in this.appliedapplicationStatus.controls) {
      this.appliedapplicationStatus.controls[i].markAsDirty()
      this.appliedapplicationStatus.controls[i].updateValueAndValidity()
    }
    if(this.appliedapplicationStatus.valid) {
      console.log('this.appliedapplicationStatus.value', this.appliedapplicationStatus.value)
    }
  }

  clearFilter() {
    this.appliedapplicationStatus.reset()
    this.appliedapplicationStatus.patchValue({
      customer_type: ['1'],
      customer_tax_exempt: ['no']
    })
  }
  log(value: string[]): void {
    console.log(value);
  }


}

