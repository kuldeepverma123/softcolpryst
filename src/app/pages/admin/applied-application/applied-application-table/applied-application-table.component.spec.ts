import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppliedApplicationTableComponent } from './applied-application-table.component';

describe('AppliedApplicationTableComponent', () => {
  let component: AppliedApplicationTableComponent;
  let fixture: ComponentFixture<AppliedApplicationTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppliedApplicationTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppliedApplicationTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
