
import { Component, OnInit, Input } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
    
  @Component({
      selector: 'app-applied-application-table',
      templateUrl: './applied-application-table.component.html',
      styleUrls: ['./applied-application-table.component.scss']
    })
    export class  AppliedApplicationTableComponent implements OnInit {
    
      @Input() appliedapplicationaccountData: any
    
      //  appliedapplication account form
      AppliedApplicationAccountForm: FormGroup  
      AppliedApplicationAccountVisible = false
    
      // search
      search_title = ''
    
      appliedapplicationaccountDisplayData: any
    
      sortName: string | null = null
      sortValue: string | null = null
    
      deleteConfirmVisible = false
    
      constructor(
        private fb: FormBuilder
      ) { 
    
      }
    
      ngOnInit() {
        this.AppliedApplicationAccountFormInit()
    
        this.appliedapplicationAccountData()
      }
    
      //  appliedapplication account form
      AppliedApplicationAccountFormInit() {
        this.AppliedApplicationAccountForm = this.fb.group({
          business: [null, [Validators.required]],
          user: [null, [Validators.required]],
          name: [null, [Validators.required]],
          type_service: [null, [Validators.required]],
          application_correlative_checks: [null, [Validators.required]]
        })      
      }  
    
      AppliedApplicationAccountOpen() {
        this.AppliedApplicationAccountVisible = true
      }
    
      AppliedApplicationAccountClose() {
        this.AppliedApplicationAccountForm.reset()
        this.AppliedApplicationAccountVisible = false
      }      
    
      AppliedApplicationAccountSave() {    
        for (const i in this.AppliedApplicationAccountForm.controls) {
          this.AppliedApplicationAccountForm.controls[i].markAsDirty()
          this.AppliedApplicationAccountForm.controls[i].updateValueAndValidity()
        }          
    
        if(this.AppliedApplicationAccountForm.valid) {
          this.AppliedApplicationAccountClose()
        }
      }
    
      appliedapplicationAccountData() {
        this.appliedapplicationaccountDisplayData = [...this.appliedapplicationaccountData]
      }
    
      keyUpSearch() {
        let searchLower = this.search_title.toLowerCase()
    
        const filterFunc = (item: { state: string; analyst: string; days_passed: string; id_card: string; business: string; user: string; name: string; type_service: string }) => {
          return (
            item.business.toLowerCase().indexOf(searchLower) !== -1 ||
            item.id_card.toLowerCase().indexOf(searchLower) !== -1 ||
            item.analyst.toLowerCase().indexOf(searchLower) !== -1 ||
            item.days_passed.toLowerCase().indexOf(searchLower) !== -1 ||
            item.state.toLowerCase().indexOf(searchLower) !== -1 ||
            item.user.toLowerCase().indexOf(searchLower) !== -1 ||
            item.name.toLowerCase().indexOf(searchLower) !== -1 ||
            item.type_service.toLowerCase().indexOf(searchLower) !== -1
          )
        }
    
        const data = this.appliedapplicationaccountData.filter((item: { state: string; analyst: string; days_passed: string;  id_card: string; business: string; user: string; name: string; type_service: string }) => filterFunc(item))
    
        this.appliedapplicationaccountDisplayData = data.sort((a, b) =>
          this.sortValue === 'ascend'
            ? a[this.sortName!] > b[this.sortName!]
              ? 1
              : -1
            : b[this.sortName!] > a[this.sortName!]
            ? 1
            : -1
        ) 
        
        if(searchLower.length == 0) {
          this.appliedapplicationaccountDisplayData = [...this.appliedapplicationaccountData]
        }    
      }  
    
      // delete confirm
      deleteConfirmOpen() {
        this.deleteConfirmVisible = true
      }
    
      deleteConfirmClose() {
        this.deleteConfirmVisible = false
      }  
    
    }
    
    
    
  
  