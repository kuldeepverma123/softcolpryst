import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgZorroAntdModule } from 'ng-zorro-antd';

// import ngx-translate and the http loader
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import { HttpClient, HttpClientModule } from '@angular/common/http';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppliedApplicationRoutingModule } from './applied-application-routing.module';
import { AppliedApplicationListComponent } from './applied-application-list/applied-application-list.component';
import { AppliedApplicationFormComponent } from './applied-application-form/applied-application-form.component';
import { AppliedApplicationTableComponent } from './applied-application-table/applied-application-table.component';
import { AppliedApplicationEditComponent } from './applied-application-edit/applied-application-edit.component';
import { AppliedApplicationStatusComponent } from './applied-application-status/applied-application-status.component';
import { AppliedApplicationDeleteComponent } from './applied-application-delete/applied-application-delete.component';


@NgModule({
  declarations: [AppliedApplicationListComponent, AppliedApplicationFormComponent, AppliedApplicationTableComponent, AppliedApplicationEditComponent, AppliedApplicationStatusComponent, AppliedApplicationDeleteComponent],
  imports: [
    NgZorroAntdModule,
    HttpClientModule,
    CommonModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    FormsModule,
    ReactiveFormsModule,
    AppliedApplicationRoutingModule
  ]
})
export class AppliedApplicationModule { }
// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
