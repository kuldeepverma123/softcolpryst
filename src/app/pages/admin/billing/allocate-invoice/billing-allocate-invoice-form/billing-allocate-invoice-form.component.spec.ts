import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BillingAllocateInvoiceFormComponent } from './billing-allocate-invoice-form.component';

describe('BillingAllocateInvoiceFormComponent', () => {
  let component: BillingAllocateInvoiceFormComponent;
  let fixture: ComponentFixture<BillingAllocateInvoiceFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BillingAllocateInvoiceFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BillingAllocateInvoiceFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
