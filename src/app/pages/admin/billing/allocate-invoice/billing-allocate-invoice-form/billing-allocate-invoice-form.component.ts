import { Component, OnInit, Input } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-billing-allocate-invoice-form',
  templateUrl: './billing-allocate-invoice-form.component.html',
  styleUrls: ['./billing-allocate-invoice-form.component.scss']
})
export class BillingAllocateInvoiceFormComponent implements OnInit {
  @Input() allocateinvoiceaccountData: any
  billingallocateinvoiceForm: FormGroup

  currentDate:any = new Date()  

  // billingallocateinvoice table data 
  billingallocateinvoiceTableDatas = []  

  // search account dialog
  
  accountDatas = []

  // allocateinvoiceaccount search dialog
  searchAllocateInvoiceaccountVisible = false
  allocateinvoiceaccountDatas = []  

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.chequeAddFormInit()

    this.billingallocateinvoiceTableData()  

  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  chequeAddFormInit() {
    this.billingallocateinvoiceForm = this.fb.group({
     billing_type: ['1'],
     client_document: [null, [Validators.required]],
     application_document: [null, [Validators.required]],
     customer_name: [null, [Validators.required]],
     no_invoice: [null, [Validators.required]],
     assigned_to: [null, [Validators.required]],
     expense_center: [null, [Validators.required]],
      chequeTableDatas: []
    })      
  }
  
  saveCheque() {
    for (const i in this.billingallocateinvoiceForm.controls) {
      this.billingallocateinvoiceForm.controls[i].markAsDirty()
      this.billingallocateinvoiceForm.controls[i].updateValueAndValidity()
    }
    if(this.billingallocateinvoiceForm.valid) {
      console.log('this.billingallocateinvoiceForm.value', this.billingallocateinvoiceForm.value)
    }
  }

  billingallocateinvoiceTableData() {
    this.billingallocateinvoiceTableDatas = []    
  }

  billingallocateinvoiceTableDataRemove(item_index) {
    this.billingallocateinvoiceTableDatas.splice(item_index, 1)
    this.billingallocateinvoiceForm = this.fb.group({
      commentary: [null],
      debit: [null],
      credit: [null]
    })     
  }  


  accountAddInput($event) {
    this.billingallocateinvoiceTableDatas.push(
      $event
    )

    this.billingallocateinvoiceForm.patchValue({
      chequeTableDatas: $event
    })     

  

}
clearFilter() {
  this.billingallocateinvoiceForm.reset()
  this.billingallocateinvoiceForm.patchValue({
    billing_type: ['1']
  })
}
}

