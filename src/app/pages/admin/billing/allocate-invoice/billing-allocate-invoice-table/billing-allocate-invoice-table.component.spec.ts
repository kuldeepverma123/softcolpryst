import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BillingAllocateInvoiceTableComponent } from './billing-allocate-invoice-table.component';

describe('BillingAllocateInvoiceTableComponent', () => {
  let component: BillingAllocateInvoiceTableComponent;
  let fixture: ComponentFixture<BillingAllocateInvoiceTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BillingAllocateInvoiceTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BillingAllocateInvoiceTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
