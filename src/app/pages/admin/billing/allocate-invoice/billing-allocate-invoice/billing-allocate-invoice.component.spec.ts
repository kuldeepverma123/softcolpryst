import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BillingAllocateInvoiceComponent } from './billing-allocate-invoice.component';

describe('BillingAllocateInvoiceComponent', () => {
  let component: BillingAllocateInvoiceComponent;
  let fixture: ComponentFixture<BillingAllocateInvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BillingAllocateInvoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BillingAllocateInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
