import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-billing-allocate-invoice',
  templateUrl: './billing-allocate-invoice.component.html',
  styleUrls: ['./billing-allocate-invoice.component.scss']
})
export class  BillingAllocateInvoiceComponent implements OnInit {

  allocateinvoiceaccountDatas: any

  activeCollapse = false  

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.allocateinvoiceaccountData()    
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  showFilter() {
    this.activeCollapse = !this.activeCollapse
  } 
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
  }
  
  submitForm() {
    this.allocateinvoiceaccountDatas = []
  }
  
  allocateinvoiceaccountData() {
    this.allocateinvoiceaccountDatas = [
      {
        company: 'Merco group',
        no_invoice: '235586888',
        expenditure_center: 'Florida',
        unit_cost: '$125.00',
        transport_high_risk: 'peritoneal membrane transport',
        inter_municipal_transport: 'peritoneal membrane transport',
        state: 'Finalized'
      }                  
    ]
  }
}


