import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BillingAllocateInvoiceComponent } from './allocate-invoice/billing-allocate-invoice/billing-allocate-invoice.component';
import { BillingCheckInvoiceComponent } from './check-invoice/billing-check-invoice/billing-check-invoice.component';


const routes: Routes = [
  {
    path: 'allocate-invoice',
    component: BillingAllocateInvoiceComponent
  },
  {
    path: 'check-invoice',
    component: BillingCheckInvoiceComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BillingRoutingModule { }

