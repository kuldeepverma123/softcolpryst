import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgZorroAntdModule } from 'ng-zorro-antd';
// import ngx-translate and the http loader
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import { HttpClient, HttpClientModule } from '@angular/common/http';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { BillingRoutingModule } from './billing-routing.module';
import { BillingAllocateInvoiceComponent } from './allocate-invoice/billing-allocate-invoice/billing-allocate-invoice.component';
import { BillingCheckInvoiceComponent } from './check-invoice/billing-check-invoice/billing-check-invoice.component';
import { BillingAllocateInvoiceFormComponent } from './allocate-invoice/billing-allocate-invoice-form/billing-allocate-invoice-form.component';
import { BillingAllocateInvoiceTableComponent } from './allocate-invoice/billing-allocate-invoice-table/billing-allocate-invoice-table.component';
import { BillingCheckInvoiceFormComponent } from './check-invoice/billing-check-invoice-form/billing-check-invoice-form.component';
import { BillingCheckInvoiceTableComponent } from './check-invoice/billing-check-invoice-table/billing-check-invoice-table.component';


@NgModule({
  declarations: [BillingAllocateInvoiceComponent, BillingCheckInvoiceComponent, BillingAllocateInvoiceFormComponent, BillingAllocateInvoiceTableComponent, BillingCheckInvoiceFormComponent, BillingCheckInvoiceTableComponent],
  imports: [
    NgZorroAntdModule,
    CommonModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    FormsModule,
    ReactiveFormsModule,
    BillingRoutingModule
  ]
})
export class BillingModule { }
// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
