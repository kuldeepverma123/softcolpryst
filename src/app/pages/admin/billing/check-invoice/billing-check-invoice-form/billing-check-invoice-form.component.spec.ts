import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BillingCheckInvoiceFormComponent } from './billing-check-invoice-form.component';

describe('BillingCheckInvoiceFormComponent', () => {
  let component: BillingCheckInvoiceFormComponent;
  let fixture: ComponentFixture<BillingCheckInvoiceFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BillingCheckInvoiceFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BillingCheckInvoiceFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
