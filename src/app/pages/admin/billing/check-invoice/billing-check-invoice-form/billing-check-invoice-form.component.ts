import { Component, OnInit, Input } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-billing-check-invoice-form',
  templateUrl: './billing-check-invoice-form.component.html',
  styleUrls: ['./billing-check-invoice-form.component.scss']
})
export class BillingCheckInvoiceFormComponent implements OnInit {
  @Input() allocateinvoiceaccountData: any
  billingcheckinvoiceForm: FormGroup

  currentDate:any = new Date()  

  // billingcheckinvoice table data 
  billingcheckinvoiceTableDatas = []  

  // search account dialog
  
  accountDatas = []

  // allocateinvoiceaccount search dialog
  searchAllocateInvoiceaccountVisible = false
  allocateinvoiceaccountDatas = []  

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.billingcheckinvoiceFormInit()

    this.billingcheckinvoiceTableData()  

  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  billingcheckinvoiceFormInit() {
    this.billingcheckinvoiceForm = this.fb.group({
     billing_type: ['1'],
     company_document: [null, [Validators.required]],
     business_name: [null, [Validators.required]],
     start_date: [null],
     end_date: [null],
     billingcheckinvoiceTableDatas: []
    })      
  }
  
  saveCheque() {
    for (const i in this.billingcheckinvoiceForm.controls) {
      this.billingcheckinvoiceForm.controls[i].markAsDirty()
      this.billingcheckinvoiceForm.controls[i].updateValueAndValidity()
    }
    if(this.billingcheckinvoiceForm.valid) {
      console.log('this.billingcheckinvoiceForm.value', this.billingcheckinvoiceForm.value)
    }
  }

  billingcheckinvoiceTableData() {
    this.billingcheckinvoiceTableDatas = []    
  }

  chequeTableDataRemove(item_index) {
    this.billingcheckinvoiceTableDatas.splice(item_index, 1)
    this.billingcheckinvoiceForm = this.fb.group({
      commentary: [null],
      debit: [null],
      credit: [null]
    })     
  }  


  accountAddInput($event) {
    this.billingcheckinvoiceTableDatas.push(
      $event
    )

    this.billingcheckinvoiceForm.patchValue({
      billingcheckinvoiceTableDatas: $event
    })     

  

}
clearFilter() {
  this.billingcheckinvoiceForm.reset()
  this.billingcheckinvoiceForm.patchValue({
    billing_type: ['1']
  })
}
}

