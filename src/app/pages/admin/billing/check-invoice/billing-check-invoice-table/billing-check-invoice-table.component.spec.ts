import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BillingCheckInvoiceTableComponent } from './billing-check-invoice-table.component';

describe('BillingCheckInvoiceTableComponent', () => {
  let component: BillingCheckInvoiceTableComponent;
  let fixture: ComponentFixture<BillingCheckInvoiceTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BillingCheckInvoiceTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BillingCheckInvoiceTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
