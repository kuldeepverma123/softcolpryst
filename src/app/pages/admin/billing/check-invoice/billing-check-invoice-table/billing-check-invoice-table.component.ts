import { Component, OnInit, Input } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-billing-check-invoice-table',
  templateUrl: './billing-check-invoice-table.component.html',
  styleUrls: ['./billing-check-invoice-table.component.scss']
})
export class BillingCheckInvoiceTableComponent implements OnInit {

  @Input() allocateinvoiceaccountData: any

  // allocateinvoice account form
  AllocateInvoiceAccountForm: FormGroup  
  AllocateInvoiceAccountVisible = false

  // search
  search_title = ''

  allocateinvoiceaccountDisplayData: any

  sortName: string | null = null
  sortValue: string | null = null

  deleteConfirmVisible = false

  constructor(
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.AllocateInvoiceAccountFormInit()

    this.allocateinvoiceAccountData()
  }

  //  allocateinvoice account form
  AllocateInvoiceAccountFormInit() {
    this.AllocateInvoiceAccountForm = this.fb.group({
      company: [null, [Validators.required]],
      no_invoice: [null, [Validators.required]],
      requested: [null, [Validators.required]],
      invoice_cost: [null, [Validators.required]],
      invoice_date: [null, [Validators.required]],
      expiration: [null, [Validators.required]],
      state: [null, [Validators.required]]
    })      
  }  

  AllocateInvoiceAccountOpen() {
    this.AllocateInvoiceAccountVisible = true
  }

  AllocateInvoiceAccountClose() {
    this.AllocateInvoiceAccountForm.reset()
    this.AllocateInvoiceAccountVisible = false
  }      

  AllocateInvoiceAccountSave() {    
    for (const i in this.AllocateInvoiceAccountForm.controls) {
      this.AllocateInvoiceAccountForm.controls[i].markAsDirty()
      this.AllocateInvoiceAccountForm.controls[i].updateValueAndValidity()
    }          

    if(this.AllocateInvoiceAccountForm.valid) {
      this.AllocateInvoiceAccountClose()
    }
  }

  allocateinvoiceAccountData() {
    this.allocateinvoiceaccountDisplayData = [...this.allocateinvoiceaccountData]
  }

  keyUpSearch() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { company: string; no_invoice: string; requested: string; invoice_cost: string; invoice_date: string; expiration: string; state: string }) => {
      return (
        item.company.toLowerCase().indexOf(searchLower) !== -1 ||
        item.no_invoice.toLowerCase().indexOf(searchLower) !== -1 ||
        item.requested.toLowerCase().indexOf(searchLower) !== -1 ||
        item.invoice_cost.toLowerCase().indexOf(searchLower) !== -1 ||
        item.invoice_date.toLowerCase().indexOf(searchLower) !== -1 ||
        item.expiration.toLowerCase().indexOf(searchLower) !== -1 ||
        item.state.toLowerCase().indexOf(searchLower) !== -1 
      )
    }

    const data = this.allocateinvoiceaccountData.filter((item: { company: string; no_invoice: string; requested: string;  invoice_cost: string; invoice_date: string; expiration: string; state: string }) => filterFunc(item))

    this.allocateinvoiceaccountDisplayData = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    ) 
    
    if(searchLower.length == 0) {
      this.allocateinvoiceaccountDisplayData = [...this.allocateinvoiceaccountData]
    }    
  }  

  // delete confirm
  deleteConfirmOpen() {
    this.deleteConfirmVisible = true
  }

  deleteConfirmClose() {
    this.deleteConfirmVisible = false
  }  

}



