import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BillingCheckInvoiceComponent } from './billing-check-invoice.component';

describe('BillingCheckInvoiceComponent', () => {
  let component: BillingCheckInvoiceComponent;
  let fixture: ComponentFixture<BillingCheckInvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BillingCheckInvoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BillingCheckInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
