import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-billing-check-invoice',
  templateUrl: './billing-check-invoice.component.html',
  styleUrls: ['./billing-check-invoice.component.scss']
})
export class  BillingCheckInvoiceComponent implements OnInit {

  allocateinvoiceaccountDatas: any

  activeCollapse = false  

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.allocateinvoiceaccountData()    
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  showFilter() {
    this.activeCollapse = !this.activeCollapse
  } 
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
  }
  
  submitForm() {
    this.allocateinvoiceaccountDatas = []
  }
  
  allocateinvoiceaccountData() {
    this.allocateinvoiceaccountDatas = [
      {
        no_invoice: '235586888',
        company: 'Merco group',
        requested: 'John Doe',
        invoice_cost: '$125.00',
        invoice_date: '12-09-2019',
        expiration: '18-10-2019',
        state: 'Finalized'
      }                  
    ]
  }
}



