import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateApplicationAddFormComponent } from './create-application-add-form.component';

describe('CreateApplicationAddFormComponent', () => {
  let component: CreateApplicationAddFormComponent;
  let fixture: ComponentFixture<CreateApplicationAddFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateApplicationAddFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateApplicationAddFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
