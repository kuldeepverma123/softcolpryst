import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-application-add-form',
  templateUrl: './create-application-add-form.component.html',
  styleUrls: ['./create-application-add-form.component.scss']
})
export class CreateApplicationAddFormComponent implements OnInit {

  createapplicationaddForm: FormGroup

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder    
  ) { }

  ngOnInit() {
    this.languageTranslate()

    this.formVariablesInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  formVariablesInit() {
    this.createapplicationaddForm = this.fb.group({
      name: [null, [Validators.required]],
      user_name: [null, [Validators.required]],
      email: [null, [Validators.required]],
      type_of_services: [null, [Validators.required]]
    })      
  }

  saveCreateapplication() {
    for (const i in this.createapplicationaddForm.controls) {
      this.createapplicationaddForm.controls[i].markAsDirty()
      this.createapplicationaddForm.controls[i].updateValueAndValidity()
    }
    if(this.createapplicationaddForm.valid) {
      console.log('this.createapplicationaddForm.value', this.createapplicationaddForm.value)
    }
  }

  clearFilter() {
    this.createapplicationaddForm.reset()
    this.createapplicationaddForm.patchValue({
      //customer_type: ['1'],
      //customer_tax_exempt: ['no']
    })
  }



}



