import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateApplicationListComponent } from './create-application-list.component';

describe('CreateApplicationListComponent', () => {
  let component: CreateApplicationListComponent;
  let fixture: ComponentFixture<CreateApplicationListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateApplicationListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateApplicationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
