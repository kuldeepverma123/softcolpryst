   import { Component, OnInit } from '@angular/core';

    import { TranslateService } from '@ngx-translate/core';
    import { FileUploader, FileItem, ParsedResponseHeaders  } from 'ng2-file-upload';
    import { FormBuilder, FormGroup, Validators } from '@angular/forms';
    import { HttpClient } from '@angular/common/http';
    import { NzNotificationService } from 'ng-zorro-antd/notification';
    import { ApiService } from '../../../../api.service';
    import * as XLSX from 'xlsx';
    @Component({
      selector: 'app-create-application-list',
      templateUrl: './create-application-list.component.html',
      styleUrls: ['./create-application-list.component.scss']
    })
    export class  CreateApplicationListComponent implements OnInit {

    public  uploader:FileUploader = new FileUploader({url:'http://18.216.180.72:3000/api/upload/photo', itemAlias: 'attach_photo' });
    public docUploader:FileUploader = new FileUploader({url:'http://18.216.180.72:3000/api/upload/doc', itemAlias: 'attach_document'});
      state = {
        value: 1, 
      };

      willDownload = false;
      fileName : ""
      bulkdata : any
      createapplicationAddform: FormGroup
      customersconsultaccountDisplayData:any
      currentService : any
      serviceTypeCheck:string
      radioValue: any = ''
      areaSelectValue: any
      typeOfServiceSelectValue: any
      isSpinning : boolean
      products : any
      services : any
      customerList : any
      employeeList : any
      attachphoto  :any
      attachdoc : any

      constructor(
        private translate: TranslateService,
        private fb: FormBuilder,
        private http: HttpClient,
        private notification: NzNotificationService,
        private service: ApiService    
      ) { }
    
      ngOnInit() {
        this.languageTranslate()
        this.formVariablesInit()
        this.getCustomersList()
        this.getProduct()
        
        this.uploader.onAfterAddingFile = (file) => {
          file.withCredentials = false;
        };

        this.uploader.onCompleteItem = (item: any, status: any) => {
          console.log('Uploaded File Details:', item);
        };

        this.docUploader.onAfterAddingFile = (file) => {
          file.withCredentials = false;
        };

        this.docUploader.onCompleteItem = (item: any, status: any) => {
          console.log('Uploaded File Details:', item);
        };

        this.uploader.onSuccessItem = (item, response, status, headers) => this.onSuccessItem(item, response, status, headers);
        this.docUploader.onSuccessItem = (item, response, status, headers) => this.onSuccessItem(item, response, status, headers);
      }

      onSuccessItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
        let data = JSON.parse(response); //success server response
        console.log(data);
         if(data){
            let finame =  data.data.fieldname;
             if(finame == "attach_photo"){
                  this.attachphoto  = data.data.filename;
             } if( finame == "attach_document"){
                 this.attachdoc  = data.data.filename;
             }
          }
      }
    
      languageTranslate() {
        let acLanguage = localStorage.getItem('acLanguage')
    
        if(acLanguage != null && acLanguage != '') {
          this.translate.use(acLanguage)
        } else {
          this.translate.setDefaultLang('es')
        }    
      }    
    
      formVariablesInit() {
        this.createapplicationAddform = this.fb.group({
          user: [null],
          area: [null, [Validators.required]],
          cell_phone: [null],
          document_number: [null],
          company: [null],
          business: [null],
          candidate_doc: [null],
          test_type: [null],
          candidate_name: [null],
          neighborhood: [null],
          expense_center: [null],
          address: [null],
          details: [null],
          email: [null],
          phone: [null],
          cellphone: [null],
          document: [null],
          last_name: [null],
          attach_file: [null],
          department: ['1'],
          city: [null],
          radio: [null],
          type_of_service: [null, [Validators.required]],
          id_doc: [null],
          name: [null],
          observations: [null],
          position: [null],
          description: [null],
          attach_photo: [this.attachphoto],
          second: [null, [Validators.required]],
          attach_document : [this.attachdoc],
          cargo : [null],
          servicetype : [null],
          testtype : [null],
          direction : [null],
          socialresone :[null],
          document_type :[null],
          documentnumber :[null],
          candidate_last_name :[null]
        })      
      }
      listOfData = [
        {
          key: '1',
          item: '1',
          description: 'MERCO GROUP',
          attached: 'No'
        },
        {
          key: '2',
          item: '2',
          description: 'MERCO GROUP',
          attached: 'No'
        },
        {
          key: '3',
          item: '3',
          description: 'MERCO GROUP',
          attached: 'No'
        }
      ];
    
    
    
      saveCreateapplication() {
        
        for (const i in this.createapplicationAddform.controls) {
          this.createapplicationAddform.controls[i].markAsDirty()
          this.createapplicationAddform.controls[i].updateValueAndValidity()
          
        }
        if(this.createapplicationAddform.valid) {
          console.log('this.createapplicationAddform.value', this.createapplicationAddform.value)
          this.serviceTypeCheck = this.createapplicationAddform.value.createapplication_type
         
          
        }
        // this.createapplicationAddform.patchValue({
        //   createapplication_type: []
        //   //createapplication_tax_exempt: ['no']
        // })
        
            this.currentService = this.createapplicationAddform.value.type_of_service
    
            console.log("this.currentService", this.currentService)
      }
    
      clearFilter() {
        this.createapplicationAddform.reset()
        this.createapplicationAddform.patchValue({
          //createapplication_type: ['0'],
          //createapplication_tax_exempt: ['no']
        })
      }
    
      areaSelect($event) {
        this.areaSelectValue = $event
        this.getService($event)
        this.createapplicationAddform.get('type_of_service').reset()
        //console.log("-->areaSelectValue",this.areaSelectValue);
      }

      bussineSelect($event) {
        //this.areaSelectValue = $event
        this.listEmployeeByClientId($event)
        this.createapplicationAddform.get('user').reset()
        //console.log("-->areaSelectValue",this.areaSelectValue);
      }

      typeOfSelectService($event) {
        console.log("$event",$event)
        this.typeOfServiceSelectValue = $event
        //this.createapplicationAddform.get('type_of_service').reset()
        //console.log("-->areaSelectValue",this.areaSelectValue);
      }      
    
      typeOfService() { 
        for (const i in this.createapplicationAddform.controls) {
          this.createapplicationAddform.controls[i].markAsDirty()
          this.createapplicationAddform.controls[i].updateValueAndValidity()
          
        }
        if(this.createapplicationAddform.valid) {
          console.log('this.createapplicationAddform.value', this.createapplicationAddform.value)
          this.serviceTypeCheck = this.createapplicationAddform.value.createapplication_type
         
          
        }     
        this.typeOfServiceSelectValue = this.createapplicationAddform.value.type_of_service
      }  
     
      submitForm() {
      //   if (this.createapplicationAddform.invalid) {
      //     return;
      // }
      this.uploader.uploadAll();
      this.docUploader.uploadAll();

        this.isSpinning = true;
       console.log("imagedata",this.uploader);
       console.log("imagedata",this.docUploader);

       setTimeout (() => {

        
        this.createapplicationAddform.value.attach_photo = this.attachphoto;

        this.createapplicationAddform.value.attachdoc = this.attachdoc;
        
        console.log("--->>",this.attachphoto);
        console.log("--->>",this.attachdoc);
        console.log("--->>",this.createapplicationAddform.value);
        this.service.createTask(this.createapplicationAddform.value).subscribe(
          (response) =>{
            console.log(response)
            this.successNotification('success')
            this.createapplicationAddform.reset();
            this.isSpinning = false;
          }, 
          (error) => {
            this.errorNotification('error');
            this.isSpinning = false;
          }
        )
      }, 2000);

      }
      successNotification(type: string): void {
        let acLanguage = localStorage.getItem('acLanguage')
       let tit, message;
      console.log(acLanguage)
      if(acLanguage == "es"){

      tit = 'Solicitud de creación exitosa'

      }else {
        tit = 'Request create successful'
      }

    this.notification.create(
      type,
      tit,
      message
       );
      }

      errorNotification(type: string): void {
        let acLanguage = localStorage.getItem('acLanguage')
        let tit, message;
          console.log(acLanguage)
          if(acLanguage == "es"){
    
          tit = 'Solicitud crear falla'
          message = 'algo está mal'
    
          }else {
            tit = 'Request create fail'
            message = 'something is wrong'
          }
    
        this.notification.create(
          type,
          tit,
          message
        );
      }


      getProduct() {
        this.service.getAllProduct().subscribe((response) => {
          console.log("response", response['data'])
          this.products = response['data']
          console.log("after", this.products)
        })
       }

       getService(data) {
        this.service.getServiceByProduct(data).subscribe((response) => {
         // console.log("response", response['data'])
          this.services = response['data']
          console.log("after  services", this.services)
        })
       }
        
       getCustomersList() {
        this.service.getCustomers().subscribe((response) => {
         // console.log("response", response['data'])
          this.customerList = response['data']
          console.log("after  customer list", this.customerList)
        })
       }
         
       listEmployeeByClientId(data) {
        this.service.getEmployeeByClientId(data).subscribe((response) => {
         // console.log("response", response['data'])
          this.employeeList = response['data']
          console.log("after  customer list", this.employeeList)
        })
       }

       onFileChange(ev) {
        let workBook = null;
        let jsonData = null;
        const reader = new FileReader();
        const file = ev.target.files[0];
        this.fileName = file.name;
        reader.onload = (event) => {
          const data = reader.result;
          workBook = XLSX.read(data, { type: 'binary' });
          jsonData = workBook.SheetNames.reduce((initial, name) => {
            const sheet = workBook.Sheets[name];
            initial[name] = XLSX.utils.sheet_to_json(sheet);
            return initial;
          }, {});
          const dataString = JSON.stringify(jsonData);
          this.bulkdata =JSON.stringify(jsonData);
          document.getElementById('output').innerHTML = dataString.slice(0, 300).concat("...");
          this.setDownload(dataString);
        }
        reader.readAsBinaryString(file);
      }
         

      setDownload(data) {
        this.willDownload = true;
        setTimeout(() => {
          const el = document.querySelector("#download");
          el.setAttribute("href", `data:text/json;charset=utf-8,${encodeURIComponent(data)}`);
          el.setAttribute("download", 'xlsxtojson.json');
        }, 1000)
      }

      groupOfService() {        
        // this.typeOfServiceSelectValue = this.createapplicationAddform.value.type_of_service
    
        this.service.bulkuploadrequest({d: this.bulkdata}).subscribe(
          (response) =>{
            console.log(response)
            this.successNotification('success')
            this.createapplicationAddform.reset();
            this.isSpinning = false;
          }, 
          (error) => {
            this.errorNotification('error');
            this.isSpinning = false;
          }
        )
      } 

    
    }
    
    
    






  
  
  
  
  
  