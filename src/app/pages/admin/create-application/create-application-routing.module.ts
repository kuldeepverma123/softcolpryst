import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateApplicationListComponent } from './create-application-list/create-application-list.component';
import { CreateApplicationAddFormComponent } from './create-application-add-form/create-application-add-form.component';


const routes: Routes = [
  {
    path: '',
    component: CreateApplicationListComponent
  },
  {
    path: 'add',
    component: CreateApplicationAddFormComponent
  }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateApplicationRoutingModule { }
