import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgZorroAntdModule } from 'ng-zorro-antd';

// import ngx-translate and the http loader
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import { HttpClient, HttpClientModule } from '@angular/common/http';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FileUploadModule } from "ng2-file-upload";   //Should import HERE
import { CreateApplicationRoutingModule } from './create-application-routing.module';
import { CreateApplicationListComponent } from './create-application-list/create-application-list.component';
import { CreateApplicationAddFormComponent } from './create-application-add-form/create-application-add-form.component';


@NgModule({
  declarations: [CreateApplicationListComponent, CreateApplicationAddFormComponent],
  imports: [
    NgZorroAntdModule,
    HttpClientModule,
    CommonModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    FormsModule,
    ReactiveFormsModule,
    CreateApplicationRoutingModule,
    FileUploadModule
  ]
})
export class CreateApplicationModule { }
// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
