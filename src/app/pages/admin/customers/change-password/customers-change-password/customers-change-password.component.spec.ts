import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomersChangePasswordComponent } from './customers-change-password.component';

describe('CustomersChangePasswordComponent', () => {
  let component: CustomersChangePasswordComponent;
  let fixture: ComponentFixture<CustomersChangePasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomersChangePasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomersChangePasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
