import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';


@Component({
  selector: 'app-customers-change-password',
  templateUrl: './customers-change-password.component.html',
  styleUrls: ['./customers-change-password.component.scss']
})
export class CustomersChangePasswordComponent implements OnInit {

  validateForm: FormGroup
  constructor(
    private fb: FormBuilder,
    private translate: TranslateService
  ) { }

  ngOnInit() {
    this.languageTranslate()
    this.validateForm = this.fb.group({
      password: [null, [Validators.required]],
      email: [null, [Validators.required]],
      newpassword: [null, [Validators.required]],
      confirm: [null, [this.confirmValidator]]
    })    
  }
  validateConfirmPassword(): void {
    setTimeout(() => this.validateForm.controls.confirm.updateValueAndValidity());
  } 
  confirmValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return { error: true, required: true }
    } else if (control.value !== this.validateForm.controls.newpassword.value) {
      return { confirm: true, error: true }
    }
    return {}
  } 
  submitForm() {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty()
      this.validateForm.controls[i].updateValueAndValidity()
    }
  } 
  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  clearFilter() {
    this.validateForm.reset()
    // this.applicationEditform.patchValue({
    //   customer_type: ['1'],
    //   customer_tax_exempt: ['no']
    // })
  }

}






