import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomersConsultDeleteComponent } from './customers-consult-delete.component';

describe('CustomersConsultDeleteComponent', () => {
  let component: CustomersConsultDeleteComponent;
  let fixture: ComponentFixture<CustomersConsultDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomersConsultDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomersConsultDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
