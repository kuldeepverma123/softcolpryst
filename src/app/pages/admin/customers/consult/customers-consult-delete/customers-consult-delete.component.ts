import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-customers-consult-delete',
  templateUrl: './customers-consult-delete.component.html',
  styleUrls: ['./customers-consult-delete.component.scss']
})
export class CustomersConsultDeleteComponent implements OnInit {

  customerconsultDelete: FormGroup

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder    
  ) { }

  ngOnInit() {
    this.languageTranslate()

    this.formVariablesInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  formVariablesInit() {
    this.customerconsultDelete = this.fb.group({
      // department: ['1'],
      // no_card: [null, [Validators.required]],
      // name: [null, [Validators.required]],
      // last_name: [null, [Validators.required]],
      // phone: [null, [Validators.required]],
      // mobile: [null, [Validators.required]],
      // address: [null, [Validators.required]],
      // position: [null, [Validators.required]],
      // area: [null, [Validators.required]],
      // profession: [null, [Validators.required]],
      // mail: [null, [Validators.required]],
      // city: [null, [Validators.required]],
      // administrator: [null],
      // observer: [null]
    })      
  }

  saveCustomer() {
    for (const i in this.customerconsultDelete.controls) {
      this.customerconsultDelete.controls[i].markAsDirty()
      this.customerconsultDelete.controls[i].updateValueAndValidity()
    }
    if(this.customerconsultDelete.valid) {
      //console.log('this.customerconsultDelete.value', this.customerconsultDelete.value)
    }
  }

  clearFilter() {
    this.customerconsultDelete.reset()
    this.customerconsultDelete.patchValue({
      //department: ['1'],
      //customer_tax_exempt: ['no']
    })
  }



}




