import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomersConsultEditComponent } from './customers-consult-edit.component';

describe('CustomersConsultEditComponent', () => {
  let component: CustomersConsultEditComponent;
  let fixture: ComponentFixture<CustomersConsultEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomersConsultEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomersConsultEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
