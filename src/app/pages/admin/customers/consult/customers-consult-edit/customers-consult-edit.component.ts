import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { ApiService } from '../../../../../api.service';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, FormControl, FormArray,  Validators } from '@angular/forms';
import { NzNotificationService } from 'ng-zorro-antd/notification';
@Component({
  selector: 'app-customers-consult-edit',
  templateUrl: './customers-consult-edit.component.html',
  styleUrls: ['./customers-consult-edit.component.scss']
})
export class CustomersConsultEditComponent implements OnInit {
  activate_account_link:any
  customercreate: FormGroup
  current = 0
  index = "First-content"
  items: FormArray
  disabled : true
  id :any

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private notification: NzNotificationService,
    private service: ApiService, 
    private route: ActivatedRoute,    
  ) { }

  pre(): void {
    this.current -= 1; 
    this.changeContent();
  }

  next(): void {

    this.saveCustomer();
    this.current += 1;
    this.changeContent();
  }

  done(): void {
    console.log('done');
  }


  changeContent(): void {
    switch (this.current) {
      case 0: {
        this.index = "First-content";
        break;
      }
      case 1: {
        this.index = "Second-content";
        break;
      }
      default: {
        this.index = "First-content";
      }
    }
  }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.languageTranslate()
    this.getcustomerData()
    this.formVariablesInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  formVariablesInit() {
    this.customercreate = this.fb.group({
      department: ['1'],
      customer_tax_exempt: ['no'],
      card_nit: [null, [Validators.required]],
      business_name: [null, [Validators.required]],
      legal_rep: [null, [Validators.required]],
      landline: [null, [Validators.required]],
      direct_employees: [null, [Validators.required]],
      indirect_direct_employess: [null, [Validators.required]],
      area: [null, [Validators.required]],
      address: [null, [Validators.required]],
      mail: [null, [Validators.required]],
      city: [null],
      company_name: [null, [Validators.required]],
      phone: [null, [Validators.required]],
      cost_center: [null, [Validators.required]],
      work_center: [null, [Validators.required]],
      billing_date: [null],
      no_of_employee: [null, [Validators.required]],
      attach_photo: [null],
      items: this.fb.array([ this.createItem() ]),
      activate_account_link : [null]
    })      
  }

  saveCustomer() {
    for (const i in this.customercreate.controls) {
      this.customercreate.controls[i].markAsDirty()
      this.customercreate.controls[i].updateValueAndValidity()
    }
    if(this.customercreate.valid) {
      console.log('this.customercreate.value', this.customercreate.value)
    }
  }

  clearFilter() {
    this.customercreate.reset()
    this.customercreate.patchValue({
      department: ['1'],
      //customer_tax_exempt: ['no']
    })
  }
  
  addAddress() {

    let its =  this.customercreate.value.items.length;

    console.log("langth",its);
    
    console.log("after", this.customercreate.value.items[its-1]); 
    if(its > 1){

    if(this.customercreate.value.items[its - 1].price == '' || this.customercreate.value.items[its -1 ].service == ''){
      console.log("error");
      this.createNotification('error');
      return
    }
  }else{
    if(this.customercreate.value.items[0].price == '' || this.customercreate.value.items[0].service == ''){
      console.log("error");
      this.createNotification('error')
      return
    }
  }
    
    this.items = this.customercreate.get('items') as FormArray;
    this.items.push(this.createItem());  
 
  }
  
  removeAddress(i: number) {
     console.log(i);
     this.items = this.customercreate.get('items') as FormArray;
     let its =  this.customercreate.value.items.length;
       if(its > 1) {
        this.items.removeAt(i);
      }
  }

  createItem(): FormGroup {
    return this.fb.group({
      service: '',
      price: ''
    });
  }

  getcustomerData(){
    this.service.getCustomerById(this.id).subscribe((response) => {
      console.log("response", response['data'])
       this.customercreate.controls['department'].setValue(response['data'][0].department);
       this.customercreate.controls['customer_tax_exempt'].setValue(response['data'][0].department);
       this.customercreate.controls['card_nit'].setValue(response['data'][0].department);
       this.customercreate.controls['business_name'].setValue(response['data'][0].department);
       this.customercreate.controls['legal_rep'].setValue(response['data'][0].department);
       this.customercreate.controls['landline'].setValue(response['data'][0].department);
       this.customercreate.controls['direct_employees'].setValue(response['data'][0].department);
       this.customercreate.controls['indirect_direct_employess'].setValue(response['data'][0].department);
       this.customercreate.controls['area'].setValue(response['data'][0].department);
       this.customercreate.controls['address'].setValue(response['data'][0].department); 
       this.customercreate.controls['mail'].setValue(response['data'][0].department);
       this.customercreate.controls['city'].setValue(response['data'][0].department);
       this.customercreate.controls['company_name'].setValue(response['data'][0].department);
       this.customercreate.controls['phone'].setValue(response['data'][0].department);
       this.customercreate.controls['cost_center'].setValue(response['data'][0].department);
       this.customercreate.controls['work_center'].setValue(response['data'][0].department);
       this.customercreate.controls['billing_date'].setValue(response['data'][0].department);
       this.customercreate.controls['activate_account_link'].setValue(response['data'][0].department);
       
       })
  }

  createNotification(type: string): void {


    let acLanguage = localStorage.getItem('acLanguage')
    let tit, message;
      console.log(acLanguage)
      if(acLanguage == "es"){

      tit = 'Error de formulario'
      message = 'Complete todos los campos y luego cree un nuevo servicio'

      }else {

        tit = 'Form Error'
        message = 'Please fill all field then create new service'
      }

    this.notification.create(
      type,
      tit,
      message
    );
  }

  

}





