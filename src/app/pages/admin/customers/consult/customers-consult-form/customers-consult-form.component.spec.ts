import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomersConsultFormComponent } from './customers-consult-form.component';

describe('CustomersConsultFormComponent', () => {
  let component: CustomersConsultFormComponent;
  let fixture: ComponentFixture<CustomersConsultFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomersConsultFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomersConsultFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
