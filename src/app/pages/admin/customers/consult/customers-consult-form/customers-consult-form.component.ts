import { Component, OnInit, Input } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-customers-consult-form',
  templateUrl: './customers-consult-form.component.html',
   styleUrls: ['./customers-consult-form.component.scss']
})
export class CustomersConsultFormComponent  implements OnInit {
  @Input() customersconsultaccountData: any
  customerconsultForm: FormGroup

  currentDate:any = new Date()  

  // customersconsult table data 
  customersconsultTableDatas = []  

  // search account dialog
  
  accountDatas = []

  // customersconsultaccount search dialog
  searchCustomersConsultaccountVisible = false
  customersconsultaccountDatas = []  

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.customersconsultAddFormInit()

    this.customersconsultTableData()  

  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  customersconsultAddFormInit() {
    this.customerconsultForm = this.fb.group({
     application_type: ['1'],
     application_tax_exempt: ['no'],
     name: [null, [Validators.required]],
     state: [null, [Validators.required]],
     assigned_to: [null, [Validators.required]],
     type_of_service: [null, [Validators.required]],
     id_card: [null, [Validators.required]],
     business_name: [null, [Validators.required]],
     nit_cedula: [null, [Validators.required]],
     customersconsultTableDatas: []
    })      
  }
  
  saveCheque() {
    for (const i in this.customerconsultForm.controls) {
      this.customerconsultForm.controls[i].markAsDirty()
      this.customerconsultForm.controls[i].updateValueAndValidity()
    }
    if(this.customerconsultForm.valid) {
      console.log('this.customerconsultForm.value', this.customerconsultForm.value)
    }
  }

  customersconsultTableData() {
    this.customersconsultTableDatas = []    
  }

  chequeTableDataRemove(item_index) {
    this.customersconsultTableDatas.splice(item_index, 1)
    this.customerconsultForm = this.fb.group({
      commentary: [null],
      debit: [null],
      credit: [null]
    })     
  }  


  accountAddInput($event) {
    this.customersconsultTableDatas.push(
      $event
    )

    this.customerconsultForm.patchValue({
      customersconsultTableDatas: $event
    })     

  

}
clearFilter() {
  this.customerconsultForm.reset()
  this.customerconsultForm.patchValue({
    customersconsult_type: ['1'],
    //appliedapplication_tax_exempt: ['no']
  })
}
}


