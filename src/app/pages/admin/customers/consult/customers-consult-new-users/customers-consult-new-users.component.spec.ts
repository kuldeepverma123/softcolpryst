import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomersConsultNewUsersComponent } from './customers-consult-new-users.component';

describe('CustomersConsultNewUsersComponent', () => {
  let component: CustomersConsultNewUsersComponent;
  let fixture: ComponentFixture<CustomersConsultNewUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomersConsultNewUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomersConsultNewUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
