import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';
import { ApiService } from '../../../../../api.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-customers-consult-new-users',
  templateUrl: './customers-consult-new-users.component.html',
  styleUrls: ['./customers-consult-new-users.component.scss']
})
export class CustomersConsultNewUsersComponent implements OnInit {

  customerconsultNewuser: FormGroup
  isSpinning : boolean
  id  : any

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,   
    private notification: NzNotificationService,   
    private service: ApiService,  
    private route: ActivatedRoute  
  ) { }

  ngOnInit() {
    this.languageTranslate()

    this.formVariablesInit()
    this.route.params.subscribe(params => {
      this.id = params['id'];
      });
      console.log(this.id);
    
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  formVariablesInit() {
    this.customerconsultNewuser = this.fb.group({
      department: ['1'],
      no_card: [null, [Validators.required]],
      name: [null, [Validators.required]],
      last_name: [null, [Validators.required]],
      phone: [null, [Validators.required]],
      mobile: [null, [Validators.required]],
      address: [null, [Validators.required]],
      position: [null, [Validators.required]],
      area: [null, [Validators.required]],
      profession: [null, [Validators.required]],
      mail: [null, [Validators.required]],
      city: [null, [Validators.required]],
      administrator: [null],
      observer: [null],
      customer_id : [this.id]
    })      
  }

  saveCustomer() {
    console.log("i am here")
     this.service.addEmployee(this.customerconsultNewuser.value).subscribe(
      (response) =>{
        console.log(response)
        this.successNotification('success')
        this.customerconsultNewuser.reset();
        this.isSpinning = false;
        
      }, 
      (error) => {
        this.errorNotification('error')
        this.isSpinning = false;
      }
    )
  }
  
  successNotification(type: string): void {
    let acLanguage = localStorage.getItem('acLanguage')
   let tit, message;
  console.log(acLanguage)
  if(acLanguage == "es"){

  tit = 'Solicitud de creación exitosa'

  }else {
    tit = 'Request create successful'
  }

this.notification.create(
  type,
  tit,
  message
   );
  }

  errorNotification(type: string): void {
    let acLanguage = localStorage.getItem('acLanguage')
    let tit, message;
      console.log(acLanguage)
      if(acLanguage == "es"){

      tit = 'Solicitud crear falla'
      message = 'algo está mal'

      }else {
        tit = 'Request create fail'
        message = 'something is wrong'
      }

    this.notification.create(
      type,
      tit,
      message
    );
  }

  clearFilter() {
    this.customerconsultNewuser.reset()
    this.customerconsultNewuser.patchValue({
      department: ['1'],
      customer_tax_exempt: ['no']
    })
  }



}



