import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomersConsultTableComponent } from './customers-consult-table.component';

describe('CustomersConsultTableComponent', () => {
  let component: CustomersConsultTableComponent;
  let fixture: ComponentFixture<CustomersConsultTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomersConsultTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomersConsultTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
