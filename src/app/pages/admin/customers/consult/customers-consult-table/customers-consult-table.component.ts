 import { Component, OnInit, Input } from '@angular/core';

 import { FormBuilder, FormGroup, Validators } from '@angular/forms';
    
  @Component({
    selector: 'app-customers-consult-table',
    templateUrl: './customers-consult-table.component.html',
    styleUrls: ['./customers-consult-table.component.scss']
    })
    export class  CustomersConsultTableComponent implements OnInit {
    
      @Input() customersconsultaccountData: any
    
      //  customersconsult account form
      CustomersConsultAccountForm: FormGroup  
      CustomersConsultAccountVisible = false
    
      // search
      search_title = ''
    
      customersconsultaccountDisplayData: any
    
      sortName: string | null = null
      sortValue: string | null = null
    
      deleteConfirmVisible = false
    
      constructor(
        private fb: FormBuilder
      ) { 
    
      }
    
      ngOnInit() {
        this.CustomersConsultAccountFormInit()
    
        this.customersconsultAccountData()
      }
    
      //  customersconsult account form
      CustomersConsultAccountFormInit() {
        this.CustomersConsultAccountForm = this.fb.group({
          business: [null, [Validators.required]],
          user: [null, [Validators.required]],
          name: [null, [Validators.required]],
          type_of_service: [null, [Validators.required]],
          application_correlative_checks: [null, [Validators.required]]
        })      
      }  
    
      CustomersConsultAccountOpen() {
        this.CustomersConsultAccountVisible = true
      }
    
      CustomersConsultAccountClose() {
        this.CustomersConsultAccountForm.reset()
        this.CustomersConsultAccountVisible = false
      }      
    
      CustomersConsultAccountSave() {    
        for (const i in this.CustomersConsultAccountForm.controls) {
          this.CustomersConsultAccountForm.controls[i].markAsDirty()
          this.CustomersConsultAccountForm.controls[i].updateValueAndValidity()
        }          
    
        if(this.CustomersConsultAccountForm.valid) {
          this.CustomersConsultAccountClose()
        }
      }
    
      customersconsultAccountData() {
        this.customersconsultaccountDisplayData = [...this.customersconsultaccountData]
      }
    
      keyUpSearch() {
        let searchLower = this.search_title.toLowerCase()
    
        const filterFunc = (item: { state: string; assigned_to: string; days: string; id_card: string; business: string; user: string; name: string; type_of_service: string }) => {
          return (
            item.business.toLowerCase().indexOf(searchLower) !== -1 ||
            item.id_card.toLowerCase().indexOf(searchLower) !== -1 ||
            item.assigned_to.toLowerCase().indexOf(searchLower) !== -1 ||
            item.days.toLowerCase().indexOf(searchLower) !== -1 ||
            item.state.toLowerCase().indexOf(searchLower) !== -1 ||
            item.user.toLowerCase().indexOf(searchLower) !== -1 ||
            item.name.toLowerCase().indexOf(searchLower) !== -1 ||
            item.type_of_service.toLowerCase().indexOf(searchLower) !== -1
          )
        }
    
        const data = this.customersconsultaccountData.filter((item: { state: string; assigned_to: string; days: string;  id_card: string; business: string; user: string; name: string; type_of_service: string }) => filterFunc(item))
    
        this.customersconsultaccountDisplayData = data.sort((a, b) =>
          this.sortValue === 'ascend'
            ? a[this.sortName!] > b[this.sortName!]
              ? 1
              : -1
            : b[this.sortName!] > a[this.sortName!]
            ? 1
            : -1
        ) 
        
        if(searchLower.length == 0) {
          this.customersconsultaccountDisplayData = [...this.customersconsultaccountData]
        }    
      }  
    
      // delete confirm
      deleteConfirmOpen() {
        this.deleteConfirmVisible = true
      }
    
      deleteConfirmClose() {
        this.deleteConfirmVisible = false
      }  
    
    }
    
    
    
  
  