import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomersConsultUserFormComponent } from './customers-consult-user-form.component';

describe('CustomersConsultUserFormComponent', () => {
  let component: CustomersConsultUserFormComponent;
  let fixture: ComponentFixture<CustomersConsultUserFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomersConsultUserFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomersConsultUserFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
