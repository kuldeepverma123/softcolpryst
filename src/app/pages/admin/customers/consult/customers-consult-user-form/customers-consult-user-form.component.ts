import { Component, OnInit, Input } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-customers-consult-user-form',
  templateUrl: './customers-consult-user-form.component.html',
  styleUrls: ['./customers-consult-user-form.component.scss']
})
export class CustomersConsultUserFormComponent implements OnInit {
  @Input() consultviewData: any
  customerconsultuserForm: FormGroup

  currentDate:any = new Date()  

  // cheque table data 
  chequeTableDatas = []  

  // search account dialog
  
  accountDatas = []

  // applicationaccount search dialog
  searchApplicationaccountVisible = false
  consultviewDatas = []  

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.customerconsultuserFormInit()


  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  customerconsultuserFormInit() {
    this.customerconsultuserForm = this.fb.group({
    //  application_type: ['1'],
    //  application_tax_exempt: ['no'],
    //   state: [null, [Validators.required]],
    //   assigned_to: [null, [Validators.required]],
    //   business: [null, [Validators.required]],
    //   name: [null, [Validators.required]],
    //   type_of_service: [null, [Validators.required]],
    //   id_card: [null, [Validators.required]],
    //   chequeTableDatas: []
    })      
  }
  


}

