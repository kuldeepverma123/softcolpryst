import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomersConsultUserTableComponent } from './customers-consult-user-table.component';

describe('CustomersConsultUserTableComponent', () => {
  let component: CustomersConsultUserTableComponent;
  let fixture: ComponentFixture<CustomersConsultUserTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomersConsultUserTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomersConsultUserTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
