import { Component, OnInit, Input } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-customers-consult-user-table',
  templateUrl: './customers-consult-user-table.component.html',
  styleUrls: ['./customers-consult-user-table.component.scss']
})
export class CustomersConsultUserTableComponent implements OnInit {

  @Input() consultviewData: any

  // CustomersConsultUserForm form
  CustomersConsultUserForm: FormGroup  
  CustomersConsultUserVisible = false

  // search
  search_title = ''

  customerconsultusertableDisplayData: any

  sortName: string | null = null
  sortValue: string | null = null

  deleteConfirmVisible = false

  constructor(
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    

    this.consultViewData()
  }

 

  ApplicationAccountOpen() {
    this.CustomersConsultUserVisible = true
  }

  CustomersConsultUserClose() {
    this.CustomersConsultUserForm.reset()
    this.CustomersConsultUserVisible = false
  }      

  ApplicationAccountSave() {    
    for (const i in this.CustomersConsultUserForm.controls) {
      this.CustomersConsultUserForm.controls[i].markAsDirty()
      this.CustomersConsultUserForm.controls[i].updateValueAndValidity()
    }          

    if(this.CustomersConsultUserForm.valid) {
      this.CustomersConsultUserClose()
    }
  }

  consultViewData() {
    this.customerconsultusertableDisplayData = [...this.consultviewData]
  }


  // delete confirm
  deleteConfirmOpen() {
    this.deleteConfirmVisible = true
  }

  deleteConfirmClose() {
    this.deleteConfirmVisible = false
  }  

}



