import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomersConsultViewComponent } from './customers-consult-view.component';

describe('CustomersConsultViewComponent', () => {
  let component: CustomersConsultViewComponent;
  let fixture: ComponentFixture<CustomersConsultViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomersConsultViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomersConsultViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
