  import { Component, OnInit } from '@angular/core';

  import { TranslateService } from '@ngx-translate/core';
  
  @Component({
    selector: 'app-customers-consult-view',
    templateUrl: './customers-consult-view.component.html',
    styleUrls: ['./customers-consult-view.component.scss']
  })
  export class  CustomersConsultViewComponent implements OnInit {
  
    consultviewDatas: any
  
    activeCollapse = false  
  
    constructor(
      private translate: TranslateService
    ) { 
  
    }
  
    ngOnInit() {
      this.languageTranslate()
  
      this.consultviewData()    
    }
  
    languageTranslate() {
      let acLanguage = localStorage.getItem('acLanguage')
  
      if(acLanguage != null && acLanguage != '') {
        this.translate.use(acLanguage)
      } else {
        this.translate.setDefaultLang('es')
      }    
    }
    
    showFilter() {
      this.activeCollapse = !this.activeCollapse
    } 
    
    receiveSearchData($event) {
      // console.log('receiveSearchData', $event)
    }
    
    submitForm() {
      this.consultviewDatas = []
    }
    
    consultviewData() {
      this.consultviewDatas = [
        {
          user_id: '123',
          city: 'Barranquilla',
          name: 'ciody castro cantillo',
          cellular: '45447477',
          mail: 'alexander@kingocean.com',
          profession: 'it does not refer',
          state: 'A'
        }                  
      ]
    }
  }
  
  





