import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomersConsultComponent } from './customers-consult.component';

describe('CustomersConsultComponent', () => {
  let component: CustomersConsultComponent;
  let fixture: ComponentFixture<CustomersConsultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomersConsultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomersConsultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
