import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ApiService } from '../../../../../api.service';
@Component({
  selector: 'app-customers-consult',
   templateUrl: './customers-consult.component.html',
   styleUrls: ['./customers-consult.component.scss']
})
export class  CustomersConsultComponent implements OnInit {

  customersconsultaccountDatas: any
  personalconsultForm: FormGroup
  activeCollapse = false  

  constructor(
    private translate: TranslateService,
    private service: ApiService, 
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()
    this.customersconsultaccountData() 
    this.personalconsultFormInit()
    
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  showFilter() {
    this.activeCollapse = !this.activeCollapse
  } 
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
  }

  personalconsultFormInit() {
    this.personalconsultForm = this.fb.group({
     nit_cedula: [null],
     business_name: [null],
     companyname: [null],
     email: [null]
    })      
  }

  saveCheque() {
    this.customersconsultaccountDatas = []
    this.service.customerFilter(this.personalconsultForm.value).subscribe((response) => {
    this.customersconsultaccountDatas = response['data'];
      console.log("length ===",this.customersconsultaccountDatas);
    })
  }
  
  submitForm() {
    this.customersconsultaccountDatas = []
  }

  clearFilter() {
    this.personalconsultForm.reset()
    this.customersconsultaccountData()
    
  }
  
  customersconsultaccountData() {

    // this.customersconsultaccountDatas = [
    //   {
    //     business: 'ocean',
    //     user: 'Alex',
    //     application: 'Financiera',
    //     type_of_service: 'Financial',
    //     name: 'Adam',
    //     id_card: '123456',
    //     state: 'No',
    //     assigned_to: '2324',
    //     days: '5 days'
    //   }                  
    //]
    
    this.service.getCustomers().subscribe((response) => {
      console.log("response", response['data'])
      this.customersconsultaccountDatas = response['data']
      console.log("after", this.customersconsultaccountDatas)
    })
  }
}




