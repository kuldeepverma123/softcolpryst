import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';
import { ApiService } from '../../../../../api.service';
import { FormBuilder, FormGroup, FormControl, FormArray,  Validators } from '@angular/forms';
import { NzNotificationService } from 'ng-zorro-antd/notification';
@Component({
  selector: 'app-customers-create',
 templateUrl: './customers-create.component.html',
 styleUrls: ['./customers-create.component.scss']
})
export class CustomersCreateComponent implements OnInit {
  activate_account_link:any
  customercreate: FormGroup
  current = 0
  index = "First-content"
  items: FormArray
  disabled : true
  services : any
  products : any
  isSpinning : boolean
  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private notification: NzNotificationService,   
    private service: ApiService     
  ) { }

  pre(): void {
    this.current -= 1;
    this.changeContent();
  }

  next(): void {
    // this.saveCustomer();
    this.current += 1;
    this.changeContent();
  }

  done(): void {
    console.log('done');
  }


  changeContent(): void {
    switch (this.current) {
      case 0: {
        this.index = "First-content";
        break;
      }
      case 1: {
        this.index = "Second-content";
        break;
      }
      default: {
        this.index = "First-content";
      }
    }
  }

  ngOnInit() {
    this.languageTranslate()
    this.getProduct()
    this.formVariablesInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  formVariablesInit() {
    this.customercreate = this.fb.group({
      department: ['1'],
      customer_tax_exempt: ['no'],
      business_name: [null, [Validators.required]],
      company_name: [null, [Validators.required]],
      mail: [null, [Validators.required]],
      city: [null, [Validators.required]],
      landline: [null, [Validators.required]],
      phone: [null, [Validators.required]],
      card_nit: [null, [Validators.required]],
      legal_rep: [null, [Validators.required]],
      direct_employees: [null, [Validators.required]],
      indirect_direct_employess: [null, [Validators.required]],
      area: [null, [Validators.required]],
      activate_account_links: [null, [Validators.required]],
      cost_center: [null, [Validators.required]],
      work_center: [null, [Validators.required]],
      billing_date: [null, [Validators.required]],
      no_of_employee: [null, [Validators.required]],
      attach_photo: [],
      address: [null, [Validators.required]],
      items: this.fb.array([ this.createItem() ])
    })      
  }

  saveCustomer() {
   
     console.log("any here");

    // for (const i in this.customercreate.controls) {
    //   this.customercreate.controls[i].markAsDirty()
    //   this.customercreate.controls[i].updateValueAndValidity()
    // }
    // if(this.customercreate.valid) {
    //   console.log('this.customercreate.value', this.customercreate.value)
    // }

    if (this.customercreate.invalid) {
      return;
     }
     this.service.addCustomer(this.customercreate.value).subscribe(
      (response) =>{
        console.log(response)
        this.successNotification('success')
        this.index = "Second-content"
        this.customercreate.reset();
        this.isSpinning = false;
        
      }, 
      (error) => {
        this.errorNotification('error')
        this.isSpinning = false;
      }
    )
  }

  clearFilter() {
    this.customercreate.reset()
    this.customercreate.patchValue({
      department: ['1'],
      //customer_tax_exempt: ['no']
    })
  }
  
  addAddress() {

    let its =  this.customercreate.value.items.length;

    console.log("langth",its);
    
    console.log("after", this.customercreate.value.items[its-1]); 
    if(its > 1){

    if(this.customercreate.value.items[its - 1].price == '' || this.customercreate.value.items[its -1 ].service == ''){
      console.log("error");
      this.createNotification('error');
      return
    }
  }else{
    if(this.customercreate.value.items[0].price == '' || this.customercreate.value.items[0].service == ''){
      console.log("error");
      this.createNotification('error')
      return
    }
  }
    
    this.items = this.customercreate.get('items') as FormArray;
    this.items.push(this.createItem());  
 
  }
  
  removeAddress(i: number) {
     console.log(i);
     this.items = this.customercreate.get('items') as FormArray;
     let its =  this.customercreate.value.items.length;
       if(its > 1) {
        this.items.removeAt(i);
      }
  }

  createItem(): FormGroup {
    return this.fb.group({
      service: '',
      price: ''
    });
  }

  createNotification(type: string): void {


    let acLanguage = localStorage.getItem('acLanguage')
    let tit, message;
      console.log(acLanguage)
      if(acLanguage == "es"){

      tit = 'Error de formulario'
      message = 'Complete todos los campos y luego cree un nuevo servicio'

      }else {

        tit = 'Form Error'
        message = 'Please fill all field then create new service'
      }

    this.notification.create(
      type,
      tit,
      message
    );
  }


  getProduct() {
    this.service.getAllProduct().subscribe((response) => {
      console.log("response", response['data'])
      this.products = response['data']
      console.log("after", this.products)
    })
   }

   successNotification(type: string): void {
    let acLanguage = localStorage.getItem('acLanguage')
   let tit, message;
  console.log(acLanguage)
  if(acLanguage == "es"){

  tit = 'Solicitud de creación exitosa'

  }else {
    tit = 'Request create successful'
  }

this.notification.create(
  type,
  tit,
  message
   );
  }

  errorNotification(type: string): void {
    let acLanguage = localStorage.getItem('acLanguage')
    let tit, message;
      console.log(acLanguage)
      if(acLanguage == "es"){

      tit = 'Solicitud crear falla'
      message = 'algo está mal'

      }else {
        tit = 'Request create fail'
        message = 'something is wrong'
      }

    this.notification.create(
      type,
      tit,
      message
    );
  }
  

}





