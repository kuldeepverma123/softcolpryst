import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomersChangePasswordComponent } from './change-password/customers-change-password/customers-change-password.component';
import { CustomersCreateComponent } from './create/customers-create/customers-create.component';
import { CustomersConsultComponent } from './consult/customers-consult/customers-consult.component';
import { CustomersConsultNewUsersComponent } from './consult/customers-consult-new-users/customers-consult-new-users.component';
import { CustomersConsultEditComponent } from './consult/customers-consult-edit/customers-consult-edit.component';
import { CustomersConsultViewComponent } from './consult/customers-consult-view/customers-consult-view.component';
import { CustomersConsultDeleteComponent } from './consult/customers-consult-delete/customers-consult-delete.component';


const routes: Routes = [
  {
    path: 'change-password',
    component: CustomersChangePasswordComponent
  },
  {
    path: 'create',
    component: CustomersCreateComponent
  },
  {
    path: 'consult',
    component: CustomersConsultComponent
  },
  {
    path: 'consult/new-user/:id',
    component: CustomersConsultNewUsersComponent
  },
  {
    path: 'consult/edit/:id',
    component: CustomersConsultEditComponent
  },
  {
    path: 'consult/view/:id',
    component: CustomersConsultViewComponent
  },
  {
    path: 'consult/delete/:id',
    component: CustomersConsultDeleteComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomersRoutingModule { }
