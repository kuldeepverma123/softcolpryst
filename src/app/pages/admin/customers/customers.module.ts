import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgZorroAntdModule } from 'ng-zorro-antd';

// import ngx-translate and the http loader
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import { HttpClient, HttpClientModule } from '@angular/common/http';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { CustomersRoutingModule } from './customers-routing.module';
import { CustomersConsultComponent } from './consult/customers-consult/customers-consult.component';
import { CustomersCreateComponent } from './create/customers-create/customers-create.component';
import { CustomersChangePasswordComponent } from './change-password/customers-change-password/customers-change-password.component';
import { CustomersConsultFormComponent } from './consult/customers-consult-form/customers-consult-form.component';
import { CustomersConsultTableComponent } from './consult/customers-consult-table/customers-consult-table.component';
import { CustomersConsultNewUsersComponent } from './consult/customers-consult-new-users/customers-consult-new-users.component';
import { CustomersConsultEditComponent } from './consult/customers-consult-edit/customers-consult-edit.component';
import { CustomersConsultViewComponent } from './consult/customers-consult-view/customers-consult-view.component';
import { CustomersConsultUserFormComponent } from './consult/customers-consult-user-form/customers-consult-user-form.component';
import { CustomersConsultUserTableComponent } from './consult/customers-consult-user-table/customers-consult-user-table.component';
import { CustomersConsultDeleteComponent } from './consult/customers-consult-delete/customers-consult-delete.component';


@NgModule({
  declarations: [CustomersConsultComponent, CustomersCreateComponent, CustomersChangePasswordComponent, CustomersConsultFormComponent, CustomersConsultTableComponent, CustomersConsultNewUsersComponent, CustomersConsultEditComponent, CustomersConsultViewComponent, CustomersConsultUserFormComponent, CustomersConsultUserTableComponent, CustomersConsultDeleteComponent],
  imports: [
    HttpClientModule,
    NgZorroAntdModule,
    CommonModule,
    CustomersRoutingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class CustomersModule { }
// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
