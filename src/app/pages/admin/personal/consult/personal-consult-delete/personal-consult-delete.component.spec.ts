import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalConsultDeleteComponent } from './personal-consult-delete.component';

describe('PersonalConsultDeleteComponent', () => {
  let component: PersonalConsultDeleteComponent;
  let fixture: ComponentFixture<PersonalConsultDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonalConsultDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalConsultDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
