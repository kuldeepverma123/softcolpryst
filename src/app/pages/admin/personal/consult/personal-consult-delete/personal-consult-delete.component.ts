import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ApiService } from '../../../../../api.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzNotificationService } from 'ng-zorro-antd/notification';
@Component({
  selector: 'app-personal-consult-delete',
  templateUrl: './personal-consult-delete.component.html',
  styleUrls: ['./personal-consult-delete.component.scss']
})
export class PersonalConsultDeleteComponent implements OnInit {

  customerconsultdelete: FormGroup
  userdata : any
  id: any
  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,   
    private service: ApiService,     
    private route: ActivatedRoute, 
    private notification: NzNotificationService   
  ) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.languageTranslate()
    this.getUserData()
    this.formVariablesInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    
  getUserData(){
    this.service.getUsersById(this.id).subscribe((response) => {
      console.log("response", response['data'])  
      this.userdata = response['data'][0]; 
    })
  }
  formVariablesInit() {
    this.customerconsultdelete = this.fb.group({
      // department: ['1'],
      // no_card: [null, [Validators.required]],
      // name: [null, [Validators.required]],
      // last_name: [null, [Validators.required]],
      // phone: [null, [Validators.required]],
      // mobile: [null, [Validators.required]],
      // address: [null, [Validators.required]],
      // position: [null, [Validators.required]],
      // area: [null, [Validators.required]],
      // profession: [null, [Validators.required]],
      // mail: [null, [Validators.required]],
      // city: [null, [Validators.required]],
      // administrator: [null],
      // observer: [null]
    })      
  }

  consultdelete() {
    this.service.deleteUserById(this.id).subscribe((response) => {
      console.log(response)
      this.successNotification('success')
     // this.applicationEdit.reset();
    }, 
    (error) => {
      this.errorNotification('error');
    })
}
successNotification(type: string): void {
  let acLanguage = localStorage.getItem('acLanguage')
 let tit, message;
console.log(acLanguage)
if(acLanguage == "es"){

tit = 'Solicitud de creación exitosa'

}else {
  tit = 'Request create successful'
}

this.notification.create(
type,
tit,
message
 );
}

errorNotification(type: string): void {
  let acLanguage = localStorage.getItem('acLanguage')
  let tit, message;
    console.log(acLanguage)
    if(acLanguage == "es"){

    tit = 'Solicitud crear falla'
    message = 'algo está mal'

    }else {
      tit = 'Request create fail'
      message = 'something is wrong'
    }

  this.notification.create(
    type,
    tit,
    message
  );
}

  clearFilter() {
    this.customerconsultdelete.reset()
    this.customerconsultdelete.patchValue({
      //department: ['1'],
      //customer_tax_exempt: ['no']
    })
  }



}






