import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalConsultEditComponent } from './personal-consult-edit.component';

describe('PersonalConsultEditComponent', () => {
  let component: PersonalConsultEditComponent;
  let fixture: ComponentFixture<PersonalConsultEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonalConsultEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalConsultEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
