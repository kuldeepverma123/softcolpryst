import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ApiService } from '../../../../../api.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzNotificationService } from 'ng-zorro-antd/notification';
@Component({
  selector: 'app-personal-consult-edit',
  templateUrl: './personal-consult-edit.component.html',
  styleUrls: ['./personal-consult-edit.component.scss']
})
export class PersonalConsultEditComponent implements OnInit {

  personalconsultEdit: FormGroup
  id :any
  roles : any

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private service: ApiService, 
    private route: ActivatedRoute,
    private notification: NzNotificationService     
  ) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.languageTranslate()
    this.formVariablesInit()
    this.getUserData()
    this.getUserRoles()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    
  getUserData(){
    this.service.getUsersById(this.id).subscribe((response) => {
      console.log("response", response['data'])
      this.personalconsultEdit.controls['department'].setValue(response['data'][0].department);
      this.personalconsultEdit.controls['city'].setValue(response['data'][0].city);
      this.personalconsultEdit.controls['id_card'].setValue(response['data'][0].identity);
      this.personalconsultEdit.controls['email'].setValue(response['data'][0].email);
      this.personalconsultEdit.controls['name'].setValue(response['data'][0].firstName);
      this.personalconsultEdit.controls['user_type'].setValue(response['data'][0].usertype);
      this.personalconsultEdit.controls['last_name'].setValue(response['data'][0].lastName);
      this.personalconsultEdit.controls['phone'].setValue(response['data'][0].mobile);
      this.personalconsultEdit.controls['profession'].setValue(response['data'][0].possition);
     
    })
  }

  formVariablesInit() {
    this.personalconsultEdit = this.fb.group({
      department: ['1'],
      city: [null, [Validators.required]],
      id_card: [null, [Validators.required]],
      name: [null, [Validators.required]],
      //department: [null, [Validators.required]],
      user_type: [null, [Validators.required]],
      user: [null, [Validators.required]],
      last_name: [null, [Validators.required]],
      phone: [null, [Validators.required]],
      profession: [null, [Validators.required]],
      email: [null, [Validators.required]],
     })      
  }

  saveCustomer() {
      console.log('this.personalconsultEdit.value', this.personalconsultEdit.value)
      this.service.updateUserById(this.personalconsultEdit.value,this.id).subscribe((response) => {
        console.log(response)
        this.successNotification('success')
       // this.applicationEdit.reset();
      }, 
      (error) => {
        this.errorNotification('error');
      })
  }
  successNotification(type: string): void {
    let acLanguage = localStorage.getItem('acLanguage')
   let tit, message;
  console.log(acLanguage)
  if(acLanguage == "es"){

  tit = 'Solicitud de creación exitosa'

  }else {
    tit = 'Request create successful'
  }

this.notification.create(
  type,
  tit,
  message
   );
  }

  errorNotification(type: string): void {
    let acLanguage = localStorage.getItem('acLanguage')
    let tit, message;
      console.log(acLanguage)
      if(acLanguage == "es"){

      tit = 'Solicitud crear falla'
      message = 'algo está mal'

      }else {
        tit = 'Request create fail'
        message = 'something is wrong'
      }

    this.notification.create(
      type,
      tit,
      message
    );
  }

  clearFilter() {
    this.personalconsultEdit.reset()
    this.personalconsultEdit.patchValue({
      department: ['1'],
      //customer_tax_exempt: ['no']
    })
  }

  getUserRoles(){
    this.service.getUserRoles().subscribe((response) => {
      console.log("response", response['data'])
      this.roles = response['data']
      console.log("after", this.roles)
    })
  }

}





