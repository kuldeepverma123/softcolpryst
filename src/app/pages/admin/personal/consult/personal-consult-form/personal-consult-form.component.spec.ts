import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalConsultFormComponent } from './personal-consult-form.component';

describe('PersonalConsultFormComponent', () => {
  let component: PersonalConsultFormComponent;
  let fixture: ComponentFixture<PersonalConsultFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonalConsultFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalConsultFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
