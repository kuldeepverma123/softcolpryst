import { Component, OnInit, Input } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({ 
  selector: 'app-personal-consult-form',
  templateUrl: './personal-consult-form.component.html',
  styleUrls: ['./personal-consult-form.component.scss']
})
export class PersonalConsultFormComponent  implements OnInit {
  @Input() personalconsultData: any
  personalconsultForm: FormGroup

  currentDate:any = new Date()  

  // personalconsultform table data 
  personalconsultformTableDatas = []  

  // search account dialog
  
  accountDatas = []

  // personalconsult search dialog
  personalconsultVisible = false
  personalconsultDatas = []  

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.personalconsultFormInit()

    this.personalconsultformTableData()  

  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  personalconsultFormInit() {
    this.personalconsultForm = this.fb.group({
     //application_type: ['1'],
     //application_tax_exempt: ['no'],
     id_card: [null, [Validators.required]],
     user_type: [null, [Validators.required]],
     name: [null, [Validators.required]],
     last_name: [null, [Validators.required]],
     user: [null, [Validators.required]],
      chequeTableDatas: []
    })      
  }
  
  saveCheque() {
    for (const i in this.personalconsultForm.controls) {
      this.personalconsultForm.controls[i].markAsDirty()
      this.personalconsultForm.controls[i].updateValueAndValidity()
    }
    if(this.personalconsultForm.valid) {
      console.log('this.personalconsultForm.value', this.personalconsultForm.value)
    }
  }

  personalconsultformTableData() {
    this.personalconsultformTableDatas = []    
  }

  personalconsultformTableDataRemove(item_index) {
    this.personalconsultformTableDatas.splice(item_index, 1)
    this.personalconsultForm = this.fb.group({
      commentary: [null],
      debit: [null],
      credit: [null]
    })     
  }  


  accountAddInput($event) {
    this.personalconsultformTableDatas.push(
      $event
    )

    this.personalconsultForm.patchValue({
      personalconsultformTableDatas: $event
    })     

  

}
clearFilter() {
  this.personalconsultForm.reset()
  this.personalconsultForm.patchValue({
    customersconsult_type: ['1'],
    //appliedapplication_tax_exempt: ['no']
  })
}
}


