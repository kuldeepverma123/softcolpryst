import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalConsultTableComponent } from './personal-consult-table.component';

describe('PersonalConsultTableComponent', () => {
  let component: PersonalConsultTableComponent;
  let fixture: ComponentFixture<PersonalConsultTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonalConsultTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalConsultTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
