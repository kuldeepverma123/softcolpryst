import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalConsultViewComponent } from './personal-consult-view.component';

describe('PersonalConsultViewComponent', () => {
  let component: PersonalConsultViewComponent;
  let fixture: ComponentFixture<PersonalConsultViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonalConsultViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalConsultViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
