import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ApiService } from '../../../../../api.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-personal-consult-view',
  templateUrl: './personal-consult-view.component.html',
  styleUrls: ['./personal-consult-view.component.scss']
})
export class PersonalConsultViewComponent implements OnInit {

  customerconsultview: FormGroup
  id: any
  userdata : any
  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private service: ApiService,     
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.languageTranslate()
    this.getUserData()
    this.formVariablesInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  formVariablesInit() {
    this.customerconsultview = this.fb.group({
      // department: ['1'],
      // no_card: [null, [Validators.required]],
      // name: [null, [Validators.required]],
      // last_name: [null, [Validators.required]],
      // phone: [null, [Validators.required]],
      // mobile: [null, [Validators.required]],
      // address: [null, [Validators.required]],
      // position: [null, [Validators.required]],
      // area: [null, [Validators.required]],
      // profession: [null, [Validators.required]],
      // mail: [null, [Validators.required]],
      // city: [null, [Validators.required]],
      // administrator: [null],
      // observer: [null]
    })      
  }

  getUserData(){
    this.service.getUsersById(this.id).subscribe((response) => {
      console.log("response", response['data'])  
      this.userdata = response['data'][0]; 
    })
  }

  saveCustomer() {
    for (const i in this.customerconsultview.controls) {
      this.customerconsultview.controls[i].markAsDirty()
      this.customerconsultview.controls[i].updateValueAndValidity()
    }
    if(this.customerconsultview.valid) {
      //console.log('this.customerconsultview.value', this.customerconsultview.value)
    }
  }

  clearFilter() {
    this.customerconsultview.reset()
    this.customerconsultview.patchValue({
      //department: ['1'],
      //customer_tax_exempt: ['no']
    })
  }



}





