import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ApiService } from '../../../../../api.service';

@Component({
  selector: 'app-personal-consult',
  templateUrl: './personal-consult.component.html',
  styleUrls: ['./personal-consult.component.scss']
})
export class  PersonalConsultComponent implements OnInit {

  personalconsultDatas: any = []
  personalconsultForm: FormGroup
  activeCollapse = false 
  roles : any 

  constructor(
    private translate: TranslateService,
    private service: ApiService,
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()
    this.getUserRoles()
    this.personalconsultFormInit()
    this.personalconsultData()  
   
  }

  personalconsultFormInit() {
    this.personalconsultForm = this.fb.group({
     //application_type: ['1'],
     //application_tax_exempt: ['no'],
     id_card: [null],
     user_type: [null],
     name: [null],
     last_name: [null],
      chequeTableDatas: []
    })      
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  } 
  
  showFilter() {
    this.activeCollapse = !this.activeCollapse
  } 
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
  }
  
  submitForm() {
    this.personalconsultDatas = []
  }

 
  clearFilter() {
    this.personalconsultForm.reset()
    this.personalconsultForm.patchValue({
      customersconsult_type: ['1'],
      //appliedapplication_tax_exempt: ['no']
    })
  }
  getUserRoles(){
    this.service.getUserRoles().subscribe((response) => {
      console.log("response", response['data'])
      this.roles = response['data']
      console.log("after", this.roles)
    })
  }
  personalconsultData() {
    this.service.getUsers().subscribe((response) => {
      console.log("response", response['data']);
      this.personalconsultDatas = response['data'];
      console.log("length",this.personalconsultDatas.length);
    })
  }
  saveCheque() {
    this.personalconsultDatas = []
    this.service.userFilter(this.personalconsultForm.value).subscribe((response) => {
     
      this.personalconsultDatas = response['data'];
      console.log("length ===",this.personalconsultDatas);
    })
  }
}




