import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../../../../../api.service';
import { NzNotificationService } from 'ng-zorro-antd/notification';

@Component({
  selector: 'app-personal-create',
  templateUrl: './personal-create.component.html',
  styleUrls: ['./personal-create.component.scss']
})
export class PersonalCreateComponent implements OnInit {
  activate_account_link:any
  personalcreateform: FormGroup
  isSpinning : boolean
  roles : any

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private service:ApiService,
    private notification: NzNotificationService

  ) { }

  ngOnInit() {
    this.languageTranslate()
    this.getUserRoles()
    this.formVariablesInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  formVariablesInit() {
    this.personalcreateform = this.fb.group({
      department: ['1'],
      customer_tax_exempt: ['no'],
      user_type: [null, [Validators.required]],
      mail: [null, [Validators.required]],
      id_card: [null, [Validators.required]],
      name: [null, [Validators.required]],
      last_name: [null, [Validators.required]],
      city: [null],
      phone: [null, [Validators.required]],
      mobile: [null, [Validators.required]],
      position: [null, [Validators.required]],
      area: [null, [Validators.required]],
      profession: [null, [Validators.required]],
      attach_photo: [null]
    })      
  }

  saveCustomer() {
    for (const i in this.personalcreateform.controls) {
      this.personalcreateform.controls[i].markAsDirty()
      this.personalcreateform.controls[i].updateValueAndValidity()
    }
    if(this.personalcreateform.valid) {
      console.log('this.personalcreateform.value', this.personalcreateform.value)
    }
  }

  clearFilter() {
    this.personalcreateform.reset()
    this.personalcreateform.patchValue({
      department: ['1'],
      //customer_tax_exempt: ['no']
    })
  }

  submitForm() {
    if (this.personalcreateform.invalid) {
      return;
  }
    this.isSpinning = true;
    this.service.createUser(this.personalcreateform.value).subscribe(
      (response) =>{
        console.log(response)
        this.successNotification('success')
        this.personalcreateform.reset();
        this.isSpinning = false;
      }, 
      (error) => {
        this.errorNotification('error')
        this.isSpinning = false;
      }
    )
  }
 
  successNotification(type: string): void {
    let acLanguage = localStorage.getItem('acLanguage')
   let tit, message;
  console.log(acLanguage)
  if(acLanguage == "es"){

  tit = 'Solicitud de creación exitosa'

  }else {
    tit = 'Request create successful'
  }

this.notification.create(
  type,
  tit,
  message
   );
  }

  errorNotification(type: string): void {
    let acLanguage = localStorage.getItem('acLanguage')
    let tit, message;
      console.log(acLanguage)
      if(acLanguage == "es"){

      tit = 'Solicitud crear falla'
      message = 'algo está mal'

      }else {
        tit = 'Request create fail'
        message = 'something is wrong'
      }

    this.notification.create(
      type,
      tit,
      message
    );
  }

  getUserRoles(){
    this.service.getUserRoles().subscribe((response) => {
      console.log("response", response['data'])
      this.roles = response['data']
      console.log("after", this.roles)
    })
  }
}






