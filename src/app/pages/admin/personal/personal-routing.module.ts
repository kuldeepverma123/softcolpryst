import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PersonalCreateComponent } from './create/personal-create/personal-create.component';
import { PersonalConsultComponent } from './consult/personal-consult/personal-consult.component';
import { PersonalConsultEditComponent } from './consult/personal-consult-edit/personal-consult-edit.component';
import { PersonalConsultViewComponent } from './consult/personal-consult-view/personal-consult-view.component';
import { PersonalConsultDeleteComponent } from './consult/personal-consult-delete/personal-consult-delete.component';


const routes: Routes = [
  {
    path: 'create',
    component:PersonalCreateComponent
  },
  {
    path: 'consult',
    component:PersonalConsultComponent
  },
  {
    path: 'consult/edit/:id',
    component:PersonalConsultEditComponent
  },
  {
    path: 'consult/view/:id',
    component:PersonalConsultViewComponent
  },
  {
    path: 'consult/delete/:id',
    component:PersonalConsultDeleteComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PersonalRoutingModule { }
