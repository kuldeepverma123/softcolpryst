import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgZorroAntdModule } from 'ng-zorro-antd';

// import ngx-translate and the http loader
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import { HttpClient, HttpClientModule } from '@angular/common/http';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { PersonalRoutingModule } from './personal-routing.module';
import { PersonalCreateComponent } from './create/personal-create/personal-create.component';
import { PersonalConsultComponent } from './consult/personal-consult/personal-consult.component';
import { PersonalConsultFormComponent } from './consult/personal-consult-form/personal-consult-form.component';
import { PersonalConsultTableComponent } from './consult/personal-consult-table/personal-consult-table.component';
import { PersonalConsultEditComponent } from './consult/personal-consult-edit/personal-consult-edit.component';
import { PersonalConsultViewComponent } from './consult/personal-consult-view/personal-consult-view.component';
import { PersonalConsultDeleteComponent } from './consult/personal-consult-delete/personal-consult-delete.component';


@NgModule({
  declarations: [PersonalCreateComponent, PersonalConsultComponent, PersonalConsultFormComponent, PersonalConsultTableComponent, PersonalConsultEditComponent, PersonalConsultViewComponent, PersonalConsultDeleteComponent],
  imports: [
    HttpClientModule,
    NgZorroAntdModule,
    CommonModule,
    PersonalRoutingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class PersonalModule { }
// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

