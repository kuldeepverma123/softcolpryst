import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../../../../api.service';
@Component({
  selector: 'app-add-new-product',
  templateUrl: './add-new-product.component.html',
  styleUrls: ['./add-new-product.component.scss']
})
export class AddNewProductComponent implements OnInit {

  addnewProduct: FormGroup
  isSpinning : boolean

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private service: ApiService,
    private notification: NzNotificationService    
  ) { }

  ngOnInit() {
    this.languageTranslate()

    this.formVariablesInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  formVariablesInit() {
    this.addnewProduct = this.fb.group({
      product_name: [null, [Validators.required]],
      code: [null, [Validators.required]]
    })      
  }

  saveProduct() {
    if (this.addnewProduct.invalid) {
          return;
    }
    this.service.createProduct(this.addnewProduct.value).subscribe(
      (response) =>{
        console.log(response)
        this.successNotification('success')
        this.addnewProduct.reset();
        this.isSpinning = false;
      }, 
      (error) => {
        this.errorNotification('error');
        this.isSpinning = false;
      }
    )


    // for (const i in this.addnewProduct.controls) {
    //   this.addnewProduct.controls[i].markAsDirty()
    //   this.addnewProduct.controls[i].updateValueAndValidity()
    // }
    // if(this.addnewProduct.valid) {
    //   console.log('this.addnewProduct.value', this.addnewProduct.value)
    // }
  }

  successNotification(type: string): void {
    let acLanguage = localStorage.getItem('acLanguage')
   let tit, message;
  console.log(acLanguage)
  if(acLanguage == "es"){

  tit = 'Solicitud de creación exitosa'

  }else {
    tit = 'Request create successful'
  }

this.notification.create(
  type,
  tit,
  message
   );
  }

  errorNotification(type: string): void {
    let acLanguage = localStorage.getItem('acLanguage')
    let tit, message;
      console.log(acLanguage)
      if(acLanguage == "es"){

      tit = 'Solicitud crear falla'
      message = 'algo está mal'

      }else {
        tit = 'Request create fail'
        message = 'something is wrong'
      }

    this.notification.create(
      type,
      tit,
      message
    );
  }

  clearFilter() {
    this.addnewProduct.reset()
    this.addnewProduct.patchValue({
      //customer_type: ['1'],
      //customer_tax_exempt: ['no']
    })
  }



}

