import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../../../../api.service';
@Component({
  selector: 'app-add-new-service',
  templateUrl: './add-new-service.component.html',
  styleUrls: ['./add-new-service.component.scss']
})
export class AddNewServiceComponent implements OnInit {

  addnewServices: FormGroup
  products :any
  isSpinning : boolean

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private service: ApiService,   
    private notification: NzNotificationService  
  ) { }

  ngOnInit() {
    this.languageTranslate()

    this.formVariablesInit()

    this.getProduct()
   
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  formVariablesInit() {
    this.addnewServices = this.fb.group({
      service_name: [null, [Validators.required]],
      code: [null, [Validators.required]],
      associated_product: [null, [Validators.required]]
    })      
  }

  saveServices() {
    // for (const i in this.addnewServices.controls) {
    //   this.addnewServices.controls[i].markAsDirty()
    //   this.addnewServices.controls[i].updateValueAndValidity()
    // }
    // if(this.addnewServices.valid) {
    //   console.log('this.addnewServices.value', this.addnewServices.value)
    // }

    if (this.addnewServices.invalid) {
      return;
}
this.service.createService(this.addnewServices.value).subscribe(
  (response) =>{
    console.log(response)
    this.successNotification('success')
    this.addnewServices.reset();
    this.isSpinning = false;
  }, 
  (error) => {
    this.errorNotification('error');
    this.isSpinning = false;
  }
)
  }

  clearFilter() {
    this.addnewServices.reset()
    this.addnewServices.patchValue({
      //customer_type: ['1'],
      //customer_tax_exempt: ['no']
    })
  }
  
 getProduct() {
  this.service.getAllProduct().subscribe((response) => {
    console.log("response", response['data'])
    this.products = response['data']
    console.log("after", this.products)
  })
 }

 successNotification(type: string): void {
  let acLanguage = localStorage.getItem('acLanguage')
 let tit, message;
console.log(acLanguage)
if(acLanguage == "es"){

tit = 'Solicitud de creación exitosa'

}else {
  tit = 'Request create successful'
}

this.notification.create(
type,
tit,
message
 );
}

errorNotification(type: string): void {
  let acLanguage = localStorage.getItem('acLanguage')
  let tit, message;
    console.log(acLanguage)
    if(acLanguage == "es"){

    tit = 'Solicitud crear falla'
    message = 'algo está mal'

    }else {
      tit = 'Request create fail'
      message = 'something is wrong'
    }

  this.notification.create(
    type,
    tit,
    message
  );
}

}


