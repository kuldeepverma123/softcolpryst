import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-product-delete',
  templateUrl: './product-delete.component.html',
  styleUrls: ['./product-delete.component.scss']
})
export class  ProductDeleteComponent implements OnInit {

  productDelete: FormGroup

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder    
  ) { }

  ngOnInit() {
    this.languageTranslate()

    this.formVariablesInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  formVariablesInit() {
    this.productDelete = this.fb.group({
      product_name: [null, [Validators.required]],
      code: [null, [Validators.required]]
    })      
  }

  Product() {
    for (const i in this.productDelete.controls) {
      this.productDelete.controls[i].markAsDirty()
      this.productDelete.controls[i].updateValueAndValidity()
    }
    if(this.productDelete.valid) {
      console.log('this.productDelete.value', this.productDelete.value)
    }
  }

  clearFilter() {
    this.productDelete.reset()
    this.productDelete.patchValue({
      // customer_type: ['1'],
      // customer_tax_exempt: ['no']
    })
  }



}


