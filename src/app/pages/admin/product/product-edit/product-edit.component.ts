import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.scss']
})
export class  ProductEditComponent implements OnInit {

  productEdit: FormGroup

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder    
  ) { }

  ngOnInit() {
    this.languageTranslate()

    this.formVariablesInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  formVariablesInit() {
    this.productEdit = this.fb.group({
      product_name: [null, [Validators.required]],
      code: [null, [Validators.required]]
    })      
  }

  saveCustomer() {
    for (const i in this.productEdit.controls) {
      this.productEdit.controls[i].markAsDirty()
      this.productEdit.controls[i].updateValueAndValidity()
    }
    if(this.productEdit.valid) {
      console.log('this.productEdit.value', this.productEdit.value)
    }
  }

  clearFilter() {
    this.productEdit.reset()
    this.productEdit.patchValue({
      // customer_type: ['1'],
      // customer_tax_exempt: ['no']
    })
  }



}

