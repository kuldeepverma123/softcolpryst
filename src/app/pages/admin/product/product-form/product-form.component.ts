import { Component, OnInit, Input } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.scss']
})
export class ProductFormComponent  implements OnInit {
  @Input() productData: any
  productForm: FormGroup

  currentDate:any = new Date()  

  // product table data 
  productTableDatas = []  

  // search account dialog 
  
  accountDatas = []

  
  productDatas = []  

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.productInit()

    this.productTableData()  

  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  productInit() {
    this.productForm = this.fb.group({
     product_code: [null, [Validators.required]],
     product_name: [null, [Validators.required]],
     productTableDatas: []
    })      
  }
  
  saveCheque() {
    for (const i in this.productForm.controls) {
      this.productForm.controls[i].markAsDirty()
      this.productForm.controls[i].updateValueAndValidity()
    }
    if(this.productForm.valid) {
      console.log('this.productForm.value', this.productForm.value)
    }
  }

  productTableData() {
    this.productTableDatas = []    
  }

  chequeTableDataRemove(item_index) {
    this.productTableDatas.splice(item_index, 1)
    this.productForm = this.fb.group({
      commentary: [null],
      debit: [null],
      credit: [null]
    })     
  }  


  accountAddInput($event) {
    this.productTableDatas.push(
      $event
    )

    this.productForm.patchValue({
      productTableDatas: $event
    })     

  

}
clearFilter() {
  this.productForm.reset()
  this.productForm.patchValue({
    customersconsult_type: ['1'],
    //appliedapplication_tax_exempt: ['no']
  })
}
}



