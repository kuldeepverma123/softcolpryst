import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductComponent } from './product/product.component';
import { AddNewProductComponent } from './add-new-product/add-new-product.component';
import { AddNewServiceComponent } from './add-new-service/add-new-service.component';
import { ProductEditComponent } from './product-edit/product-edit.component';
import { ProductDeleteComponent } from './product-delete/product-delete.component';
import { ServiceEditComponent } from './service-edit/service-edit.component';
import { ServiceDeleteComponent } from './service-delete/service-delete.component';


const routes: Routes = [
  {
    path: '',
    component: ProductComponent
  },
  {
    path: 'product-edit',
    component: ProductEditComponent
  },
  {
    path: 'product-delete',
    component: ProductDeleteComponent
  },
  {
    path: 'service-edit',
    component:ServiceEditComponent
  },
  {
    path: 'service-delete',
    component:ServiceDeleteComponent
  },
  {
    path: 'new-product',
    component: AddNewProductComponent
  },
  {
    path: 'new-service',
    component: AddNewServiceComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductRoutingModule { }
