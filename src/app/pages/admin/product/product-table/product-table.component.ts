import { Component, OnInit, Input } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
   
 @Component({
  selector: 'app-product-table',
  templateUrl: './product-table.component.html',
  styleUrls: ['./product-table.component.scss']
   })
   export class  ProductTableComponent implements OnInit {
   
     @Input() productData: any
   
     //  Product form
    
     ProductVisible = false
   
     // search
     search_title = ''
   
     productDisplayData: any
   
     sortName: string | null = null
     sortValue: string | null = null
   
     deleteConfirmVisible = false
   
     constructor(
       private fb: FormBuilder
     ) { 
   
     }
   
     ngOnInit() {
       this.ProductData()
     }
   
     //  customersconsult account form 
   
     ProductData() {
       this.productDisplayData = [...this.productData]
     }
  
   
   }
   
   
   
 
 
