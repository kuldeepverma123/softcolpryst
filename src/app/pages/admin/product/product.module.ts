import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgZorroAntdModule } from 'ng-zorro-antd';

// import ngx-translate and the http loader
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import { HttpClient, HttpClientModule } from '@angular/common/http';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ProductRoutingModule } from './product-routing.module';
import { ProductComponent } from './product/product.component';
import { ProductFormComponent } from './product-form/product-form.component';
import { ProductTableComponent } from './product-table/product-table.component';
import { AddNewServiceComponent } from './add-new-service/add-new-service.component';
import { AddNewProductComponent } from './add-new-product/add-new-product.component';
import { ProductEditComponent } from './product-edit/product-edit.component';
import { ProductDeleteComponent } from './product-delete/product-delete.component';
import { ServiceEditComponent } from './service-edit/service-edit.component';
import { ServiceDeleteComponent } from './service-delete/service-delete.component';


@NgModule({
  declarations: [ProductComponent, ProductFormComponent, ProductTableComponent, AddNewServiceComponent, AddNewProductComponent, ProductEditComponent, ProductDeleteComponent, ServiceEditComponent, ServiceDeleteComponent],
  imports: [
    HttpClientModule,
    NgZorroAntdModule,
    CommonModule,
    ProductRoutingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class ProductModule { }
// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
