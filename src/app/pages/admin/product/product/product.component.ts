import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ApiService } from '../../../../api.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class  ProductComponent implements OnInit {

  productDatas: any = []
  productForm: FormGroup
  activeCollapse = false  

  constructor(
    private translate: TranslateService,
    private service: ApiService,
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()
    this.productInit()
    this.productData()    
  }

  productInit() {
    this.productForm = this.fb.group({
     product_code: [null],
     product_name: [null]
    })      
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  showFilter() {
    this.activeCollapse = !this.activeCollapse
  } 
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
  }
  

  saveCheque() {
      this.productDatas = []
      this.service.productFilter(this.productForm.value).subscribe((response) => {
        console.log("response", response['data'])
        this.productDatas = response['data']
        console.log("after", this.productDatas)

      })
  }

  clearFilter() {
    this.productForm.reset()
    this.productData()
  }
  
  productData() {
    this.productDatas = []
    this.service.getSerivceProudct().subscribe((response) => {
      console.log("response", response['data'])
      this.productDatas = response['data']
      console.log("after", this.productDatas)
      // this.applicationaccountDatas = response['data'];
      // console.log("=== dss ",this.applicationaccountDatas);
    })

  }
}





