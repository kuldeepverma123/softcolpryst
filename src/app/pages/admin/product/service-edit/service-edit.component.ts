import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-service-edit',
  templateUrl: './service-edit.component.html',
   styleUrls: ['./service-edit.component.scss']
})
export class  ServiceEditComponent implements OnInit {

  serviceEdit: FormGroup

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder    
  ) { }

  ngOnInit() {
    this.languageTranslate()

    this.formVariablesInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  formVariablesInit() {
    this.serviceEdit = this.fb.group({
      service_name: [null, [Validators.required]],
      code: [null, [Validators.required]]
    })      
  }

  saveCustomer() {
    for (const i in this.serviceEdit.controls) {
      this.serviceEdit.controls[i].markAsDirty()
      this.serviceEdit.controls[i].updateValueAndValidity()
    }
    if(this.serviceEdit.valid) {
      console.log('this.serviceEdit.value', this.serviceEdit.value)
    }
  }

  clearFilter() {
    this.serviceEdit.reset()
    this.serviceEdit.patchValue({
      // customer_type: ['1'],
      // customer_tax_exempt: ['no']
    })
  }



}


