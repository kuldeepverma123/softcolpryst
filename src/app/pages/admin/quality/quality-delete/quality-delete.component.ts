import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-quality-delete',
  templateUrl: './quality-delete.component.html',
  styleUrls: ['./quality-delete.component.scss']
})
export class QualityDeleteComponent implements OnInit {

  qualitydelete: FormGroup

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder    
  ) { }

  ngOnInit() {
    this.languageTranslate()

    this.formVariablesInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  formVariablesInit() {
    this.qualitydelete = this.fb.group({
      // department: ['1'],
      // no_card: [null, [Validators.required]],
      // name: [null, [Validators.required]],
      // last_name: [null, [Validators.required]],
      // phone: [null, [Validators.required]],
      // mobile: [null, [Validators.required]],
      // address: [null, [Validators.required]],
      // position: [null, [Validators.required]],
      // area: [null, [Validators.required]],
      // profession: [null, [Validators.required]],
      // mail: [null, [Validators.required]],
      // city: [null, [Validators.required]],
      // administrator: [null],
      // observer: [null]
    })      
  }

  saveCustomer() {
    for (const i in this.qualitydelete.controls) {
      this.qualitydelete.controls[i].markAsDirty()
      this.qualitydelete.controls[i].updateValueAndValidity()
    }
    if(this.qualitydelete.valid) {
      //console.log('this.qualitydelete.value', this.qualitydelete.value)
    }
  }

  clearFilter() {
    this.qualitydelete.reset()
    this.qualitydelete.patchValue({
      //department: ['1'],
      //customer_tax_exempt: ['no']
    })
  }



}








