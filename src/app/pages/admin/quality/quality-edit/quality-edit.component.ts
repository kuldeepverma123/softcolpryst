import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
selector: 'app-quality-edit',
  templateUrl: './quality-edit.component.html',
  styleUrls: ['./quality-edit.component.scss']
})
export class QualityEditComponent implements OnInit {

  qualityEdit: FormGroup

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder    
  ) { }

  ngOnInit() {
    this.languageTranslate()

    this.formVariablesInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  formVariablesInit() {
    this.qualityEdit = this.fb.group({
      customer_type: ['1'],
      customer_tax_exempt: ['no'],
      business: [null, [Validators.required]],
      user: [null, [Validators.required]],
      type_of_service: [null, [Validators.required]],
      name: [null, [Validators.required]],
      assigned_to: [null, [Validators.required]],
      state: [null, [Validators.required]],
      id_card: [null, [Validators.required]]
    })      
  }

  saveCustomer() {
    for (const i in this.qualityEdit.controls) {
      this.qualityEdit.controls[i].markAsDirty()
      this.qualityEdit.controls[i].updateValueAndValidity()
    }
    if(this.qualityEdit.valid) {
      console.log('this.qualityEdit.value', this.qualityEdit.value)
    }
  }

  clearFilter() {
    this.qualityEdit.reset()
    this.qualityEdit.patchValue({
      customer_type: ['1'],
      customer_tax_exempt: ['no']
    })
  }



}


