import { Component, OnInit, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-quality-form',
  templateUrl: './quality-form.component.html',
  styleUrls: ['./quality-form.component.scss']
})
export class QualityFormComponent implements OnInit {
  @Input() qualityaccountData: any
  qualityForm: FormGroup

  currentDate:any = new Date()  

  // cheque table data 
  chequeTableDatas = []  

  // search account dialog
  
  accountDatas = []

  // applicationaccount search dialog
  searchApplicationaccountVisible = false
  qualityaccountDatas = []  

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.qualityFormInit()

    this.chequeTableData()  

  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  qualityFormInit() {
    this.qualityForm = this.fb.group({
     application_type: ['1'],
     application_tax_exempt: ['no'],
      state: [null, [Validators.required]],
      assigned_to: [null, [Validators.required]],
      business: [null, [Validators.required]],
      name: [null, [Validators.required]],
      type_of_service: [null, [Validators.required]],
      id_card: [null, [Validators.required]],
      chequeTableDatas: []
    })      
  }
  
  saveCheque() {
    for (const i in this.qualityForm.controls) {
      this.qualityForm.controls[i].markAsDirty()
      this.qualityForm.controls[i].updateValueAndValidity()
    }
    if(this.qualityForm.valid) {
      console.log('this.qualityForm.value', this.qualityForm.value)
    }
  }

  chequeTableData() {
    this.chequeTableDatas = []    
  }

  chequeTableDataRemove(item_index) {
    this.chequeTableDatas.splice(item_index, 1)
    this.qualityForm = this.fb.group({
      commentary: [null],
      debit: [null],
      credit: [null]
    })     
  }  


  accountAddInput($event) {
    this.chequeTableDatas.push(
      $event
    )

    this.qualityForm.patchValue({
      chequeTableDatas: $event
    })     

  

}
clearFilter() {
  this.qualityForm.reset()
  this.qualityForm.patchValue({
    application_type: ['1'],
    application_tax_exempt: ['no']
  })
}
}
