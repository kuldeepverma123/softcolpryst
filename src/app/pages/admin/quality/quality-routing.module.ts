import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QualityComponent } from './quality/quality.component';
import { QualityEditComponent } from './quality-edit/quality-edit.component';
import { QualityStatusComponent } from './quality-status/quality-status.component';
import { QualityDeleteComponent } from './quality-delete/quality-delete.component';


const routes: Routes = [
  {
    path: '',
    component: QualityComponent
  },
  {
    path: 'edit',
    component: QualityEditComponent
  },
  {
    path: 'status',
    component: QualityStatusComponent
  },
  {
    path: 'delete',
    component: QualityDeleteComponent
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QualityRoutingModule { }
