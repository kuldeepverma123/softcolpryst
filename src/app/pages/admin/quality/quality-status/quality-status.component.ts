import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-quality-status',
  templateUrl: './quality-status.component.html',
  styleUrls: ['./quality-status.component.scss']
})
export class QualityStatusComponent implements OnInit {
  checked = true;
  qualityStatus: FormGroup

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder    
  ) { }

  ngOnInit() {
    this.languageTranslate()

    this.formVariablesInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  formVariablesInit() {
    this.qualityStatus = this.fb.group({
      customer_type: ['1'],
      customer_tax_exempt: ['no'],
      background: [null, [Validators.required]],
      concept: [null, [Validators.required]]
    })      
  }

  saveApplication() {
    for (const i in this.qualityStatus.controls) {
      this.qualityStatus.controls[i].markAsDirty()
      this.qualityStatus.controls[i].updateValueAndValidity()
    }
    if(this.qualityStatus.valid) {
      console.log('this.qualityStatus.value', this.qualityStatus.value)
    }
  }

  clearFilter() {
    this.qualityStatus.reset()
    this.qualityStatus.patchValue({
      customer_type: ['1'],
      customer_tax_exempt: ['no']
    })
  }
  log(value: string[]): void {
    console.log(value);
  }


}


