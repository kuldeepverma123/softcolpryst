import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QualityTableComponent } from './quality-table.component';

describe('QualityTableComponent', () => {
  let component: QualityTableComponent;
  let fixture: ComponentFixture<QualityTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QualityTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QualityTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
