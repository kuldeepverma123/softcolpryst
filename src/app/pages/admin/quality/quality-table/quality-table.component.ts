import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-quality-table',
  templateUrl: './quality-table.component.html',
  styleUrls: ['./quality-table.component.scss']
})
export class QualityTableComponent implements OnInit {

  @Input() qualityaccountData: any

  //  application account form
  ApplicationAccountForm: FormGroup  
  ApplicationAccountVisible = false
  today: number = Date.now();
  
  // search
  search_title = ''

  qualityaccountDisplayData: any

  sortName: string | null = null
  sortValue: string | null = null

  deleteConfirmVisible = false

  constructor(
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.ApplicationAccountFormInit()

    this.qualityAccountData()
  }
  
  //  application account form
  ApplicationAccountFormInit() {
    this.ApplicationAccountForm = this.fb.group({
      business: [null, [Validators.required]],
      user: [null, [Validators.required]],
      name: [null, [Validators.required]],
      type_service: [null, [Validators.required]],
      application_correlative_checks: [null, [Validators.required]]
    })      
  }  

  ApplicationAccountOpen() {
    this.ApplicationAccountVisible = true
  }

  ApplicationAccountClose() {
    this.ApplicationAccountForm.reset()
    this.ApplicationAccountVisible = false
  }      

  ApplicationAccountSave() {    
    for (const i in this.ApplicationAccountForm.controls) {
      this.ApplicationAccountForm.controls[i].markAsDirty()
      this.ApplicationAccountForm.controls[i].updateValueAndValidity()
    }          

    if(this.ApplicationAccountForm.valid) {
      this.ApplicationAccountClose()
    }
  }

  qualityAccountData() {
    this.qualityaccountDisplayData = [...this.qualityaccountData]
  }

  keyUpSearch() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { state: string; assigned_to: string; days_passed: string; id_card: string; business: string; user: string; name: string; type_service: string }) => {
      return (
        item.business.toLowerCase().indexOf(searchLower) !== -1 ||
        item.id_card.toLowerCase().indexOf(searchLower) !== -1 ||
        item.assigned_to.toLowerCase().indexOf(searchLower) !== -1 ||
        item.days_passed.toLowerCase().indexOf(searchLower) !== -1 ||
        item.state.toLowerCase().indexOf(searchLower) !== -1 ||
        item.user.toLowerCase().indexOf(searchLower) !== -1 ||
        item.name.toLowerCase().indexOf(searchLower) !== -1 ||
        item.type_service.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.qualityaccountData.filter((item: { state: string; assigned_to: string; days_passed: string;  id_card: string; business: string; user: string; name: string; type_service: string }) => filterFunc(item))

    this.qualityaccountDisplayData = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    ) 
    
    if(searchLower.length == 0) {
      this.qualityaccountDisplayData = [...this.qualityaccountData]
    }    
  }  

  // delete confirm
  deleteConfirmOpen() {
    this.deleteConfirmVisible = true
  }

  deleteConfirmClose() {
    this.deleteConfirmVisible = false
  }  
  

}



