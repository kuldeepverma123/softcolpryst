import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgZorroAntdModule } from 'ng-zorro-antd';

// import ngx-translate and the http loader
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import { HttpClient, HttpClientModule } from '@angular/common/http';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { QualityRoutingModule } from './quality-routing.module';
import { QualityComponent } from './quality/quality.component';
import { QualityEditComponent } from './quality-edit/quality-edit.component';
import { QualityStatusComponent } from './quality-status/quality-status.component';
import { QualityFormComponent } from './quality-form/quality-form.component';
import { QualityTableComponent } from './quality-table/quality-table.component';
import { QualityDeleteComponent } from './quality-delete/quality-delete.component';


@NgModule({
  declarations: [QualityComponent, QualityEditComponent, QualityStatusComponent, QualityFormComponent, QualityTableComponent, QualityDeleteComponent],
  imports: [
    HttpClientModule,
    NgZorroAntdModule,
    CommonModule,
    QualityRoutingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class QualityModule { }
// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
