import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-quality',
  templateUrl: './quality.component.html',
  styleUrls: ['./quality.component.scss']
})
export class  QualityComponent implements OnInit {

  qualityaccountDatas: any

  activeCollapse = false  

  constructor(
    private translate: TranslateService
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.qualityaccountData()    
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  showFilter() {
    this.activeCollapse = !this.activeCollapse
  } 
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
  }
  
  submitForm() {
    this.qualityaccountDatas = []
  }
  
  qualityaccountData() {
    this.qualityaccountDatas = [
      {
        business: 'Merco group',
        user: 'John Doe',
        application: 'Banco Financiera Comercial',
        type_service: 'Business associates',
        name: 'John',
        id_card: '458689',
        state: 'Finalized',
        days_passed: '4 Days',
        assigned_to: 'Vanessa ariza'
      }                  
    ]
  }
}


