import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-application-delete',
  templateUrl: './application-delete.component.html',
  styleUrls: ['./application-delete.component.scss']
})
export class ApplicationDeleteComponent implements OnInit {

  customerconsultdelete: FormGroup

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder    
  ) { }

  ngOnInit() {
    this.languageTranslate()

    this.formVariablesInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  formVariablesInit() {
    this.customerconsultdelete = this.fb.group({
      // department: ['1'],
      // no_card: [null, [Validators.required]],
      // name: [null, [Validators.required]],
      // last_name: [null, [Validators.required]],
      // phone: [null, [Validators.required]],
      // mobile: [null, [Validators.required]],
      // address: [null, [Validators.required]],
      // position: [null, [Validators.required]],
      // area: [null, [Validators.required]],
      // profession: [null, [Validators.required]],
      // mail: [null, [Validators.required]],
      // city: [null, [Validators.required]],
      // administrator: [null],
      // observer: [null]
    })      
  }

  saveCustomer() {
    for (const i in this.customerconsultdelete.controls) {
      this.customerconsultdelete.controls[i].markAsDirty()
      this.customerconsultdelete.controls[i].updateValueAndValidity()
    }
    if(this.customerconsultdelete.valid) {
      //console.log('this.customerconsultdelete.value', this.customerconsultdelete.value)
    }
  }

  clearFilter() {
    this.customerconsultdelete.reset()
    this.customerconsultdelete.patchValue({
      //department: ['1'],
      //customer_tax_exempt: ['no']
    })
  }



}








