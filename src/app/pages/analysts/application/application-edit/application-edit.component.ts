import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';
import { FileUploader, FileItem, ParsedResponseHeaders  } from 'ng2-file-upload';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-application-edit',
  templateUrl: './application-edit.component.html',
  styleUrls: ['./application-edit.component.scss']
})
export class ApplicationEditComponent implements OnInit {

  applicationEdit: FormGroup 

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder    
  ) { }

  ngOnInit() {
    this.languageTranslate()

    this.formVariablesInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  formVariablesInit() {
    this.applicationEdit = this.fb.group({
      //customer_type: ['1'],
      ///customer_tax_exempt: ['no'],
      business: [null, [Validators.required]],
      user_request: [null, [Validators.required]],
      type_of_service: [null, [Validators.required]],
      id_card: [null, [Validators.required]],
      candidate: [null, [Validators.required]],
      application_date: [null, [Validators.required]],
      assigned_to: [null, [Validators.required]],
      state: [null, [Validators.required]],
      //id_card: [null, [Validators.required]]
    })      
  }

  saveCustomer() {
  
    
    for (const i in this.applicationEdit.controls) {
      this.applicationEdit.controls[i].markAsDirty()
      this.applicationEdit.controls[i].updateValueAndValidity()
    }
    if(this.applicationEdit.valid) {
      console.log('this.applicationEdit.value', this.applicationEdit.value)
    }
  }

  clearFilter() {
    this.applicationEdit.reset()
    this.applicationEdit.patchValue({
      customer_type: ['1'],
      customer_tax_exempt: ['no']
    })
  }



}


