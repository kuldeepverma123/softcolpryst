import { Component, OnInit, Input } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-application-table',
  templateUrl: './application-table.component.html',
  styleUrls: ['./application-table.component.scss']
})
export class ApplicationTableComponent implements OnInit {

  @Input() applicationaccountData: any

  //  application account form
  ApplicationAccountForm: FormGroup  
  ApplicationAccountVisible = false
  today: number = Date.now();
  
  // search
  search_title = ''

  applicationaccountDisplayData: any

  sortName: string | null = null
  sortValue: string | null = null

  deleteConfirmVisible = false

  constructor(
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.applicationAccountData()
  }


  ApplicationAccountOpen() {
    this.ApplicationAccountVisible = true
  }

  ApplicationAccountClose() {
    this.ApplicationAccountForm.reset()
    this.ApplicationAccountVisible = false
  }      

  ApplicationAccountSave() {    
    for (const i in this.ApplicationAccountForm.controls) {
      this.ApplicationAccountForm.controls[i].markAsDirty()
      this.ApplicationAccountForm.controls[i].updateValueAndValidity()
    }          

    if(this.ApplicationAccountForm.valid) {
      this.ApplicationAccountClose()
    }
  }

  applicationAccountData() {
    this.applicationaccountDisplayData = [...this.applicationaccountData]
  }

  keyUpSearch() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { s_no: string; state: string; assigned_to: string; application_date: string; id_card: string; business: string; service: string; candidate: string; user_request: string }) => {
      return (
        item.s_no.toLowerCase().indexOf(searchLower) !== -1 ||
        item.business.toLowerCase().indexOf(searchLower) !== -1 ||
        item.id_card.toLowerCase().indexOf(searchLower) !== -1 ||
        item.assigned_to.toLowerCase().indexOf(searchLower) !== -1 ||
        item.application_date.toLowerCase().indexOf(searchLower) !== -1 ||
        item.state.toLowerCase().indexOf(searchLower) !== -1 ||
        item.service.toLowerCase().indexOf(searchLower) !== -1 ||
        item.candidate.toLowerCase().indexOf(searchLower) !== -1 ||
        item.user_request.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.applicationaccountData.filter((item: { s_no: string; state: string; assigned_to: string; application_date: string;  id_card: string; business: string; service: string; candidate: string; user_request: string }) => filterFunc(item))

    this.applicationaccountDisplayData = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    ) 
    
    if(searchLower.length == 0) {
      this.applicationaccountDisplayData = [...this.applicationaccountData]
    }    
  }  

  // delete confirm
  deleteConfirmOpen() {
    this.deleteConfirmVisible = true
  }

  deleteConfirmClose() {
    this.deleteConfirmVisible = false
  }  
  

}



