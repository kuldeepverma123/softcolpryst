import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../../../../api.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-application',
  templateUrl: './application.component.html',
  styleUrls: ['./application.component.scss']
})
export class  ApplicationComponent implements OnInit {

  applicationaccountDatas: any
  activeCollapse = false  
  currentUser :any 
  applicationForm: FormGroup
  products : any  = []
  services : any = []
  areaSelectValue: any
  typeOfServiceSelectValue: any
  assigned_to :any = []

  constructor(
    private translate: TranslateService,
    private service: ApiService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    let loginRes = JSON.parse(localStorage.getItem('loginRes'))
    this.currentUser = loginRes.id;
    this.languageTranslate()
    this.applicationaccountData()  
    this.applicationFormInit()
    this.getProduct()    
   
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }

  applicationFormInit() {
    this.applicationForm = this.fb.group({
     application_type: ['1'],
     application_tax_exempt: ['no'],
      area: [null],
      name: [null],
      type_of_service: [null],
      id_card: [null]
    })      
  }
  
  saveCheque() {
    for (const i in this.applicationForm.controls) {
      this.applicationForm.controls[i].markAsDirty()
      this.applicationForm.controls[i].updateValueAndValidity()
    }
    if(this.applicationForm.valid) {
      console.log('this.applicationForm.value', this.applicationForm.value)
      this.service.requestFilterAnalyst(this.applicationForm.value,this.currentUser).subscribe((response) => {
        console.log("response", response['data'])
        this.applicationaccountDatas = response['data']
           var i = 0;
        response['data'].forEach(element => {
           console.log(element.assignDate);
          let date1 = new Date();
          let date2 = new Date(element.assignDate);
           var Difference_In_Time = date1.getTime() - date2.getTime();
  
           var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24); 
           this.applicationaccountDatas[i].Difference_In_Days = Math.round(Difference_In_Days);
          });
  
        console.log("after", this.applicationaccountDatas)
      })
    }
  }

  getProduct() {
    this.service.getAllProduct().subscribe((response) => {
      console.log("response", response['data'])
      this.products = response['data']
      console.log("after", this.products)
    })
   }

   getService(data) {
    this.service.getServiceByProduct(data).subscribe((response) => {
     // console.log("response", response['data'])
      this.services = response['data']
      console.log("after  services", this.services)
    })
   }

   typeOfSelectService($event) {
    console.log("$event",$event)
    this.typeOfServiceSelectValue = $event
    //this.createapplicationAddform.get('type_of_service').reset()
    //console.log("-->areaSelectValue",this.areaSelectValue);
  }  

   areaSelect($event) {
    this.areaSelectValue = $event
    this.getService($event)
    this.applicationForm.get('type_of_service').reset()
    //console.log("-->areaSelectValue",this.areaSelectValue);
  }
  

  
  
  clearFilter() {
    this.applicationForm.reset()
    this.applicationForm.patchValue({
      application_type: ['1'],
      application_tax_exempt: ['no']
    })
  }
  
  showFilter() {
    this.activeCollapse = !this.activeCollapse
  } 
  
  receiveSearchData($event) {
    // console.log('receiveSearchData', $event)
  }
  
  submitForm() {
    this.applicationaccountDatas = []
  }
  
  applicationaccountData() {

    this.service.getRequestForAnalyst(this.currentUser).subscribe((response) => {
       console.log("response", response['data'])
       this.applicationaccountDatas = response['data']
       
     })
    
  }
}


