    import { Component, OnInit } from '@angular/core';

    import { TranslateService } from '@ngx-translate/core';
    
    import { FormBuilder, FormGroup, Validators } from '@angular/forms';
    
    @Component({
      selector: 'app-create-application',
      templateUrl: './create-application.component.html',
      styleUrls: ['./create-application.component.scss']
    })
    export class  CreateApplicationComponent implements OnInit {
     
      state = {
        value: 1,
      };
      
      createapplicationAddform: FormGroup
      customersconsultaccountDisplayData:any
      currentService : any
    
      serviceTypeCheck:string
    
      radioValue: any = ''
    
      areaSelectValue: any
      typeOfServiceSelectValue: any
    
      constructor(
        private translate: TranslateService,
        private fb: FormBuilder    
      ) { }
    
      ngOnInit() {
        this.languageTranslate()
    
        this.formVariablesInit()
        console.log('radioValue --', this.radioValue)
      }
    
      languageTranslate() {
        let acLanguage = localStorage.getItem('acLanguage')
    
        if(acLanguage != null && acLanguage != '') {
          this.translate.use(acLanguage)
        } else {
          this.translate.setDefaultLang('es')
        }    
      }    
    
      formVariablesInit() {
        this.createapplicationAddform = this.fb.group({
          area: [null, [Validators.required]],
          cell_phone: [null],
          document_number: [null],
          company: [null],
          business: [null],
          candidate_doc: [null],
          test_type: [null],
          candidate_name: [null],
          neighborhood: [null],
          expense_center: [null],
          address: [null],
          details: [null],
          email: [null],
          phone: [null],
          document: [null],
          last_name: [null],
          user: [null],
          attach_file: [null],
          department: ['1'],
          city: [null],
          radio: [null],
          type_of_service: [null, [Validators.required]],
          id_doc: [null],
          name: [null],
          observations: [null],
          position: [null],
          description: [null],
          //attach_file: [null],
          attach_photo: [null],
          second: [null, [Validators.required]]
          
    
        })      
      }
      listOfData = [
        {
          key: '1',
          item: '1',
          description: 'MERCO GROUP',
          attached: 'No'
        },
        {
          key: '2',
          item: '2',
          description: 'MERCO GROUP',
          attached: 'No'
        },
        {
          key: '3',
          item: '3',
          description: 'MERCO GROUP',
          attached: 'No'
        }
      ];
    
    
    
      saveCreateapplication() {
        
        for (const i in this.createapplicationAddform.controls) {
          this.createapplicationAddform.controls[i].markAsDirty()
          this.createapplicationAddform.controls[i].updateValueAndValidity()
          
        }
        if(this.createapplicationAddform.valid) {
          console.log('this.createapplicationAddform.value', this.createapplicationAddform.value)
          this.serviceTypeCheck = this.createapplicationAddform.value.createapplication_type
         
          
        }
        // this.createapplicationAddform.patchValue({
        //   createapplication_type: []
        //   //createapplication_tax_exempt: ['no']
        // })
        
            this.currentService = this.createapplicationAddform.value.type_of_service
    
            console.log("this.currentService", this.currentService)
      }
    
      clearFilter() {
        this.createapplicationAddform.reset()
        this.createapplicationAddform.patchValue({
          //createapplication_type: ['0'],
          //createapplication_tax_exempt: ['no']
        })
      }
    
      areaSelect($event) {
        this.areaSelectValue = $event
        this.createapplicationAddform.get('type_of_service').reset()
        //console.log("-->areaSelectValue",this.areaSelectValue);
      }

      typeOfSelectService($event) {
        console.log("$event",$event)
        this.typeOfServiceSelectValue = $event
        //this.createapplicationAddform.get('type_of_service').reset()
        //console.log("-->areaSelectValue",this.areaSelectValue);
      }      
    
      typeOfService() { 
        for (const i in this.createapplicationAddform.controls) {
          this.createapplicationAddform.controls[i].markAsDirty()
          this.createapplicationAddform.controls[i].updateValueAndValidity()
          
        }
        if(this.createapplicationAddform.valid) {
          console.log('this.createapplicationAddform.value', this.createapplicationAddform.value)
          this.serviceTypeCheck = this.createapplicationAddform.value.createapplication_type
         
          
        }     
        this.typeOfServiceSelectValue = this.createapplicationAddform.value.type_of_service
      }  
      groupOfService() {        
        this.typeOfServiceSelectValue = this.createapplicationAddform.value.type_of_service
      } 
    
    }
    
    
    






  
  
  
  
  
  
  
  
  



























