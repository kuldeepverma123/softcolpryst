import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeesConsultDeleteComponent } from './employees-consult-delete.component';

describe('EmployeesConsultDeleteComponent', () => {
  let component: EmployeesConsultDeleteComponent;
  let fixture: ComponentFixture<EmployeesConsultDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeesConsultDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeesConsultDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
