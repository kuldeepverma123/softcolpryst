import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-employees-consult-delete',
  templateUrl: './employees-consult-delete.component.html',
  styleUrls: ['./employees-consult-delete.component.scss']
})
export class EmployeesConsultDeleteComponent implements OnInit {

  employeesconsultdelete: FormGroup

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder    
  ) { }

  ngOnInit() {
    this.languageTranslate()

    this.formVariablesInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  formVariablesInit() {
    this.employeesconsultdelete = this.fb.group({
      // department: ['1'],
      // no_card: [null, [Validators.required]],
      // name: [null, [Validators.required]],
      // last_name: [null, [Validators.required]],
      // phone: [null, [Validators.required]],
      // mobile: [null, [Validators.required]],
      // address: [null, [Validators.required]],
      // position: [null, [Validators.required]],
      // area: [null, [Validators.required]],
      // profession: [null, [Validators.required]],
      // mail: [null, [Validators.required]],
      // city: [null, [Validators.required]],
      // administrator: [null],
      // observer: [null]
    })      
  }

  saveCustomer() {
    for (const i in this.employeesconsultdelete.controls) {
      this.employeesconsultdelete.controls[i].markAsDirty()
      this.employeesconsultdelete.controls[i].updateValueAndValidity()
    }
    if(this.employeesconsultdelete.valid) {
      //console.log('this.employeesconsultdelete.value', this.employeesconsultdelete.value)
    }
  }

  clearFilter() {
    this.employeesconsultdelete.reset()
    this.employeesconsultdelete.patchValue({
      //department: ['1'],
      //customer_tax_exempt: ['no']
    })
  }



}







