import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeesConsultEditComponent } from './employees-consult-edit.component';

describe('EmployeesConsultEditComponent', () => {
  let component: EmployeesConsultEditComponent;
  let fixture: ComponentFixture<EmployeesConsultEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeesConsultEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeesConsultEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
