import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-employees-consult-edit',
  templateUrl: './employees-consult-edit.component.html',
  styleUrls: ['./employees-consult-edit.component.scss']
})
export class EmployeesConsultEditComponent implements OnInit {

  employeesconsultEdit: FormGroup

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder    
  ) { }

  ngOnInit() {
    this.languageTranslate()

    this.formVariablesInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  formVariablesInit() {
    this.employeesconsultEdit = this.fb.group({
      department: ['1'],
      city: [null],
      code: [null, [Validators.required]],
      id_card: [null, [Validators.required]],
      email: [null, [Validators.required]],
      name: [null, [Validators.required]],
      //department: [null, [Validators.required]],
      //user_type: [null, [Validators.required]],
      user: [null, [Validators.required]],
      last_name: [null, [Validators.required]],
      phone: [null, [Validators.required]],
      profession: [null, [Validators.required]],
      mail: [null, [Validators.required]],
      attach_photo: [null, [Validators.required]]
    })      
  }

  saveCustomer() {
    for (const i in this.employeesconsultEdit.controls) {
      this.employeesconsultEdit.controls[i].markAsDirty()
      this.employeesconsultEdit.controls[i].updateValueAndValidity()
    }
    if(this.employeesconsultEdit.valid) {
      console.log('this.employeesconsultEdit.value', this.employeesconsultEdit.value)
    }
  }

  clearFilter() {
    this.employeesconsultEdit.reset()
    this.employeesconsultEdit.patchValue({
      department: ['1'],
      //customer_tax_exempt: ['no']
    })
  }



}






