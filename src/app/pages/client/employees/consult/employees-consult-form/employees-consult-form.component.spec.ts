import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeesConsultFormComponent } from './employees-consult-form.component';

describe('EmployeesConsultFormComponent', () => {
  let component: EmployeesConsultFormComponent;
  let fixture: ComponentFixture<EmployeesConsultFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeesConsultFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeesConsultFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
