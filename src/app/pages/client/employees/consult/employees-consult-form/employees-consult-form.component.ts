
import { Component, OnInit, Input } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-employees-consult-form',
  templateUrl: './employees-consult-form.component.html',
  styleUrls: ['./employees-consult-form.component.scss']
})
export class EmployeesConsultFormComponent  implements OnInit {
  @Input() employeesconsultData: any
  employeesconsultForm: FormGroup

  currentDate:any = new Date()  

  // employeesconsultForm table data 
  employeesconsultformTableDatas = []  

  // search account dialog
   
  accountDatas = []

  // employeesconsult search dialog
  employeesconsultVisible = false
  employeesconsultDatas = []  

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.employeesconsultFormInit()

    this.employeesconsultformTableData()  

  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  employeesconsultFormInit() {
    this.employeesconsultForm = this.fb.group({
     //application_type: ['1'],
     //application_tax_exempt: ['no'],
     id_card: [null, [Validators.required]],
     //user_type: [null, [Validators.required]],
     name: [null, [Validators.required]],
     last_name: [null, [Validators.required]],
     user: [null, [Validators.required]],
      chequeTableDatas: []
    })      
  }
  
  saveCheque() {
    for (const i in this.employeesconsultForm.controls) {
      this.employeesconsultForm.controls[i].markAsDirty()
      this.employeesconsultForm.controls[i].updateValueAndValidity()
    }
    if(this.employeesconsultForm.valid) {
      console.log('this.employeesconsultForm.value', this.employeesconsultForm.value)
    }
  }

  employeesconsultformTableData() {
    this.employeesconsultformTableDatas = []    
  }

  personalconsultformTableDataRemove(item_index) {
    this.employeesconsultformTableDatas.splice(item_index, 1)
    this.employeesconsultForm = this.fb.group({
      commentary: [null],
      debit: [null],
      credit: [null]
    })     
  }  


  accountAddInput($event) {
    this.employeesconsultformTableDatas.push(
      $event
    )

    this.employeesconsultForm.patchValue({
      employeesconsultformTableDatas: $event
    })     

  

}
clearFilter() {
  this.employeesconsultForm.reset()
  this.employeesconsultForm.patchValue({
    customersconsult_type: ['1'],
    //appliedapplication_tax_exempt: ['no']
  })
}
}



