import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeesConsultTableComponent } from './employees-consult-table.component';

describe('EmployeesConsultTableComponent', () => {
  let component: EmployeesConsultTableComponent;
  let fixture: ComponentFixture<EmployeesConsultTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeesConsultTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeesConsultTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
