import { Component, OnInit, Input } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-employees-consult-table',
  templateUrl: './employees-consult-table.component.html',
  styleUrls: ['./employees-consult-table.component.scss']

  })
  export class  EmployeesConsultTableComponent implements OnInit {
  
    @Input() employeesconsultData: any
  
    // employeesconsult account form
   
  
    // search
    search_title = ''
  
    employeesconsultDisplayData: any
  
    constructor(
      private fb: FormBuilder
    ) { 
  
    }
  
    ngOnInit() {
     
      this.employeesConsultData()
    }
  
  
    // PersonalConsultOpen() {
    //   this.EmployeesConsultVisible = true
    // }
  
  
 
  
    employeesConsultData() {
      this.employeesconsultDisplayData = [...this.employeesconsultData]
    }
  
    
  
  }
  

    
  
  

