import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeesConsultViewComponent } from './employees-consult-view.component';

describe('EmployeesConsultViewComponent', () => {
  let component: EmployeesConsultViewComponent;
  let fixture: ComponentFixture<EmployeesConsultViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeesConsultViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeesConsultViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
