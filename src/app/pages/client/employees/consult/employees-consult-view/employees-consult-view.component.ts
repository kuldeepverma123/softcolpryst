import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-employees-consult-view',
  templateUrl: './employees-consult-view.component.html',
  styleUrls: ['./employees-consult-view.component.scss']
})
export class EmployeesConsultViewComponent implements OnInit {

  employeesconsultview: FormGroup

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder    
  ) { }

  ngOnInit() {
    this.languageTranslate()

    this.formVariablesInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  formVariablesInit() {
    this.employeesconsultview = this.fb.group({
      // department: ['1'],
      // no_card: [null, [Validators.required]],
      // name: [null, [Validators.required]],
      // last_name: [null, [Validators.required]],
      // phone: [null, [Validators.required]],
      // mobile: [null, [Validators.required]],
      // address: [null, [Validators.required]],
      // position: [null, [Validators.required]],
      // area: [null, [Validators.required]],
      // profession: [null, [Validators.required]],
      // mail: [null, [Validators.required]],
      // city: [null, [Validators.required]],
      // administrator: [null],
      // observer: [null]
    })      
  }

  saveCustomer() {
    for (const i in this.employeesconsultview.controls) {
      this.employeesconsultview.controls[i].markAsDirty()
      this.employeesconsultview.controls[i].updateValueAndValidity()
    }
    if(this.employeesconsultview.valid) {
      //console.log('this.employeesconsultview.value', this.employeesconsultview.value)
    }
  }

  clearFilter() {
    this.employeesconsultview.reset()
    this.employeesconsultview.patchValue({
      //department: ['1'],
      //customer_tax_exempt: ['no']
    })
  }



}






