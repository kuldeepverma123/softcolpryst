import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeesConsultComponent } from './employees-consult.component';

describe('EmployeesConsultComponent', () => {
  let component: EmployeesConsultComponent;
  let fixture: ComponentFixture<EmployeesConsultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeesConsultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeesConsultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
