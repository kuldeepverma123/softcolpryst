import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../../../api.service';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
@Component({
  selector: 'app-employees-consult',
  templateUrl: './employees-consult.component.html',
  styleUrls: ['./employees-consult.component.scss']
})
export class  EmployeesConsultComponent implements OnInit {

  employeesconsultDatas: any = []
  activeCollapse = false 
  currentUser:any
  employeesconsultForm: FormGroup

  constructor(
    private translate: TranslateService,
    private service: ApiService,
    private fb: FormBuilder

  ) {  }

  ngOnInit() {
    this.languageTranslate()
    let loginRes = JSON.parse(localStorage.getItem('loginRes'))
    this.currentUser = loginRes.client_id;
    this.employeesconsultData()  
    this.employeesconsultFormInit()
  
  }
   
  employeesconsultFormInit() {
    this.employeesconsultForm = this.fb.group({
      id_card: [null],
     name: [null],
     last_name: [null]
    })      
  }

  saveCheque() {
    this.employeesconsultDatas =[];
    this.service.employeeFilter( this.employeesconsultForm.value, this.currentUser).subscribe((response) => {
     // console.log("response", response['data'])
      this.employeesconsultDatas = response['data']
      console.log("after  customer list", this.employeesconsultDatas)
    })
    
  }

  clearFilter() {
    this.employeesconsultForm.reset()
    this.employeesconsultData()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }

 
  
  employeesconsultData() {

    this.employeesconsultDatas =[];
    console.log("this.clientID", this.currentUser);
    this.service.getEmployeeByClientId(this.currentUser).subscribe((response) => {
     // console.log("response", response['data'])
      this.employeesconsultDatas = response['data']
      console.log("after  customer list", this.employeesconsultDatas)
    })
   }
}





