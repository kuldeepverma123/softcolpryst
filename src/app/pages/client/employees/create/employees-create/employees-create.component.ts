import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../../../api.service';
import { TranslateService } from '@ngx-translate/core';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-employees-create',
   templateUrl: './employees-create.component.html',
   styleUrls: ['./employees-create.component.scss']
})
export class EmployeesCreateComponent implements OnInit {
  activate_account_link:any
  employeescreateform: FormGroup
  isSpinning : boolean
  currentUser :any

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private service: ApiService,  
    private notification: NzNotificationService,       
  ) { }

  ngOnInit() {

    let loginRes = JSON.parse(localStorage.getItem('loginRes'))
    this.currentUser = loginRes.client_id;
    this.languageTranslate()
    
    this.formVariablesInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  formVariablesInit() {
    this.employeescreateform = this.fb.group({
      department: ['1'],
      no_card: [null, [Validators.required]],
      name: [null, [Validators.required]],
      last_name: [null, [Validators.required]],
      phone: [null, [Validators.required]],
      mobile: [null, [Validators.required]],
      address: [null, [Validators.required]],
      position: [null, [Validators.required]],
      area: [null, [Validators.required]],
      profession: [null, [Validators.required]],
      mail: [null, [Validators.required]],
      city: [null, [Validators.required]],
      administrator: [null],
      observer: [null],
      customer_id : [this.currentUser]
    })      
  }

  saveCustomer() {
    this.service.addEmployee(this.employeescreateform.value).subscribe(
      (response) =>{
        console.log(response)
        this.successNotification('success')
        this.employeescreateform.reset();
        this.isSpinning = false;
        
      }, 
      (error) => {
        this.errorNotification('error')
        this.isSpinning = false;
      }
    )
  }

  successNotification(type: string): void {
    let acLanguage = localStorage.getItem('acLanguage')
   let tit, message;
  console.log(acLanguage)
  if(acLanguage == "es"){

  tit = 'Solicitud de creación exitosa'

  }else {
    tit = 'Request create successful'
  }

this.notification.create(
  type,
  tit,
  message
   );
  }

  errorNotification(type: string): void {
    let acLanguage = localStorage.getItem('acLanguage')
    let tit, message;
      console.log(acLanguage)
      if(acLanguage == "es"){

      tit = 'Solicitud crear falla'
      message = 'algo está mal'

      }else {
        tit = 'Request create fail'
        message = 'something is wrong'
      }

    this.notification.create(
      type,
      tit,
      message
    );
  }

  clearFilter() {
    this.employeescreateform.reset()
    this.employeescreateform.patchValue({
      department: ['1'],
      //customer_tax_exempt: ['no']
    })
  }



}







