import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeesCreateComponent } from './create/employees-create/employees-create.component';
import { EmployeesConsultComponent } from './consult/employees-consult/employees-consult.component';
import { EmployeesConsultEditComponent } from './consult/employees-consult-edit/employees-consult-edit.component';
import { EmployeesConsultViewComponent } from './consult/employees-consult-view/employees-consult-view.component';
import { EmployeesConsultDeleteComponent } from './consult/employees-consult-delete/employees-consult-delete.component';


const routes: Routes = [
  {
    path: 'create',
    component:EmployeesCreateComponent
  },
  {
    path: 'consult',
    component:EmployeesConsultComponent
  },
  {
    path: 'consult/edit',
    component:EmployeesConsultEditComponent
  },
  {
    path: 'consult/view',
    component:EmployeesConsultViewComponent
  },
  {
    path: 'consult/delete',
    component:EmployeesConsultDeleteComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeesRoutingModule { }
