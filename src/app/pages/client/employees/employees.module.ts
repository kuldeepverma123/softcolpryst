import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgZorroAntdModule } from 'ng-zorro-antd';

// import ngx-translate and the http loader
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import { HttpClient, HttpClientModule } from '@angular/common/http';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { EmployeesRoutingModule } from './employees-routing.module';
import { EmployeesConsultComponent } from './consult/employees-consult/employees-consult.component';
import { EmployeesCreateComponent } from './create/employees-create/employees-create.component';
import { EmployeesConsultFormComponent } from './consult/employees-consult-form/employees-consult-form.component';
import { EmployeesConsultTableComponent } from './consult/employees-consult-table/employees-consult-table.component';
import { EmployeesConsultViewComponent } from './consult/employees-consult-view/employees-consult-view.component';
import { EmployeesConsultEditComponent } from './consult/employees-consult-edit/employees-consult-edit.component';
import { EmployeesConsultDeleteComponent } from './consult/employees-consult-delete/employees-consult-delete.component';


@NgModule({
  declarations: [EmployeesConsultComponent, EmployeesCreateComponent, EmployeesConsultFormComponent, EmployeesConsultTableComponent, EmployeesConsultViewComponent, EmployeesConsultEditComponent, EmployeesConsultDeleteComponent],
  imports: [
    CommonModule,
    NgZorroAntdModule,
    HttpClientModule,
    EmployeesRoutingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class EmployeesModule { }
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
