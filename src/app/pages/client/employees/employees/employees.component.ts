import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss']
})
export class EmployeesComponent implements OnInit {
  activate_account_link:any
  employeesForm: FormGroup

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder    
  ) { }

  ngOnInit() {
    this.languageTranslate()

    this.formVariablesInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  formVariablesInit() {
    this.employeesForm = this.fb.group({
      department: ['1'],
      customer_tax_exempt: ['no'],
      user_type: [null, [Validators.required]],
      mail: [null, [Validators.required]],
      id_card: [null, [Validators.required]],
      name: [null, [Validators.required]],
      last_name: [null, [Validators.required]],
      city: [null],
      phone: [null, [Validators.required]],
      mobile: [null, [Validators.required]],
      position: [null, [Validators.required]],
      area: [null, [Validators.required]],
      profession: [null, [Validators.required]],
      attach_photo: [null]
    })      
  }

  saveCustomer() {
    for (const i in this.employeesForm.controls) {
      this.employeesForm.controls[i].markAsDirty()
      this.employeesForm.controls[i].updateValueAndValidity()
    }
    if(this.employeesForm.valid) {
      console.log('this.employeesForm.value', this.employeesForm.value)
    }
  }

  clearFilter() {
    this.employeesForm.reset()
    this.employeesForm.patchValue({
      department: ['1'],
      //customer_tax_exempt: ['no']
    })
  }



}







