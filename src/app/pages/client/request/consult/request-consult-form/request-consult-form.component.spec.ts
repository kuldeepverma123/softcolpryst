import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestConsultFormComponent } from './request-consult-form.component';

describe('RequestConsultFormComponent', () => {
  let component: RequestConsultFormComponent;
  let fixture: ComponentFixture<RequestConsultFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestConsultFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestConsultFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
