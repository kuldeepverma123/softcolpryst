import { Component, OnInit, Input } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-request-consult-form',
  templateUrl: './request-consult-form.component.html',
  styleUrls: ['./request-consult-form.component.scss']
})
export class RequestConsultFormComponent implements OnInit {
  @Input() requestconsultData: any
  requestconsultForm: FormGroup

  currentDate:any = new Date()  

  // cheque table data 
  chequeTableDatas = []  

  // search account dialog
  
  accountDatas = []

  // applicationaccount search dialog
  searchApplicationaccountVisible = false
  requestconsultDatas = []  

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.languageTranslate()

    this.applicationFormInit()

    this.chequeTableData()  

  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }
  
  applicationFormInit() {
    this.requestconsultForm = this.fb.group({
      candidate_id: [null],
      candidate_name: [null],
      type_of_service: [null],
      type_of_product: [null],
      name: [null],
      user: [null],
      chequeTableDatas: []
    })      
  }
  
  saveCheque() {
    for (const i in this.requestconsultForm.controls) {
      this.requestconsultForm.controls[i].markAsDirty()
      this.requestconsultForm.controls[i].updateValueAndValidity()
    }
    if(this.requestconsultForm.valid) {
      console.log('this.requestconsultForm.value', this.requestconsultForm.value)
    }
  }

  chequeTableData() {
    this.chequeTableDatas = []    
  }

  chequeTableDataRemove(item_index) {
    this.chequeTableDatas.splice(item_index, 1)
    this.requestconsultForm = this.fb.group({
      commentary: [null],
      debit: [null],
      credit: [null]
    })     
  }  


  accountAddInput($event) {
    this.chequeTableDatas.push(
      $event
    )

    this.requestconsultForm.patchValue({
      chequeTableDatas: $event
    })     

  

}
clearFilter() {
  this.requestconsultForm.reset()
  this.requestconsultForm.patchValue({
    application_type: ['1'],
    application_tax_exempt: ['no']
  })
}
}

