import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestConsultStatusComponent } from './request-consult-status.component';

describe('RequestConsultStatusComponent', () => {
  let component: RequestConsultStatusComponent;
  let fixture: ComponentFixture<RequestConsultStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestConsultStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestConsultStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
