import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-request-consult-status',
  templateUrl: './request-consult-status.component.html',
  styleUrls: ['./request-consult-status.component.scss']
})
export class RequestConsultStatusComponent implements OnInit {
  checked = true;
  requestconsultStatus: FormGroup

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder    
  ) { }

  ngOnInit() {
    this.languageTranslate()

    this.formVariablesInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  formVariablesInit() {
    this.requestconsultStatus = this.fb.group({
      customer_type: ['1'],
      customer_tax_exempt: ['no'],
      background: [null, [Validators.required]],
      concept: [null, [Validators.required]]
    })      
  }

  saveApplied() {
    for (const i in this.requestconsultStatus.controls) {
      this.requestconsultStatus.controls[i].markAsDirty()
      this.requestconsultStatus.controls[i].updateValueAndValidity()
    }
    if(this.requestconsultStatus.valid) {
      console.log('this.requestconsultStatus.value', this.requestconsultStatus.value)
    }
  }

  clearFilter() {
    this.requestconsultStatus.reset()
    this.requestconsultStatus.patchValue({
      customer_type: ['1'],
      customer_tax_exempt: ['no']
    })
  }
  log(value: string[]): void {
    console.log(value);
  }


}


