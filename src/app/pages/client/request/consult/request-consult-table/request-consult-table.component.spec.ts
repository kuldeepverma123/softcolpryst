import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestConsultTableComponent } from './request-consult-table.component';

describe('RequestConsultTableComponent', () => {
  let component: RequestConsultTableComponent;
  let fixture: ComponentFixture<RequestConsultTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestConsultTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestConsultTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
