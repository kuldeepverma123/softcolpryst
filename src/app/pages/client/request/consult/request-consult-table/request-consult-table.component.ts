import { Component, OnInit, Input } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-request-consult-table',
  templateUrl: './request-consult-table.component.html',
  styleUrls: ['./request-consult-table.component.scss']
})
export class RequestConsultTableComponent implements OnInit {

  @Input() requestconsultData: any

  //  RequestConsult form
  RequestConsultForm: FormGroup  
  RequestConsultVisible = false
  today: number = Date.now();
  
  // search
  search_title = ''

  requestconsultDisplayData: any

  sortName: string | null = null
  sortValue: string | null = null

  deleteConfirmVisible = false

  constructor(
    private fb: FormBuilder
  ) { 

  }

  ngOnInit() {
   
    this.requestConsultData()
  }


  ApplicationAccountOpen() {
    this.RequestConsultVisible = true
  }

  RequestConsultClose() {
    this.RequestConsultForm.reset()
    this.RequestConsultVisible = false
  }      

  ApplicationAccountSave() {    
    for (const i in this.RequestConsultForm.controls) {
      this.RequestConsultForm.controls[i].markAsDirty()
      this.RequestConsultForm.controls[i].updateValueAndValidity()
    }          

    if(this.RequestConsultForm.valid) {
      this.RequestConsultClose()
    }
  }

  requestConsultData() {
    this.requestconsultDisplayData = [...this.requestconsultData]
  }

  keyUpSearch() {
    let searchLower = this.search_title.toLowerCase()

    const filterFunc = (item: { state: string; application_date: string; candidate: string; cand_id: string; s_no: string; requested: string; service: string; product: string }) => {
      return (
        item.s_no.toLowerCase().indexOf(searchLower) !== -1 ||
        item.cand_id.toLowerCase().indexOf(searchLower) !== -1 ||
        item.application_date.toLowerCase().indexOf(searchLower) !== -1 ||
        item.candidate.toLowerCase().indexOf(searchLower) !== -1 ||
        item.state.toLowerCase().indexOf(searchLower) !== -1 ||
        item.requested.toLowerCase().indexOf(searchLower) !== -1 ||
        item.service.toLowerCase().indexOf(searchLower) !== -1 ||
        item.product.toLowerCase().indexOf(searchLower) !== -1
      )
    }

    const data = this.requestconsultData.filter((item: { state: string; application_date: string; candidate: string;  cand_id: string; s_no: string; requested: string; service: string; product: string }) => filterFunc(item))

    this.requestconsultDisplayData = data.sort((a, b) =>
      this.sortValue === 'ascend'
        ? a[this.sortName!] > b[this.sortName!]
          ? 1
          : -1
        : b[this.sortName!] > a[this.sortName!]
        ? 1
        : -1
    ) 
    
    if(searchLower.length == 0) {
      this.requestconsultDisplayData = [...this.requestconsultData]
    }    
  }  

  // delete confirm
  deleteConfirmOpen() {
    this.deleteConfirmVisible = true
  }

  deleteConfirmClose() {
    this.deleteConfirmVisible = false
  }  
  

}



