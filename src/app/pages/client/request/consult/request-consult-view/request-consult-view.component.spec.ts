import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestConsultViewComponent } from './request-consult-view.component';

describe('RequestConsultViewComponent', () => {
  let component: RequestConsultViewComponent;
  let fixture: ComponentFixture<RequestConsultViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestConsultViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestConsultViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
