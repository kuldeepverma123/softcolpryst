import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-request-consult-view',
  templateUrl: './request-consult-view.component.html',
  styleUrls: ['./request-consult-view.component.scss']
})
export class RequestConsultViewComponent implements OnInit {

  consultview: FormGroup

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder    
  ) { }

  ngOnInit() {
    this.languageTranslate()

    this.formVariablesInit()
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')

    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }    

  formVariablesInit() {
    this.consultview = this.fb.group({
      notes: [null],
    })      
  }

  saveCustomer() {
    for (const i in this.consultview.controls) {
      this.consultview.controls[i].markAsDirty()
      this.consultview.controls[i].updateValueAndValidity()
    }
    if(this.consultview.valid) {
      //console.log('this.consultview.value', this.consultview.value)
    }
  }

  clearFilter() {
    this.consultview.reset()
    this.consultview.patchValue({
      //department: ['1'],
      //customer_tax_exempt: ['no']
    })
  }



}






