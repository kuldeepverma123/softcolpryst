    import { Component, OnInit } from '@angular/core';

    import { TranslateService } from '@ngx-translate/core';
    import { FormBuilder, FormGroup, Validators } from '@angular/forms';
    import { HttpClient } from '@angular/common/http';
    import { NzNotificationService } from 'ng-zorro-antd/notification';
    import { ApiService } from '../../../../../api.service';
    import * as XLSX from 'xlsx';
    
    @Component({
      selector: 'app-request-create',
          templateUrl: './request-create.component.html',
          styleUrls: ['./request-create.component.scss']
    })
    export class  RequestCreateComponent implements OnInit {
     
      state = {
        value: 1, 
      };

      willDownload = false;
      fileName : ""
      bulkdata : any
      createapplicationAddform: FormGroup
      customersconsultaccountDisplayData:any
      currentService : any
      serviceTypeCheck:string
      radioValue: any = ''
      areaSelectValue: any
      typeOfServiceSelectValue: any
      isSpinning : boolean
      products : any
      services : any
      customerList : any
      employeeList : any
      currentUser : any
      

      constructor(
        private translate: TranslateService,
        private fb: FormBuilder,
        private http: HttpClient,
        private notification: NzNotificationService,
        private service: ApiService    
      ) { }
    
      ngOnInit() {
        this.languageTranslate()
        this.formVariablesInit()
        this.getCustomersList()
        this.getProduct()
       
        let loginRes = JSON.parse(localStorage.getItem('loginRes'))
        console.log(loginRes);
        this.currentUser = loginRes.client_id

        this.listEmployeeByClientId()
      }
    
      languageTranslate() {
        let acLanguage = localStorage.getItem('acLanguage')
    
        if(acLanguage != null && acLanguage != '') {
          this.translate.use(acLanguage)
        } else {
          this.translate.setDefaultLang('es')
        }    
      }    
    
      formVariablesInit() {
        this.createapplicationAddform = this.fb.group({
          user: [null],
          area: [null, [Validators.required]],
          cell_phone: [null],
          document_number: [null],
          company: [null],
          business: [null],
          candidate_doc: [null],
          test_type: [null],
          candidate_name: [null],
          neighborhood: [null],
          expense_center: [null],
          address: [null],
          details: [null],
          email: [null],
          phone: [null],
          cellphone: [null],
          document: [null],
          last_name: [null],
          attach_file: [null],
          department: ['1'],
          city: [null],
          radio: [null],
          type_of_service: [null, [Validators.required]],
          id_doc: [null],
          name: [null],
          observations: [null],
          position: [null],
          description: [null],
          attach_photo: [null],
          second: [null, [Validators.required]],
          attach_document : [null],
          cargo : [null],
          servicetype : [null],
          testtype : [null],
          direction : [null],
          socialresone :[null],
          document_type :[null],
          documentnumber :[null],
          candidate_last_name :[null]
        })      
      }
      listOfData = [
        {
          key: '1',
          item: '1',
          description: 'MERCO GROUP',
          attached: 'No'
        },
        {
          key: '2',
          item: '2',
          description: 'MERCO GROUP',
          attached: 'No'
        },
        {
          key: '3',
          item: '3',
          description: 'MERCO GROUP',
          attached: 'No'
        }
      ];
    
    
    
      saveCreateapplication() {
        
        for (const i in this.createapplicationAddform.controls) {
          this.createapplicationAddform.controls[i].markAsDirty()
          this.createapplicationAddform.controls[i].updateValueAndValidity()
          
        }
        if(this.createapplicationAddform.valid) {
          console.log('this.createapplicationAddform.value', this.createapplicationAddform.value)
          this.serviceTypeCheck = this.createapplicationAddform.value.createapplication_type
         
          
        }
        // this.createapplicationAddform.patchValue({
        //   createapplication_type: []
        //   //createapplication_tax_exempt: ['no']
        // })
        
            this.currentService = this.createapplicationAddform.value.type_of_service
    
            console.log("this.currentService", this.currentService)
      }
    
      clearFilter() {
        this.createapplicationAddform.reset()
        this.createapplicationAddform.patchValue({
          //createapplication_type: ['0'],
          //createapplication_tax_exempt: ['no']
        })
      }
    
      areaSelect($event) {
        this.areaSelectValue = $event
        this.getService($event)
        this.createapplicationAddform.get('type_of_service').reset()
        //console.log("-->areaSelectValue",this.areaSelectValue);
      }

      bussineSelect($event) {
        //this.areaSelectValue = $event
       // this.listEmployeeByClientId()
        this.createapplicationAddform.get('user').reset()
        //console.log("-->areaSelectValue",this.areaSelectValue);
      }

      typeOfSelectService($event) {
        console.log("$event",$event)
        this.typeOfServiceSelectValue = $event
        //this.createapplicationAddform.get('type_of_service').reset()
        //console.log("-->areaSelectValue",this.areaSelectValue);
      }      
    
      typeOfService() { 
        for (const i in this.createapplicationAddform.controls) {
          this.createapplicationAddform.controls[i].markAsDirty()
          this.createapplicationAddform.controls[i].updateValueAndValidity()
          
        }
        if(this.createapplicationAddform.valid) {
          console.log('this.createapplicationAddform.value', this.createapplicationAddform.value)
          this.serviceTypeCheck = this.createapplicationAddform.value.createapplication_type
         
          
        }     
        this.typeOfServiceSelectValue = this.createapplicationAddform.value.type_of_service
      }  
     
      submitForm() {
      //   if (this.createapplicationAddform.invalid) {
      //     return;
      // }
        this.isSpinning = true;
        console.log("---", this.createapplicationAddform.get('company').value);
        var formData: any = new FormData();
        formData.append("business", this.createapplicationAddform.get('company').value);
        formData.append("businessUser", this.createapplicationAddform.get('user').value);
        formData.append("product_id", this.createapplicationAddform.get('area').value);
        formData.append("service_id", this.createapplicationAddform.get('type_of_service').value);
        formData.append("identity_document", this.createapplicationAddform.get('id_doc').value);
        formData.append("typeofDocument", this.createapplicationAddform.get('document_type').value);
        formData.append("ducumentNumber", this.createapplicationAddform.get('documentnumber').value);
        formData.append("firstname", this.createapplicationAddform.get('candidate_name').value);
        formData.append("lastname", this.createapplicationAddform.get('candidate_last_name').value);
        formData.append("email", this.createapplicationAddform.get('email').value);
        formData.append("direction", this.createapplicationAddform.get('direction').value);
        formData.append("phone", this.createapplicationAddform.get('phone').value);
        formData.append("mobile", this.createapplicationAddform.get('cellphone').value);
        formData.append("departmant", this.createapplicationAddform.get('department').value);
        formData.append("neighborhood", this.createapplicationAddform.get('neighborhood').value);
        formData.append("position", this.createapplicationAddform.get('position').value);
        formData.append("expenditureCenter", this.createapplicationAddform.get('expense_center').value);
        formData.append("observations", this.createapplicationAddform.get('observations').value);
        formData.append("description", this.createapplicationAddform.get('description').value);
        formData.append("details", this.createapplicationAddform.get('details').value);
        formData.append("attachphoto", this.createapplicationAddform.get('attach_photo').value);
        formData.append("attachdocuments", this.createapplicationAddform.get('attach_document').value);
        formData.append("socialresion", this.createapplicationAddform.get('socialresone').value);
        formData.append("typeofservice", this.createapplicationAddform.get('servicetype').value);
        formData.append("testType", this.createapplicationAddform.get('testtype').value);
        
        console.log("formData", this.createapplicationAddform.value);
    
        this.service.createTask(this.createapplicationAddform.value).subscribe(
          (response) =>{
            console.log(response)
            this.successNotification('success')
            this.createapplicationAddform.reset();
            this.isSpinning = false;
          }, 
          (error) => {
            this.errorNotification('error');
            this.isSpinning = false;
          }
        )
      }

      successNotification(type: string): void {
        let acLanguage = localStorage.getItem('acLanguage')
       let tit, message;
      console.log(acLanguage)
      if(acLanguage == "es"){

      tit = 'Solicitud de creación exitosa'

      }else {
        tit = 'Request create successful'
      }

    this.notification.create(
      type,
      tit,
      message
       );
      }

      errorNotification(type: string): void {
        let acLanguage = localStorage.getItem('acLanguage')
        let tit, message;
          console.log(acLanguage)
          if(acLanguage == "es"){
    
          tit = 'Solicitud crear falla'
          message = 'algo está mal'
    
          }else {
            tit = 'Request create fail'
            message = 'something is wrong'
          }
    
        this.notification.create(
          type,
          tit,
          message
        );
      }


      getProduct() {
        this.service.getAllProduct().subscribe((response) => {
          console.log("response", response['data'])
          this.products = response['data']
          console.log("after", this.products)
        })
       }

       getService(data) {
        this.service.getServiceByProduct(data).subscribe((response) => {
         // console.log("response", response['data'])
          this.services = response['data']
          console.log("after  services", this.services)
        })
       }
        
       getCustomersList() {
        this.service.getCustomers().subscribe((response) => {
         // console.log("response", response['data'])
          this.customerList = response['data']
          console.log("after  customer list", this.customerList)
        })
       }
         
       listEmployeeByClientId() {
        console.log("this.clientID", this.currentUser);
        this.service.getEmployeeByClientId(this.currentUser).subscribe((response) => {
         // console.log("response", response['data'])
          this.employeeList = response['data']
          console.log("after  customer list", this.employeeList)
        })
       }

       onFileChange(ev) {
        let workBook = null;
        let jsonData = null;
        const reader = new FileReader();
        const file = ev.target.files[0];
        this.fileName = file.name;
        reader.onload = (event) => {
          const data = reader.result;
          workBook = XLSX.read(data, { type: 'binary' });
          jsonData = workBook.SheetNames.reduce((initial, name) => {
            const sheet = workBook.Sheets[name];
            initial[name] = XLSX.utils.sheet_to_json(sheet);
            return initial;
          }, {});
          const dataString = JSON.stringify(jsonData);
          this.bulkdata =JSON.stringify(jsonData);
          document.getElementById('output').innerHTML = dataString.slice(0, 300).concat("...");
          this.setDownload(dataString);
        }
        reader.readAsBinaryString(file);
      }
         

      setDownload(data) {
        this.willDownload = true;
        setTimeout(() => {
          const el = document.querySelector("#download");
          el.setAttribute("href", `data:text/json;charset=utf-8,${encodeURIComponent(data)}`);
          el.setAttribute("download", 'xlsxtojson.json');
        }, 1000)
      }

      groupOfService() {        
        // this.typeOfServiceSelectValue = this.createapplicationAddform.value.type_of_service
    
        this.service.bulkuploadrequestForClient({ id:this.currentUser, d:this.bulkdata}).subscribe(
          (response) =>{
            console.log(response)
            this.successNotification('success')
            this.createapplicationAddform.reset();
            this.isSpinning = false;
          }, 
          (error) => {
            this.errorNotification('error');
            this.isSpinning = false;
          }
        )
      } 
    }