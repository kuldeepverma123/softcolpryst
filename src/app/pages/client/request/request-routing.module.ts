import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RequestConsultComponent } from './consult/request-consult/request-consult.component';
import { RequestConsultStatusComponent } from './consult/request-consult-status/request-consult-status.component';
import { RequestConsultViewComponent } from './consult/request-consult-view/request-consult-view.component';
import { RequestCreateComponent } from './create/request-create/request-create.component';



const routes: Routes = [
  {
    path: 'consult',
    component: RequestConsultComponent
  },
  {
    path: 'consult/status/:id',
    component: RequestConsultStatusComponent
  },
  {
    path: 'consult/view/:id',
    component: RequestConsultViewComponent
  },
  {
    path: 'create',
    component: RequestCreateComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RequestRoutingModule { }
