import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgZorroAntdModule } from 'ng-zorro-antd';

// import ngx-translate and the http loader
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import { HttpClient, HttpClientModule } from '@angular/common/http';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { RequestRoutingModule } from './request-routing.module';
import { RequestConsultComponent } from './consult/request-consult/request-consult.component';
import { RequestCreateComponent } from './create/request-create/request-create.component';
import { RequestConsultFormComponent } from './consult/request-consult-form/request-consult-form.component';
import { RequestConsultTableComponent } from './consult/request-consult-table/request-consult-table.component';
import { RequestConsultStatusComponent } from './consult/request-consult-status/request-consult-status.component';
import { RequestConsultViewComponent } from './consult/request-consult-view/request-consult-view.component';


@NgModule({
  declarations: [RequestConsultComponent, RequestCreateComponent, RequestConsultFormComponent, RequestConsultTableComponent, RequestConsultStatusComponent, RequestConsultViewComponent],
  imports: [
    NgZorroAntdModule,
    HttpClientModule,
    CommonModule,
    RequestRoutingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class RequestModule { }
// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
