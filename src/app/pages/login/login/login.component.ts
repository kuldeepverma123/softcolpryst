import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { NzModalService } from 'ng-zorro-antd/modal';

import { ApiService }  from '../../../api.service';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class loginComponent implements OnInit {

  validateForm: FormGroup

  passwordVisible = false
  password: string

  loginData

  constructor(
    private loginService: ApiService,
    private fb: FormBuilder,
    private modalService: NzModalService,
    private router: Router,
    private authService: AuthenticationService,
    private translate: TranslateService
  ) { 

  }  

  ngOnInit() {
    this.languageTranslate()
    
    this.validateForm = this.fb.group({
      username: [null, [Validators.required]],
      password: [null, [Validators.required]]
    })  
  }

  languageTranslate() {
    let acLanguage = localStorage.getItem('acLanguage')
    if(acLanguage != null && acLanguage != '') {
      this.translate.use(acLanguage)
    } else {
      this.translate.setDefaultLang('es')
    }    
  }  

  submitForm() {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty()
      this.validateForm.controls[i].updateValueAndValidity()
    }
    if (this.validateForm.valid) {

      let params = {
        "email": this.validateForm.value.username,
        "password": this.validateForm.value.password
      }

      // if(this.validateForm.value.username == 'admin@gmail.com' && this.validateForm.value.password == '123456') {
      //   let userdata = {
      //     id: '129',
      //     role: 'admin'
      //   }        
      //   localStorage.setItem('loginRes', JSON.stringify(userdata))
      //   this.router.navigate(['/admin/requests'])
      // } else if(this.validateForm.value.username == 'client@gmail.com' && this.validateForm.value.password == '123456') {
      //   let userdata = {
      //     id: '129',
      //     role: 'client'
      //   }        
      //   localStorage.setItem('loginRes', JSON.stringify(userdata))
      //   this.router.navigate(['/client/requests/consult'])
      // } else if(this.validateForm.value.username == 'analyst@gmail.com' && this.validateForm.value.password == '123456') {
      //   let userdata = {
      //     id: '129',
      //     role: 'analysts'
      //   }        
      //   localStorage.setItem('loginRes', JSON.stringify(userdata))
      //   this.router.navigate(['/analysts/requests'])
      // } else {
      //   this.errorMsg()
      // }      
    
      
      console.log("params loginData", params);

      this.loginService.loginUsers(params).subscribe((response) => {
        this.loginData = response
        
        console.log("loginData", this.loginData);

        if(this.loginData.token) {
          let userdata = {
            id: this.loginData.data.user_id,
            token: this.loginData.token,
            role: this.loginData.data.usertype,
            client_id : ""
           }
 
           if(this.loginData.data.customer_id){
             
            userdata.client_id = this.loginData.data.customer_id
           } 

          console.log("=",userdata);
          localStorage.setItem('loginRes', JSON.stringify(userdata))
          if(userdata.role == 1) {
            this.router.navigate(['/admin/requests'])
           //this.router.navigate(['/client/requests/consult'])
           //this.router.navigate(['/analysts/requests'])
          } else if(userdata.role == 13) {
            // localStorage.setItem('clientRes', JSON.stringify(userdata))
            this.router.navigate(['/client/requests/consult'])
           // this.router.navigate(['/analysts/requests'])
          } else if(userdata.role == 2) {
            this.router.navigate(['/analysts/requests'])
          }
        }
      }, (err) => {
        this.errorMsg()
        console.log('err --', err)
      })
    }
  }

  errorMsg() {
    const modal = this.modalService.error({
      nzTitle: 'Wrong credentials'
    });

    setTimeout(() => modal.destroy(), 2000);
  }  
  
  addLocalStorage() {
    localStorage.setItem('dataSource', 'hello')
  }

  getLocalStorage() {
    console.log('getLocalStorage', localStorage.getItem('dataSource'))
  }

  clearLocalStorage() {
    localStorage.removeItem('dataSource')
  }
  
  useLanguage(language: string) {
    this.translate.use(language)
    localStorage.setItem('acLanguage', language)
  }

}
