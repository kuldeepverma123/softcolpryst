import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

import * as apiUrl  from 'src/config/defaultConfig';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  dummyurl: string = apiUrl.url.DUMMY_URL

  constructor(private http: HttpClient) { 
    
  }

  allTodos() {
    return this.http.get(apiUrl.url.All_TODOS_URL)
  }

  login(params) {
    // return this.http.get(apiUrl.url.DUMMY_URL)
    return this.http.post(this.dummyurl+'/api/login', params)
  }

}
