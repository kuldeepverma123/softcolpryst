import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthenticationAnalystsService } from './authentication-analysts.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardAnalystsService implements CanActivate {

  constructor(
    private authAnalystsService: AuthenticationAnalystsService,
    private router: Router
    ) { 

    }

  canActivate(): boolean {
    if (this.authAnalystsService.loggedIn()) {
      return true
    } else {
      this.router.navigate(['/auth'])
    }
  }

}
