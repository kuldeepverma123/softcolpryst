import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthenticationClientService } from './authentication-client.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardClientService implements CanActivate {

  constructor(
    private authClientService: AuthenticationClientService,
    private router: Router
    ) { 

    }

  canActivate(): boolean {
    if (this.authClientService.loggedIn()) {
      return true
    } else {
      this.router.navigate(['/auth'])
    }
  }

}
