import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationClientService {

  authenticationState = new BehaviorSubject(false)

  constructor( ) { 
  }

  login() {
    return this.authenticationState.next(true)
  }

  logout() { 
    this.authenticationState.next(false)
    localStorage.removeItem('loginRes')
    return window.location.reload()
  }

  isAuthenticated() {    
    return this.authenticationState.value
  }

  loggedIn() {    
    let loginRes = JSON.parse(localStorage.getItem('loginRes'))
    if(loginRes != null) {
      if(loginRes.role == 13) {
        return true
      } else {
        return false
      }
    }
  }


}
